<?php
session_start();
if (!($_SESSION['type'] == 'DD' && $_SESSION['username'])) {
    echo "<script>window.location='login.php'</script>";
}
include 'library/dbconnect.php';
$myusername = $_SESSION['username'];
//include_once("submit.inc.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link rel="stylesheet" type="text/css" href="style.css" />
        <script src="JSCal2/js/jscal2.js"></script>
        <script src="JSCal2/js/lang/en.js"></script>
        <link rel="stylesheet" type="text/css" href="JSCal2/css/jscal2.css" />
        <link rel="stylesheet" type="text/css" href="JSCal2/css/border-radius.css" />
        <link rel="stylesheet" type="text/css" href="JSCal2/css/steel/steel.css" />

        <script src="JSCal2/js/gen_validatorv31.js" type="text/javascript"></script>
    </head>
    <body>
        <div id="container">
            <div id="header">
                <div id="header_title">CENTRAL SPONSORED SCHEME FOR PROVIDING QUALITY EDUCATION IN MADRASA (SPQEM)</div>


            </div>
            <div id="menubar"><ul>
                    <li><a href="DD.php">Home</a></li>

                    <li><a href="logout.php">Logout</a></li>

                </ul></div>
            <div id="body">

                <div id="inner" id="inner">
                    <div id="middle1" style="padding:20px;">

                        <div><font color="RED"><?php //echo $msg;   ?></font></div>

                        <form action="" method="post" name="test" style="border: 1px solid black;padding-left: 10px;padding-right: 10px;width: 935px;">    

                            <?php
                            if (isset($_GET['id'])) {
                                $id = $_GET['id'];
                                $status = $_GET['status'];
                                $username = $_GET['username'];
                                $district = mysql_query("select district from wp_user_master where username='$username'");
                                $districts = mysql_fetch_array($district);
                               $district_name = $districts[0];
                                $districtss = mysql_query("select application_id from wp_master where application_id='$id'");
                                //echo "select application_id from wp_master where id_no='$id'";
                                $application_id = mysql_fetch_array($districtss);
                                $applicationid = $application_id[0];
                                $contact_exist = mysql_query("select * from wp_format5 where mater_application_id='$id' and (status=1 or status=2 or status=3)");
                                // echo "select * from wp_master where application_id='$serach'";
                                if (mysql_num_rows($contact_exist) != 0) {
                                    $result = mysql_fetch_row($contact_exist);
                                    //print_r($result);
                                    $seracch = $result[1];
                                    ?>

                                    <input type="hidden" name="application_no" id="name" value="<?= $result[1] ?>" />
                                    <input type="hidden" name="id_no" id="name" value="<?= $result[2] ?>" />
                                    <div class="main_heading">
                                        <div class="application">
                                            <?php
                                            echo "<b>FORMAT-V - NA</b>";
                                            ?>
                                        </div><!--application-->
                                        <div class="part">
                                            <?php
                                            echo "<b>(To be filled in the State Govt. after approval in GIAC to central Govt. for assistance)<br/>Details for assistance under the Madrasa Modernization Programme</b>";
                                            ?>
                                        </div><!--part--> 
                                    </div><!--main_heading-->   

                                    <div style="clear:both;"></div>
                                    <div id="print_style">
                                        <div class="main_table">
                                            <div class="sub_table1">
                                                1. District/Tehsil :
                                            </div><!-- sub_table1 -->
                                            <div class="sub_table2">
                                                <?= $result[3] ?>
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->  

                                        <div style="clear:both;"></div>

                                        <div class="main_table">
                                            <div class="sub_table1">
                                                2. Name & addrs of madrasa Level of madrasa eg: primary,upper primarty,secondary,senior secondary be stated :
                                            </div><!-- sub_table1 -->
                                            <div class="sub_table2">
                                                <?= $result[4] ?>
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->  

                                        <div style="clear:both;"></div>

                                        <div class="main_table">
                                            <div class="sub_table1">
                                                3. Date of establishment and registration with Madrasa Board/Waqkf Board/NIOS :
                                            </div><!-- sub_table1 -->
                                            <div class="sub_table2">
                                                Reg Date :<?php echo $result[5]; ?><br/>
                                                Reg No :<?php echo $result[6]; ?>
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->  

                                        <div style="clear:both;"></div>

                                        <div class="main_table">
                                            <div class="sub_table1">
                                                4. Details of assistance recieved by central/state schemes in the past :
                                            </div><!-- sub_table1 -->
                                            <div class="sub_table2">
                                                <?= $result[7] ?>
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->  

                                        <div style="clear:both;"></div>

                                        <div class="main_table">
                                            <div class="sub_table1">
                                                5. Total number of students in the madrasa :
                                            </div><!-- sub_table1 -->
                                            <div class="sub_table2">
                                                <?= $result[8] ?>
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->  

                                        <div style="clear:both;"></div>

                                        <div class="main_table">
                                            <div class="sub_table1">
                                                6. If accredited with NIOS number of students appeared for certification of classes 3,5,8,10 & 12 :
                                            </div><!-- sub_table1 -->
                                            <div class="sub_table2">
                                                <?= $result[9] ?>
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->  

                                        <div style="clear:both;"></div>

                                        <div class="main_table">
                                            <div class="sub_table1">
                                                7. Total number of teachers for whom assistance is proposed :
                                            </div><!-- sub_table1 -->
                                            <div class="sub_table2">
                                                <?= $result[10] ?>
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->  

                                        <div style="clear:both;"></div>

                                        <div class="main_table">
                                            <div class="sub_table1">
                                                8. Details of equipment and teaching learning materials required for Science/Computer labs* and Science kit/Math kit* :
                                            </div><!-- sub_table1 -->
                                            <div class="sub_table2">
                                                <?= $result[11] ?>
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->  

                                        <div style="clear:both;"></div>

                                        <div class="main_table">
                                            <div class="sub_table1">
                                                9. Details of text Books/books/Library required for students:
                                            </div><!-- sub_table1 -->
                                            <div class="sub_table2">
                                                <?= $result[12] ?>
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->  

                                        <div style="clear:both;"></div>

                                        <div class="main_table">
                                            <div class="sub_table1">
                                                10. Accreditation with NIOS is required, if not accredited :
                                            </div><!-- sub_table1 -->
                                            <div class="sub_table2">
                                                <?= $result[13] ?>
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->  

                                        <div style="clear:both;"></div>

                                        <div class="main_table">
                                            <div class="sub_table1">
                                                11. Remarks :
                                            </div><!-- sub_table1 -->
                                            <div class="sub_table2">
                                                <?= $result[14] ?>
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->  
                                        <div style="clear:both;"></div>
                                        <div class="main_table" >
                                            <div class="sub_table1" >
                                                12. DD Memo :
                                                </div><!-- sub_table1 -->
                                                <?php                                                 
                                                $dd_memo = mysql_query("select comments from wp_dd_comments where master_id_no='$id'");
                                                $dd_memos = mysql_fetch_array($dd_memo);
                                                $dd_memos_accept = $dd_memos[0];?>
                                                <div class="sub_table2">
                                                    <?php 
                                                echo $dd_memos_accept;
                                                ?>
                                                    </div><!-- sub_table2 -->
                                            
                                            <?php if ($status == 2) { ?>
                                                <style>
                                                    .status{
                                                        display:none;
                                                    }
                                                </style>
                                            <?php } else { ?>

                                                <div class="sub_table2" class="status">
                                                    <textarea name="comments" id="comments"></textarea>
                                                </div><!-- sub_table2 -->

                                            <?php } ?>
                                        </div><!-- main_table -->  
                                        <div style="clear:both;"></div>
                                    </div><!--print_style-->
                                    <div class="noprint">
                                        <?php if ($status == 2) { ?>
                                            <style>
                                                .status{
                                                    display:none;
                                                }
                                            </style>
                                        <?php } elseif ($status == 3) { ?>
                                            <div style="margin-bottom: -15px;padding-top: 36px;" class="status">
                                                <div style="float:right;"><input type="submit" name="submit_confirm" value="Accept"/></div>
                                                <div style="float:right;display: none;"><input type="submit" name="submit_reject" value="Reject"/></div>
                                            </div> 
                                        <?php }
                                        else { ?>
                                            <div style="margin-bottom: -15px;padding-top: 36px;" class="status">
                                                <div style="float:right;"><input type="submit" name="submit_confirm" value="Accept"/></div>
                                                <div style="float:right;"><input type="submit" name="submit_reject" value="Reject"/></div>
                                            </div> 
                                        <?php }
                                        ?>

                                    </div>
                                <?php
                                }
                            }
                              if ($status == 3) {
                                if (isset($_POST['submit_confirm'])) {
;
                                    $comments = $_POST['comments'];
                                    $sql_master = mysql_query("update wp_master set status=2,status_dpi=2 where application_id=$id");
                                    $sql_format1 = mysql_query("update wp_format1 set status=2,status_dpi=2 where master_application_id=$id");
                                    $sql_format2 = mysql_query("update wp_format2 set status=2,status_dpi=2 where mater_application_id=$id");
                                    $sql_format3 = mysql_query("update wp_format3 set status=2,status_dpi=2 where mater_application_id=$id");
                                    $sql_format4 = mysql_query("update wp_format4 set status=2,status_dpi=2 where mater_application_id=$id");
                                    $sql_format5 = mysql_query("update wp_format5 set status=2,status_dpi=2 where mater_application_id=$id");
                                    $status_format5 = mysql_query("update wp_dd_comments set comments='$comments',district='$district_name' where master_application_no=$id");
                                    echo '<script>window.location="DD.php";</script>';
                                }
                            }
                            else if (isset($_POST['submit_confirm'])) {
;
                                $comments = $_POST['comments'];
                                $sql_master = mysql_query("update wp_master set status=2,status_dpi=2 where application_id=$id");
                                $sql_format1 = mysql_query("update wp_format1 set status=2,status_dpi=2 where master_application_id=$id");
                                $sql_format2 = mysql_query("update wp_format2 set status=2,status_dpi=2 where mater_application_id=$id");
                                $sql_format3 = mysql_query("update wp_format3 set status=2,status_dpi=2 where mater_application_id=$id");
                                $sql_format4 = mysql_query("update wp_format4 set status=2,status_dpi=2 where mater_application_id=$id");
                                $sql_format5 = mysql_query("update wp_format5 set status=2,status_dpi=2 where mater_application_id=$id");
                                $sql_comments = mysql_query("insert into wp_dd_comments values('','$seracch','$id','$comments','$district_name')");
                                echo '<script>window.location="DD.php";</script>';
                            }

                            if (isset($_POST['submit_reject'])) {
                                $comments = $_POST['comments'];
                                $sql_master = mysql_query("update wp_master set status=3,status_dpi=5 where application_id=$id");
                                $sql_format1 = mysql_query("update wp_format1 set status=3,status_dpi=5 where master_application_id=$id");
                                $sql_format2 = mysql_query("update wp_format2 set status=3,status_dpi=5 where mater_application_id=$id");
                                $sql_format3 = mysql_query("update wp_format3 set status=3,status_dpi=5 where mater_application_id=$id");
                                $sql_format4 = mysql_query("update wp_format4 set status=3,status_dpi=5 where mater_application_id=$id");
                                $sql_format5 = mysql_query("update wp_format5 set status=3,status_dpi=5 where mater_application_id=$id");
                                $sql_comments = mysql_query("insert into wp_dd_comments values('','$seracch','$id','$comments','$district_name')");
                                echo '<script>window.location="DD.php";</script>';
                            }
                          
//                            if ($status == 3) {
//                                if (isset($_POST['submit_reject'])) {
//                                    $sql_master = mysql_query("update wp_master set status=3,status_dpi=5 where id_no=$id");
//                                    $sql_format1 = mysql_query("update wp_format1 set status=3,status_dpi=5 where master_id_no=$id");
//                                    $sql_format2 = mysql_query("update wp_format2 set status=3,status_dpi=5 where master_id_no=$id");
//                                    $sql_format3 = mysql_query("update wp_format3 set status=3,status_dpi=5 where master_id_no=$id");
//                                    $sql_format4 = mysql_query("update wp_format4 set status=3,status_dpi=5 where master_id_no=$id");
//                                    $sql_format5 = mysql_query("update wp_format5 set status=3,status_dpi=5 where master_id_no=$id");
//                                    $status_format5 = mysql_query("update wp_dd_comments set comments='$comments',district='$district_name' where master_application_no=$seracch and master_id_no=$id");
//                                    echo '<script>window.location="DD.php";</script>';
//                                }
//                            }
                            ?>

                        </form> 
                    </div>
                </div>
            </div>
            <div id="footerouter">
                <div id="footer">© All Rights Reserved. IT@School, General Education Department, Govt of Kerala. </div>
            </div>
        </div>	
    </body>
</html>

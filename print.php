<?php
include 'library/dbconnect.php';
//include_once("submit.inc.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link rel="stylesheet" type="text/css" href="style.css" />
        <script src="JSCal2/js/jscal2.js"></script>
        <script src="JSCal2/js/lang/en.js"></script>
        <link rel="stylesheet" type="text/css" href="JSCal2/css/jscal2.css" />
        <link rel="stylesheet" type="text/css" href="JSCal2/css/border-radius.css" />
        <link rel="stylesheet" type="text/css" href="JSCal2/css/steel/steel.css" />

        <script src="JSCal2/js/gen_validatorv31.js" type="text/javascript"></script>
    </head>
    <style type="text/css">

        @media print
        {
            #header {display:none;}
            .main_table0{display:none;}
            .noprint{display:none;}
            #noprint_pg{display:none;}
            .breadcrumb-list{display:none;}
            #footer{display:none;}
            #menubar{display:none;}
            #print_style{font-size:12px;padding:20px;margin-right: 150px !important;}
            .print_new{border:none !important}
            .main_heading{margin-right: 120px !important;}
            /*.sub_table2_words{margin-right: 120px !important;}*/
            /*.sub_table8{margin-right: 120px !important;}*/
        }
    </style>
    <body>

        <div id="container">
            <div id="header">
                <div id="header_title">CENTRAL SPONSORED SCHEME FOR PROVIDING QUALITY EDUCATION IN MADRASA (SPQEM)</div>
           </div>
            <div id="menubar"><ul>
                    <li><a href="index.php">Home</a></li>
                    <li><a href="input.php">New Application</a></li>
                    <li><a href="edit.php">Edit Application</a></li>
                    <li><a href="print.php">Print Application</a></li>
                    <li><a href="status.php">View Application Status</a></li>
                    <li><a href="login.php">Login</a></li>
                </ul>
            </div>
            <div id="body">

                <!--<div id="inner" id="inner">-->
                <!--<div id="middle1">-->	
                <div id="menubar2">
                    <div class="heading_head">
                        Print Your Application
                    </div>                
                </div>
                <!--                     <div id="inner" id="inner">-->
                <form action="print-apl1.php" method="post" name="test"> 
                    <!--                    <div id="middle1" style="padding:20px;">-->

<!--                        <div><font color="RED"><?php //echo $msg;    ?></font></div>-->


                    <div class="main_table0">
                        <div class="sub_table0">
                            Enter Application Id:
                        </div><!-- sub_table1 -->
                        <div class="sub_table">
                            <input type="text" name="application_number" onkeypress="return numbersonly(event)">
                        </div><!-- sub_table2 -->

                        <div style="clear:both;"></div>

                        <div class="sub_table0">
                            Enter Application No:
                        </div><!-- sub_table1 -->
                        <div class="sub_table">
                            <input type="text" name="file_no" onkeypress="return numbersonly(event)"/>
                        </div><!-- sub_table2 -->

                        <div style="clear:both;"></div>
                        <!-- <div style="float:right;margin-top: -30px;margin-right: 55px;">
                         <a href="print.php" class="back">Back</a>
                      </div>-->
                    </div><!-- main_table -->  

                    <div style="clear:both;"></div>

                    <div class="noprint"><input type="submit" name="submit_form_five" value="Go" class="go_button"></div> 
                </form> 
            </div>
            <div style="clear:both;"></div>
            <!--</div>-->
            <!--</div>-->
            <div id="footerouter">
                <div id="footer">© All Rights Reserved. IT@School, General Education Department, Govt of Kerala. </div>
            </div>
            <!--</div>-->
    </body>
</html> 

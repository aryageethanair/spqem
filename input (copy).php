<?php

//include_once 'submit.inc.php';

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link rel="stylesheet" type="text/css" href="style.css" />
        <script src="JSCal2/js/jscal2.js"></script>
        <script src="JSCal2/js/lang/en.js"></script>
       <link rel="stylesheet" type="text/css" href="JSCal2/css/jscal2.css" />

       <link rel="stylesheet" type="text/css" href="JSCal2/css/border-radius.css" />
        <link rel="stylesheet" type="text/css" href="JSCal2/css/steel/steel.css" />

       <link rel="stylesheet" type="text/javascript" href="JSCal2/js/jquery-1.7.2-min.js" />
       <script type="text/javascript" src="JSCal2/js/jquery-1.4.js"></script>

 <script src="JSCal2/js/gen_validatorv31.js" type="text/javascript"></script>
       <script src="js/jquery.min.js"></script>
    <script src="js/jquery.validate.js"></script>

    <script type="text/javascript">
         $(document).ready(function() {

              $("#poll_form").validate({
                  onkeyup: false,
                   onfocusout: function(element) {
                      $(element).valid();
                  },
                  onblur: function(element) {
                       $(element).valid();
                   },
                 rules: {
                       name_address: {
                           required: true,

                        },

                        society_address: {

                            required: true,

                        },

                        country: {

                            required: true,

                        },

                        email: {

                            required: true,

                            email: true,

                        },

                        obj_actv: {

                            required: true,

                        },

                        radio1: {

                            required: true,

                        },

                        madrasa: {

                            required: true,

                        },

                        mytext: {

                            required: true,

                        },

                        dob: {

                            required: true,

                            date: true,

                        },

                        educational: {

                            required: true,

                        },

                        radio2: {

                            required: true,

                        },

                        nios: {

                            required: true,

                        },

                        qty2: {

                            required: true,

                       },

                        total: {

                            required: true,

                      },

                        qualification_teacher: {

                            required: true,

                        },

                        school_level: {

                            required: true,

                       },

                        radio4: {

                            required: true,

                       },

                        located: {

                            required: true,

                      },

                        radio3: {

                            required: true,

                       },

                        radio5: {

                            required: true,

                        },

                        radio6: {

                            required: true,

                        },

                        radio7: {

                            required: true,

                        },

                        amount_required: {

                            required: true,

                        },

                        room_avaliable: {

                            required: true,

                        },

                        accommodation: {

                            required: true,

                        },

                        seperate_room: {

                            required: true,

                        },

                        proposal: {

                            required: true,

                        },

                        requirements: {

                            required: true,

                       },

                        appointment: {

                            required: true,

                        },

                        total1: {

                            required: true,

                        },

                        format: {

                            required: true,

                        },

                        purchase: {

                            required: true,

                        },

                        vocational_stream: {

                            required: true,

                        },

                        total2: {

                            required: true,

                        },

                        modern_subject: {

                            required: true,

                        },

                    },

                    errorPlacement: function(error, element) {

                        error.insertBefore(element);

                    }

                });



            });

        </script>

        <script>

            var pathToImages = 'images/';

        </script>
        
        
        
        
         <script type="text/javascript">

            function showText(num)

            {

                if (num == 0) {

                    document.getElementById('mytext').style.display = 'block';

                    document.getElementById('madrasa').style.display = 'block';

                    document.getElementById('dob').style.display = 'block';

                    document.getElementById('reg_no').style.display = 'block';

                    document.getElementById('reg_date').style.display = 'block';

                }

                else {

                    document.getElementById('mytext').style.display = 'none';

                    document.getElementById('madrasa').style.display = 'none';

                    document.getElementById('dob').style.display = 'none';

                    document.getElementById('reg_no').style.display = 'none';

                    document.getElementById('reg_date').style.display = 'none';

                    return;

                }

            }

          
  function showText1(num)

            {

                if (num == 0)

                    document.getElementById('mytext1').style.display = 'block';



                else

                    document.getElementById('mytext1').style.display = 'none';



                return;

            }



            function showText2(num)

            {

                if (num == 0)

                    document.getElementById('mytext2').style.display = 'block';



                else

                    document.getElementById('mytext2').style.display = 'none';



                return;

            }



            function showText3(num)

            {

                if (num == 0)

                    document.getElementById('mytext3').style.display = 'block';



                else

                    document.getElementById('mytext3').style.display = 'none';



                return;

            }



            function showText5(num)

            {

                if (num == 0)

                    document.getElementById('purchase').style.display = 'block';



                else

                    document.getElementById('purchase').style.display = 'none';



                return;

            }


            function findTotal() {

                var arr = document.getElementsByName('qty[]');

                var tot = 0;

                for (var i = 0; i < arr.length; i++) {

                    if (parseInt(arr[i].value))

                        tot += parseInt(arr[i].value);

                }

                document.getElementById('total').value = tot;

            }



            function findTotal1() {

                var arr = document.getElementsByName('qty1[]');

                var tot = 0;

                for (var i = 0; i < arr.length; i++) {

                    if (parseInt(arr[i].value))

                        tot += parseInt(arr[i].value);

                }

                document.getElementById('total1').value = tot;

            }



            function findTotal2() {

                var arr = document.getElementsByName('qty2[]');

                var tot = 0;

                for (var i = 0; i < arr.length; i++) {

                    if (parseInt(arr[i].value))

                        tot += parseInt(arr[i].value);

                }

                document.getElementById('total2').value = tot;

            }





          





            function numbersonly(e) {

                var keynum;

                var keychar;

                var numcheck;

                if (window.event) // IE

                {

                    keynum = e.keyCode;

                }

                else if (e.which) // netscape/Firefox/opera

                {

                    keynum = e.which;

                }



                var tabkey = e.keyCode;



                if (keynum == 8 || tabkey == 9 || tabkey == 37 || tabkey == 39)

                {

                    return true;

                }



                else

                {

                    keychar = String.fromCharCode(keynum);

                    //numcheck = /\d/;

                    numcheck = /[.0-9]/;

                    return numcheck.test(keychar);

                }

            }

            $(".nios").change(function() {

                //alert("hai");

                var val = $(this).val();

                //alert(val);

                if (val == 3) {

                    $("#curriculum").show();

                } else {

                    $("#curriculum").hide();

                }

            });



            function yesnoCheck() {

                if (document.getElementById('yesCheck').checked) {

                    document.getElementById('ifYes').style.display = 'block';

                }

                else

                    document.getElementById('ifYes').style.display = 'none';



            }



  $(function() {

                /*----------------- Dropdown1 functions -------------------------*/

                $('#drop1').on('change', function() {

                    var current_val = $(this).val();

                    if (current_val == 1) {

                        $('#elm2').remove();

                        $('#elm3').remove();

                    }

                    if (current_val == 2) {

                        $('#elm3').remove();

                        if ($('#elm2').length == 0) {

                            $('#elmt_cont').append('<div id="elm2"><select name="qualification_teacher[]" id="qualification_teacher" class="qualification_teacher"><option value="">SelectOne</option><option value="1">Post Graduation</option><option value="2">Graduation</option><option value="3">Plus Two</option><option value="4">SSLC</option> <option value="5">Below SSLC</option></select></div>');

                        }

                    }

                    if (current_val == 3) {

                        $('#elm3').remove();

                        if ($('#elm2').length == 0) {

                            $('#elmt_cont').append('<div id="elm2"><select name="qualification_teacher[]" id="qualification_teacher" class="qualification_teacher"><option value="">SelectOne</option><option value="1">Post Graduation</option><option value="2">Graduation</option><option value="3">Plus Two</option> <option value="4">SSLC</option> <option value="5">Below SSLC</option></select></div>');

                        }

                        if ($('#elm3').length == 0) {

                            $('#elmt_cont').append('<div id="elm3"><select name="qualification_teacher[]" id="qualification_teacher" class="qualification_teacher"><option value="">SelectOne</option><option value="1">Post Graduation</option><option value="2">Graduation</option><option value="3">Plus Two</option> <option value="4">SSLC</option> <option value="5">Below SSLC</option></select></div>');

                        }

                    }

                });



            });



    </script>
        <script src="js/dhtmlgoodies_calendar.js"></script>

        <link rel="stylesheet" type="text/css" href="dhtmlgoodies_calendar.css"></link>

        <style type="text/css">
            .sub_table2 label{
                float: right;
                color: red;
            }
        </style>

    </head>

    <body>
        <div>
            <div id="container">

                <div id="header">

                    <div style="padding-top: 56px;">CENTRAL SPONSORED SCHEME FOR PROVIDING QUALITY EDUCATION IN MADRASA (SPQEM)</div>

                </div>

                <div id="menubar"><ul>

                        <li><a href="index.php">Home</a></li>

                        <li><a href="input.php">New Application</a></li>

                        <li><a href="edit.php">Edit Application</a></li>

                        <li><a href="print.php">Print Application</a></li>

                        <li><a href="status.php">View Application Status</a></li>

                        <li><a href="login.php">Login</a></li>

                    </ul></div>

                <div id="menubar2">

                    <div style="padding-top: 6px;">APPLICATION FORM</div>

                </div>



                <div id="content" style="margin-top: 30px;">

                    <form id="poll_form" method="post" name="poll_form" action="format-one-page.php" style="padding: 20px;"> 

                        <div class="main_heading">

                            <div class="application">

<?php

echo "(To be submitted in duplicate)<br/><b>Scheme for Providing Quality Education to Madrasas <br/>APPLICATION FORM</b>";

?>

                            </div><!--application-->

                            <div class="part">

<?php

echo "<b>PART I</b><br/>(To be filled by applicant)";

?>

                            </div><!--part--> 

                        </div><!--main_heading-->

                        <div class="main_table">

                            <div class="sub_table1">

                                1. <b>Name of organisation/Society</b> running the Madrasa * :

                                <br/>(With complete address)

                            </div><!-- sub_table1 -->

                            <div class="sub_table2">

                                <textarea name="society_address" id="society_address" style="width: 209px;"></textarea>

                            </div><!-- sub_table2 -->

                        </div><!-- main_table -->



                        <div style="clear:both;"></div>



                        <div class="main_table">

                            <div class="sub_table1">

                                2. <b>Name with address of Madrasa</b> seeking Financial Assistance :

                            </div><!-- sub_table1 -->

                            <div class="sub_table2">

                                <textarea name="name_address" id="name_address" style="width: 209px;"></textarea>                     

                            </div> 

                        </div><!-- main_table -->



                        <div style="clear:both;"></div>

                        <div class="main_table">

                            <div class="sub_table1">

                                (a). Revenue District :

                            </div><!-- sub_table1 -->

                            <div class="sub_table2">                          

                                <select name="country" id="country" style="width: 209px;">

                                    <option value="">Select One</option> 

                                    <option value="Thiruvanathapuram">Thiruvanathapuram</option> 

                                    <option value="Kollam">Kollam</option>

                                    <option value="Pathanamthitta">Pathanamthitta</option>

                                    <option value="Alappuzha">Alappuzha</option>

                                    <option value="Kottayam">Kottayam</option>

                                    <option value="Idukki">Idukki</option>

                                    <option value="Ernakulam">Ernakulam</option>

                                    <option value="Thrissur">Thrissur</option>

                                    <option value="Palakkad">Palakkad</option>

                                    <option value="Malappuram">Malappuram</option>

                                    <option value="Kozhikode">Kozhikode</option>

                                    <option value="Wayanad">Wayanad</option>

                                    <option value="Kannur">Kannur</option>

                                    <option value="Kasargod">Kasargod</option>

                                </select>



                            </div> <!-- sub_table2 -->

                        </div><!-- main_table -->



                        <div style="clear:both;"></div>



                        <div class="main_table">

                            <div class="sub_table1">

                                (b). Email :

                            </div><!-- sub_table1 -->

                            <div class="sub_table2">

                                <input type="text"  name="email" id="email"/>

                            </div><!-- sub_table2 -->

                        </div><!-- main_table -->



                        <div style="clear:both;"></div>



                        <div class="main_table">

                            <div class="sub_table1">

                                3. <b>Objectives and activities</b> {give brief history of the organization/society running the Madras(s)} :

                            </div><!-- sub_table1 -->

                            <div class="sub_table2">

                                <textarea name="obj_actv" id="society_address1" style="width: 209px;"></textarea>

                            </div><!-- sub_table2 -->

                        </div><!-- main_table -->



                        <div style="clear:both;"></div>



                        <div class="main_table">

                            <div class="sub_table1">

                                4. <b>Whether Registered</b> under central or state WAKF Acts/State Madrasa Board or accredited center of NIOS. If yes,<b> Regn No.</b>( A copy of the registration / accrediation certificate may be attached) :

                            </div><!-- sub_table1 -->

                            <div class="sub_table2">

                                <input type="radio" name="radio1" id="radio1" onclick="showText(0)" value="Yes"/>Yes

                                <input type="radio" name="radio1" id="radio1" onclick="showText(1)" value="No"/>No<br/>  

                                <select name="madrasa" id="madrasa" hidden>

                                    <option value="">Select One</option>

                                    <option value="1">WAKF Acts/</option>

                                    <option value="2">Madrasa</option>

                                    <option value="3">NIOS</option>

                                    <option value="4">Others</option>

                                </select>

                                <div style="padding-top:10px;"></div>                                 

                                <input type="text" id="reg_no" hidden value="Reg No :" style="border: none;outline: none;background: transparent;"/>

                                <input type="text" id="mytext" name="reg_no" hidden onkeypress="return numbersonly(event)">

                                    <div style="padding-top:10px;"></div>

                                    <input type="text" id="reg_date" hidden value="Reg Date :" style="border: none;outline: none;background: transparent;"/>

                                    <input type="text" readonly hidden onclick="displayCalendar(document.getElementById('dob'), 'dd/mm/yyyy', this);" name="reg_date" id="dob">



                                        </div><!-- sub_table2 -->

                                        </div><!-- main_table -->



                                        <div style="clear:both"></div>



                                        <div class="main_table">

                                            <div class="sub_table1">

                                                5. <b>Specific educational activities in modern subjects of the Madrasa seeking financial assistance under the scheme.</b>                                          

                                            </div><!-- sub_table1 -->

                                            <div class="sub_table2">

                                                <textarea name="educational" id="educational" style="width: 209px;"></textarea>

                                            </div><!-- sub_table2 -->                    

                                        </div><!-- main_table -->



                                        <div style="clear:both;"></div>



                                        <div class="main_table">

                                            <div class="sub_table1">

                                                (a). Whether the Madrasa seeking financial assistance has any experience in teaching of subjects like science (Phy., chem.., Bio.), Maths , social studies (history, geography, civics etc.); Languages (State language/Hindi/English) etc.? If so, brief description may be given:

                                            </div><!-- sub_table1 -->

                                            <div class="sub_table2">

                                                <input type="radio" name="radio2" onclick="showText1(0)" value="Yes">Yes

                                                    <input type="radio" name="radio2" onclick="showText1(1)" value="No" />No<br/>                            

                                                    <textarea id="mytext1" name="financial" hidden style="resize:none;width: 209px;"></textarea>

                                            </div><!-- sub_table2 -->

                                        </div><!-- main_table -->



                                        <div style="clear:both;"></div>



                                        <div class="main_table">

                                            <div class="sub_table1">

                                                (b). Whether State curriculum, NIOS or any other curriculum followed; please specify; :

                                            </div><!-- sub_table1 -->

                                            <div class="sub_table2">

                                              <!--  <input type="text" name="nios" id="nios"/>-->

                                                <select name="nios" id="nios" class="nios">

                                                    <option value="">Select One</option>

                                                    <option value="1">State curriculum</option>

                                                    <option value="2">NIOS curriculum</option>

                                                    <option value="3">Others</option>                                

                                                </select>

                                                <input type="text" name="curriculum" id="curriculum" style="display:none;"/>



                                            </div><!-- sub_table2 -->



                                        </div><!-- main_table -->



                                        <div style="clear:both;"></div>



                                        <div class="main_table">

                                            <div class="sub_table1">

                                                (c). Number of children studying these subjects by class and by gender.If there are any children with special needs (disabled children), number and class may be mentioned :

                                            </div><!-- sub_table1 -->

                                            <div class="sub_table2">                           

                                                Boys: <input onblur="findTotal()" type="text" name="qty[]" id="qty2" style="width: 65px;" onkeypress="return numbersonly(event)"/>

                                                Girls: <input onblur="findTotal()" type="text" name="qty[]" id="qty2" style="width: 65px;" onkeypress="return numbersonly(event)"/>

                                                <br/><br/>  Total : <input type="text" name="total" id="total" readonly style="width: 175px;"/>

                                            </div><!-- sub_table2 -->

                                        </div><!-- main_table -->



                                        <div style="clear:both;"></div>



                                        <div class="main_table">

                                            <div class="sub_table1">

                                                (d). No. of teachers already working and teaching modern subjects. Please give details including year of recruitment; whether they are trained teachers (with pre-service qualifications as per NCTE norms); information be given disaggregated by level of teaching (primary/upper primary/secondary/ senior secondary subjects); Subject wise break up be also mentioned :

                                            </div><!-- sub_table1 -->

                                            <div class="sub_table2">

                                                <select name="drop1" id="drop1" class="drop1">

                                                    <option value="1">1</option>

                                                    <option value="2">2</option>

                                                    <option value="3">3</option>

                                                </select><br/>

                                                <input type="hidden" name="elmt_count" id="elmt_count" value="1">

                                                    <div id="elmt_cont" style="width:200px;min-height:50px;">

                                                        <div id="elm1">

                                                            <select name="qualification_teacher[]" id="qualification_teacher" class="qualification_teacher">

                                                                <option value="">SelectOne</option>

                                                                <option value="1">Post Graduation</option>

                                                                <option value="2">Graduation</option>

                                                                <option value="3">Plus Two</option>

                                                                <option value="4">SSLC</option>

                                                                <option value="5">Below SSLC</option>

                                                            </select>

                                                          <!--   <textarea name="qualification_teacher[]" id="qualification_teacher" style="width: 209px;"></textarea>-->

                                                        </div>

                                                    </div>                          

                                            </div><!-- sub_table2 -->

                                        </div><!-- main_table -->                           



                                        <div style="clear:both;"></div>



                                        <div class="main_table">

                                            <div class="sub_table1">

                                                (f). School Level  :

                                            </div><!-- sub_table1 -->

                                            <div class="sub_table2">

                                                <select name="school_level" id="school_level" onclick="schoollevel();">

                                                    <option value="">Select One</option>

                                                    <option value="1">Primary Level</option>

                                                    <option value="2">Upper Primary Level</option>

                                                    <option value="3">Secondary Level</option>

                                                    <option value="4">Sr.Secondary Level</option>

                                                </select>

                                              <!--<input type="text" name="school_level" id="school_level"/>-->

                                            </div><!-- sub_table2 -->

                                        </div><!-- main_table --> 



                                        <div style="clear:both;"></div>



                                        <div class="main_table">                        

                                            6. <b>Infrastructure details of the Madrasa</b>  

                                        </div><!-- main_table -->



                                        <div style="clear:both;"></div>



                                        <div class="main_table">

                                            <div class="sub_table1">

                                                (a). Whether the Madrasa is located in its own or rented building? Give details :

                                            </div><!-- sub_table1 -->

                                            <div class="sub_table2">                        

                                                <input type="radio" name="radio4" value="own">Own

                                                    <input type="radio" name="radio4" value="rent">Rent<br/>

                                                        <textarea name="located" id="located" style="width: 209px;"></textarea>

                                                        </div><!-- sub_table2 -->

                                                        </div><!-- main_table --> 



                                                        <div style="clear:both;"></div>



                                                        <div class="main_table">

                                                            <div class="sub_table1">

                                                                (b). No of rooms available for teaching & administrative purposes :

                                                            </div><!-- sub_table1 -->

                                                            <div class="sub_table2">                        

                                                                <input type="text" name="room_avaliable" id="room_avaliable" onkeypress="return numbersonly(event)">

                                                            </div><!-- sub_table2 -->

                                                        </div><!-- main_table -->  



                                                        <div style="clear:both;"></div>



                                                        <div class="main_table">

                                                            <div class="sub_table1">

                                                                (c). If the present accommodation sufficient for the teaching of traditional as well as modern subjects ? Give details :

                                                            </div><!-- sub_table1 -->

                                                            <div class="sub_table2">

                                                                <input type="radio" name="radio3" onclick="showText2(0)" value="Yes">Yes

                                                                    <input type="radio" name="radio3" onclick="showText2(1)" value="No">No<br/>                            

                                                                        <textarea id="mytext2" name="accommodation" hidden style="resize:none;width: 209px;"></textarea>

                                                                        </div><!-- sub_table2 -->

                                                                        </div><!-- main_table -->



                                                                        <div style="clear:both;"></div>



                                                                        <div class="main_table">

                                                                            <div class="sub_table1">

                                                                                (d). Whether the Madarssa has a seperate room(s) for Science Laboratories & Computer education labs etc. [Applicable only for Secondary/ Sr.secondary level Madarassas(s)] Give details :

                                                                            </div><!-- sub_table1 -->

                                                                            <div class="sub_table2">

                                                                                <input type="radio" name="radio5" onclick="showText3(0)" value="No">No

                                                                                    <input type="radio" name="radio5" onclick="showText3(1)" value="Yes">Yes<br/>                            

                                                                                        <textarea id="mytext3" name="seperate_room" hidden style="resize:none;width: 209px;"></textarea>

                                                                                        </div><!-- sub_table2 -->

                                                                                        </div><!-- main_table -->



                                                                                        <div style="clear:both;"></div>



                                                                                        <div class="main_table">                        

                                                                                            7. <b>Accreditation with NIOS</b>  

                                                                                        </div><!-- main_table -->



                                                                                        <div style="clear:both;"></div>

                                                                                        <div class="main_table">   

                                                                                            <div class="sub_table1">

                                                                                                (a). If already accrediated by NIOS give details of 

                                                                                            </div><!-- sub_table1 -->

                                                                                            <div class="sub_table2">

                                                                                                <input type="radio" onclick="javascript:yesnoCheck();" name="radio6" id="yesCheck" value="Yes"/>Yes

                                                                                                <input type="radio" onclick="javascript:yesnoCheck();" name="radio6" id="noCheck" value="No"/>No

                                                                                            </div><!-- sub_table2 -->

                                                                                        </div><!-- main_table -->

                                                                                        <div style="clear:both;"></div>

                                                                                        <div id="ifYes" style="display:none">       

                                                                                            <div class="main_table">

                                                                                                <div class="sub_table1">

                                                                                                    (i). Number of students registered with NIOS :                            

                                                                                                </div><!-- sub_table1 -->

                                                                                                <div class="sub_table2">

                                                                                                    <input type="text" name="resgistered" id="resgistered" onkeypress="return numbersonly(event)">

                                                                                                </div><!-- sub_table2 -->

                                                                                            </div><!-- main_table -->



                                                                                            <div style="clear:both;"></div>



                                                                                            <div class="main_table">

                                                                                                <div class="sub_table1">

                                                                                                    (ii). Number of students who have obtained certification from NIOS for class 3,5,8,10 and 12 sepeartely for each of the year of accrediation :



                                                                                                </div><!-- sub_table1 -->

                                                                                                <div class="sub_table2">

                                                                                                    Class 3 :&nbsp;&nbsp;<br/>

                                                                                                    Number of students:<br/>

                                                                                                    <input type="text" name="no_stud_cert" id="class3" onkeypress="return numbersonly(event)">

                                                                                                        <input type="hidden" name="year_passing" id="year_passing"/>

                                                                                                </div><!-- sub_table2 -->

                                                                                                <div style="clear:both;"></div>                         



                                                                                                <div class="sub_table2">

                                                                                                    Class 5 :&nbsp;&nbsp;<br/>

                                                                                                    Number of students:<br/>

                                                                                                    <input type="text"  name="no_stud_cert1" id="class5"  onkeypress="return numbersonly(event)">

                                                                                                        <input type="hidden" name="year_passing1" id="year_passing1" />

                                                                                                </div><!-- sub_table2 -->

                                                                                                <div style="clear:both;"></div>



                                                                                                <div class="sub_table2">

                                                                                                    Class 8 :&nbsp;&nbsp;<br/>

                                                                                                    Number of students: <br/>

                                                                                                    <input type="text"  name="no_stud_cert2" id="class8"  onkeypress="return numbersonly(event)">

                                                                                                        <input type="hidden" name="year_passing2" id="year_passing2"/>

                                                                                                </div><!-- sub_table2 -->

                                                                                                <div style="clear:both;"></div>                        



                                                                                                <div class="sub_table2">

                                                                                                    Class 10 :<br/>

                                                                                                    Number of students:<br/>

                                                                                                    <input type="text"name="no_stud_cert3" id="class10" onkeypress="return numbersonly(event)">

                                                                                                        <input type="hidden" name="year_passing3" id="year_passing3" />

                                                                                                </div><!-- sub_table2 -->

                                                                                                <div style="clear:both;"></div>                        



                                                                                                <div class="sub_table2">

                                                                                                    Class 12 :<br/>

                                                                                                    Number of students: <br/>

                                                                                                    <input type="text" name="no_stud_cert4" id="class12"  onkeypress="return numbersonly(event)">

                                                                                                        <input type="hidden" name="year_passing4" id="year_passing4"/>

                                                                                                </div><!-- sub_table2 -->

                                                                                            </div><!-- main_table -->



                                                                                            <div style="clear:both;"></div>



                                                                                            <div class="main_table">

                                                                                                <div class="sub_table1">

                                                                                                    (b). If not yet accrediated, whether the Madrassa seeking financial assistance is interested in NIOS accrediation ? If so, whether applied to NIOS (refernce number of application be given) and by when NIOS accrediation is expected for (a) academic stream (class 3,5,8,10 & 12) and /or (b) vocational stream (Secondary & Sr. seconadry)) :



                                                                                                </div><!-- sub_table1 -->

                                                                                                <div class="sub_table2">

                                                                                                    <textarea name="financila_assisment" id="financila_assisment" style="width: 209px;"></textarea>

                                                                                                </div><!-- sub_table2 -->

                                                                                            </div><!-- main_table -->

                                                                                        </div>







                                                                                        <div style="clear:both;"></div>



                                                                                        <div class="main_table"> 

                                                                                            <div class="sub_table1">                       

                                                                                                8. <b>Details of proposal for financial assistance :</b>  

                                                                                            </div>

                                                                                            <div class="sub_table2">

                                                                                                <select name="proposal" id="proposal">

                                                                                                    <option value="1">Filled format I enclosed</option>

                                                                                                    <option value="2">Filled format I not enclosed</option>

                                                                                                </select>

                                                                                           <!--   <input type="text" name="proposal" id="proposal">-->

                                                                                            </div><!-- sub_table2 -->

                                                                                        </div><!-- main_table -->



                                                                                        <div style="clear:both;"></div>



                                                                                        <div class="main_table">

                                                                                            <div class="sub_table1">

                                                                                                (i). No of addtional teachers and amount required for teaching modern subjects as well as provision for their training :

                                                                                            </div><!-- sub_table1 -->

                                                                                            <div class="sub_table2">

                                                                                                <textarea name="additional_teacher" id="additional_teacher" onkeypress="return numbersonly(event)" style="width: 209px;"></textarea>

                                                                                            </div><!-- sub_table2 -->

                                                                                        </div><!-- main_table -->



                                                                                        <div style="clear:both;"></div>



                                                                                        <div class="main_table"> 

                                                                                            <div class="sub_table1">                       

                                                                                                <b>Requirements be given in Format I enclosed :</b>  

                                                                                            </div>

                                                                                            <div class="sub_table2">

                                                                                                <select name="requirements" id="requirements">

                                                                                                    <option value="1">Filled format I enclosed</option>

                                                                                                    <option value="2">Filled format I not enclosed</option>

                                                                                                </select>

                                                                                               <!--<textarea name="requirements" id="requirements" style="width: 209px;"></textarea>-->

                                                                                            </div><!-- sub_table2 -->

                                                                                        </div><!-- main_table -->



                                                                                        <div style="clear:both;"></div>



                                                                                        <div class="main_table" hidden> 

                                                                                            <div class="sub_table1">                       

                                                                                                (These appointments may be on contract basis. This scheme doesnot provide for a cadre or regular appointment. These are purely on the short term basis.) :  

                                                                                            </div>

                                                                                            <div class="sub_table2">

                                                                                               <!--  <input type="text" name="appointment" id="appointment">-->

                                                                                            </div><!-- sub_table2 -->

                                                                                        </div><!-- main_table -->



                                                                                        <div style="clear:both;"></div>



                                                                                        <div class="main_table" hidden>

                                                                                            <div class="sub_table1">

                                                                                                (ii). Number and amount required for Libraries/Book banks/Text books/Science labs/Computer labs/Science & Maths Kits tc. for teaching modern subjects :

                                                                                            </div><!-- sub_table1 -->

                                                                                            <div class="sub_table2">                          

                                                                                                Computer rate : <input onblur="findTotal1()" type="text" name="computer" id="qty3" />

                                                                                            </div>

                                                                                            <div style="padding-bottom: 25px;padding-top:20px"></div>

                                                                                            <div class="sub_table2">

                                                                                                For Training :&nbsp;&nbsp;&nbsp; <input type="text" name="training" id="qty3"/>                        

                                                                                            </div>

                                                                                            <div style="padding-bottom: 25px;padding-top:20px"></div>

                                                                                            <div class="sub_table2">

                                                                                                Annual Grand : <input  type="text" name="annual" id="qty3"  />

                                                                                            </div>

                                                                                            <div style="padding-bottom: 25px;padding-top:20px"></div>        

                                                                                            <div class="sub_table2">

                                                                                                Library :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input  type="text" name="library" id="qty3"/>                        

                                                                                            </div>                      

                                                                                            <div style="padding-bottom: 25px;padding-top:20px"></div>

                                                                                            <div class="sub_table2">

                                                                                                Total : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="total1" id="total1" readonly />

                                                                                            </div><!-- sub_table2 -->

                                                                                        </div><!-- main_table -->



                                                                                        <div style="clear:both;"></div>



                                                                                        <div class="main_table"> 

                                                                                            <div class="sub_table1">                       

                                                                                                <b>Requirements be given in Format II enclosed :</b> <br/>

                                                                                                (Laboratories/Science labs/Computer labs are for secondary & senior secondary level only) 

                                                                                            </div>

                                                                                            <div class="sub_table2">

                                                                                                <select name="format" id="format">

                                                                                                    <option value="1">Filled format II enclosed</option>

                                                                                                    <option value="2">Filled format II not enclosed</option>

                                                                                                </select>

                                                                                              <!--<input type="text" name="format" id="format"/>-->

                                                                                            </div><!-- sub_table2 -->

                                                                                        </div><!-- main_table -->



                                                                                        <div style="clear:both;"></div>



                                                                                        <div class="main_table"> 

                                                                                            <div class="sub_table1">                       

                                                                                                (iii) For library books has a selction criteria been developed and a purchase commitee been set up by the madrassa ? :

                                                                                            </div>

                                                                                            <div class="sub_table2">

                                                                                                <input type="radio" name="radio7" onclick="showText5(0)" value="Yes">Yes

                                                                                                    <input type="radio" name="radio7" onclick="showText5(1)" value="No">No<br/>

                                                                                                        <textarea name="purchase" id="purchase" hidden style="width: 209px;"></textarea>

                                                                                                        </div><!-- sub_table2 -->

                                                                                                        </div><!-- main_table -->



                                                                                                        <div style="clear:both;"></div>



                                                                                                        <div class="main_table"> 

                                                                                                            <div class="sub_table1">                       

                                                                                                                (iv) Amount required by Madrassas opting for NIOS accrediation for academic stream in modern subjects :

                                                                                                            </div>

                                                                                                            <div class="sub_table2">

                                                                                                                <input type="text" name="amount_required" id="amount_required" onkeypress="return numbersonly(event)"/>

                                                                                                            </div><!-- sub_table2 -->

                                                                                                        </div><!-- main_table -->



                                                                                                        <div style="clear:both;"></div>



                                                                                                        <div class="main_table">                                              

                                                                                                            <b>Requirements be given in Format III enclosed :</b> 

                                                                                                        </div><!-- main_table -->



                                                                                                        <div class="main_table"> 

                                                                                                            <div class="sub_table1">                       

                                                                                                                (v) Amount required by Madrassas opting for NIOS accrediation for vocational stream :

                                                                                                            </div>

                                                                                                            <div class="sub_table2">

                                                                                                                <input type="text" name="vocational_stream" id="vocational_stream" onkeypress="return numbersonly(event)"/>

                                                                                                            </div><!-- sub_table2 -->

                                                                                                        </div><!-- main_table -->



                                                                                                        <div style="clear:both;"></div>



                                                                                                        <div class="main_table">                                              

                                                                                                            <b>Requirements of funds to be given in Format IV enclosed :</b> 

                                                                                                        </div><!-- main_table -->



                                                                                                        <div style="clear:both;"></div>



                                                                                                        <div class="main_table" hidden> 

                                                                                                            <div class="sub_table1">                       

                                                                                                                10. Total Amount required :

                                                                                                            </div>

                                                                                                            <div class="sub_table2">

                                                                                                                For salary : &nbsp;<input  type="text" name="salary" id="salary"/> 

                                                                                                                <div style="padding-top:10px"></div>

                                                                                                                Others : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input  type="text" name="others" id="others" /> 

                                                                                                                <div style="padding-top:10px"></div>   



                                                                                                                <div style="clear:both;"></div>             

                                                                                                                Total : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="total2" id="total2" />

                                                                                                            </div><!-- sub_table2 -->

                                                                                                        </div><!-- main_table -->

                                                                                                        <div style="clear:both;"></div>

                                                                                                        <div style="clear:both;"></div>



                                                                                                        <div class="main_table"> 

                                                                                                            <div class="sub_table1">                       

                                                                                                                (11) Whether the Madrasa is getting any financial assistance for teaching of modern subjects from an y other source. If so, the amount and the purpose for which it is intended, be mentioned. (No duplication should be done) :

                                                                                                            </div>

                                                                                                            <div class="sub_table2">

                                                                                                                <textarea name="modern_subject" id="modern_subject" style="width: 209px;">0</textarea>

                                                                                                            </div><!-- sub_table2 -->

                                                                                                        </div><!-- main_table -->



                                                                                                        <div style="clear:both;"></div>



                                                                                                        <div class="main_table" hidden> 

                                                                                                            <div class="sub_table1">                       

                                                                                                                (12) Net amount requested from Government (10-11) :

                                                                                                            </div>

                                                                                                            <div class="sub_table2">

                                                                                                                Net Amount :&nbsp; 

                                                                                                                <input type="text" size="18" value="" name="inum" onblur="poll_form.rnum.value = toWords(poll_form.inum.value);" onkeypress="return numbersonly(event)">

                                                                                                                    <br/>  <div style="clear:both;"></div>

                                                                                                                    In Words :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <textarea name="rnum" id="rnum" readonly style="width: 205px;"></textarea>

                                                                                                            </div><!-- sub_table2 -->

                                                                                                        </div><!-- main_table -->



                                                                                                        <div style="clear:both;"></div>



                                                                                                        <div><input type="submit" name="submit" value="Next" class="submit_button"/></div>                                         



                                                                                                        <!--</div>--><!-- end of foundproperty--> 



                                                                                                        </form>		

                                                                                                        </div>

                                                                                                        <div style="clear:both;"></div>

                                                                                                        <div

                                                                                                            <div id="footerouter1">

                                                                                                                <div id="footer">© All Rights Reserved. IT@School, General Education Department, Govt of Kerala. </div>

                                                                                                            </div>	

                                                                                                           

   </body>

 </html>




<?php
include 'library/dbconnect.php';
//include_once("submit.inc.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link rel="stylesheet" type="text/css" href="style.css" />
        <script src="JSCal2/js/jscal2.js"></script>
        <script src="JSCal2/js/lang/en.js"></script>
        <link rel="stylesheet" type="text/css" href="JSCal2/css/jscal2.css" />
        <link rel="stylesheet" type="text/css" href="JSCal2/css/border-radius.css" />
        <link rel="stylesheet" type="text/css" href="JSCal2/css/steel/steel.css" />

        <script src="JSCal2/js/gen_validatorv31.js" type="text/javascript"></script>
    </head>
    <body>
           <style type="text/css">

            @media print
            {
                #header {display:none;}
                .main_table0{display:none;}
                .noprint{display:none;}
                #noprint_pg{display:none;}
                .breadcrumb-list{display:none;}
                #footer{display:none;}
                #menubar{display:none;}
                #print_style{font-size:12px;padding:20px;margin-right: 150px !important;}
                .print_new{border:none !important}
                .main_heading{margin-right: 120px !important;}
                .id_no{margin-right: 30px !important;}
                /*.sub_table2_words{margin-right: 120px !important;}*/
                /*.sub_table8{margin-right: 120px !important;}*/
            }
        </style>
        <div id="container">
            <div id="header">
                <div id="header_title">CENTRAL SPONSORED SCHEME FOR PROVIDING QUALITY EDUCATION IN MADRASA (SPQEM)</div>


            </div>
            <div id="menubar"><ul>
                    <li><a href="index.php">Home</a></li>
         <li><a href="input.php">New Application</a></li>
         <li><a href="edit.php">Edit Application</a></li>
         <li><a href="print.php">Print Application</a></li>
         <li><a href="status.php">View Application Status</a></li>
         <li><a href="login.php">Login</a></li>

                </ul></div>
            <div id="body">

                <div id="inner" id="inner">
                     <form action="" method="post" name="test"> 
                    <div id="middle1" style="padding:20px;">

                        <div><font color="RED"><?php //echo $msg;   ?></font></div>


                           <?php
                            if (isset($_POST['submit_form_five'])) {
                                $serach = $_POST['application_number'];
                                $file_no=$_POST['file_no'];
                               $contact_exist=mysql_query("select * from wp_master where application_id='$serach' and id_no='$file_no' and status=1");
                                // echo "select * from wp_master where application_id='$serach'";
                                if (mysql_num_rows($contact_exist) != 0) {
                                    $result = mysql_fetch_row($contact_exist);
                                    $seracch= $result[1];
                                    $fileno= $result[2];
                                    ?>

                                    <input type="hidden" name="application_no" id="name" value="<?= $result[1] ?>" />
                                    <input type="hidden" name="id_no" id="name" value="<?= $result[2] ?>" />
                                    <div style="border: 1px solid black;padding: 10px;width: 910px;margin-top: 14px;" class="print_new">
                                    <div class="main_heading">
                                        <div class="application">
                                            <?php
                                            echo "(To be submitted in duplicate)<br/><b>Scheme for Providing Quality Education to Madrasas <br/>APPLICATION FORM</b>";
                                            ?>
                                        </div><!--application-->
                                        <div class="part">
                                            <?php
                                            echo "<b>PART I</b><br/>(To be filled by applicant)";
                                            ?>
                                        </div><!--part--> 
                                    </div><!--main_heading-->
                                    
                       <div id="print_style">
                                        <div class="applicationid">
                                            <div class="application_no">Application Id:&nbsp;<?php echo $result[1]; ?></div>     
                                            <div class="id_no">Application No:&nbsp;<?php echo $result[2]; ?></div>
                                        </div>
                                        <div class="main_table">
                                            <div class="sub_table1">
                                                1. <b>Name of organisation/Society</b> running the Madrasa * :
                                                <br/>(With complete address)
                                            </div><!-- sub_table1 -->
                                            <div class="sub_table2">
                                                <?= $result[4] ?>
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>

                                        <div class="main_table">
                                            <div class="sub_table1">
                                                2. <b>Name with address of Madrasa</b> seeking Financial Assistance :
                                            </div><!-- sub_table1 -->
                                            <div class="sub_table2">
                                                <?= $result[5] ?>
                                            </div><!-- sub_table2 -->                  

                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>

                                        <div class="main_table">
                                            <div class="sub_table1">
                                                (a). Email :
                                            </div><!-- sub_table1 -->
                                            <div class="sub_table2">
                                                <?= $result[7] ?>
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>

                                        <div class="main_table">
                                            <div class="sub_table1">
                                                3. <b>Objectives and activities</b> {give brief history of the organization/society running the Madras(s)} :
                                            </div><!-- sub_table1 -->
                                            <div class="sub_table2">
                                                <?= $result[8] ?>
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>

                                        <div class="main_table">
                                            <div class="sub_table1">
                                                4. <b>Whether Registered</b> under central or state WAKF Acts/State Madrasa Board or accredited center of NIOS. If yes,<b> Regn No.</b>( A copy of the registration / accrediation certificate may be attached) :
                                            </div><!-- sub_table1 -->
                                            <div class="sub_table2">
                                                <?php
                                                $value = $result[9];
                                                if ($value == 'Yes') {
                                                    ?>
                                                    <?= $result[9]; ?><br/>
                                                   <?php $madrasa= $result[10]; 
                                                    if($madrasa==1){
                                                        echo "WAKF Acts";
                                                    }
                                                     if($madrasa==2){
                                                        echo "Madrasa";
                                                    }
                                                    if($madrasa==3){
                                                        echo "NIOS";
                                                    }
                                                    if($madrasa==4){
                                                        echo "Others";
                                                    }
                                                    ?><br/>     
                                                    Reg No :<?= $result[11] ?><br/>
                                                    <div id="noprint_pg"> Reg Date : <?= $result[12] ?></div>
                                                    <?php
                                                }
                                                if ($value == 'No') {
                                                    ?>
                                                    <?= $result[9]; ?>
                                                <?php }
                                                ?>

                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>

                                        <div class="main_table">
                                            <div class="sub_table1">
                                                5. <b>Specific educational activities in modern subjects of the Madrasa seeking financial assistance under the scheme.</b>                                          
                                            </div><!-- sub_table1 -->
                                            <div class="sub_table2">
                                                <?= $result[13] ?>
                                            </div><!-- sub_table2 -->                    
                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>

                                        <div class="main_table">
                                            <div class="sub_table1">
                                                (a). Whether the Madrasa seeking financial assistance has any experience in teaching of subjects like science (Phy., chem.., Bio.), Maths , social studies (history, geography, civics etc.); Languages (State language/Hindi/English) etc.? If so, brief description may be given:
                                            </div><!-- sub_table1 -->
                                            <div class="sub_table2">
                                                <?php
                                                $values = $result[14];
                                                if ($values == 'Yes') {
                                                    ?>
                                                    <?= $result[14] ?><br/> 
                                                    <?= $result[15] ?>
                                                    <?php
                                                }
                                                if ($values == 'No') {
                                                    ?>
                                                    <?= $result[14] ?>
                                                <?php }
                                                ?>
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>

                                        <div class="main_table">
                                            <div class="sub_table1">
                                                (b). Whether State curriculum, NIOS or any other curriculum followed; please specify; :
                                            </div><!-- sub_table1 -->
                                            <div class="sub_table2">
                                                <?php $cirriculam= $result[16];
                                                if($cirriculam==1){
                                                  echo "State curriculum";
                                                }
                                                
                                                 if($cirriculam==2){
                                                  echo "NIOS curriculum";
                                                }
                                                
                                                 if($cirriculam==3){
                                                  echo "Others";echo "<br/>";
                                                  echo $result[17];
                                                }
                                                
                                                ?>

                                            </div><!-- sub_table2 -->

                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>

                                        <div class="main_table">
                                            <div class="sub_table1">
                                                (c). Number of children studying these subjects by class and by gender.If there are any children with special needs (disabled children), number and class may be mentioned :
                                            </div><!-- sub_table1 -->
                                            <div class="sub_table2">    
                                                <?php
                                                $total = $result[18];
                                                $newArray = explode(",", $total);
                                                implode(" ", $newArray);
                                                $boy = $newArray[0];
                                                $girl = $newArray[1];
                                                ?>

                                                Boys: <?= $boy ?> &
                                                Girls: <?= $girl ?>
                                                <br/>  Total :<?= $result[19]; ?>
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>


                                        <?php
                                        $count_techer1 = $result[20];
                                        $qualification_teacher = $result[21];
                                        $qualification_teacher1 = explode(",", $qualification_teacher);
                                        implode(" ", $qualification_teacher1);
                                        $qualification_teachers1 = $qualification_teacher1[0];
                                        $qualification_teachers2 = $qualification_teacher1[1];
                                        $qualification_teachers3 = $qualification_teacher1[2];
                                        if ($count_techer1 == 1) {
                                            ?>
                                            <div class="main_table">
                                                <div class="sub_table1">
                                                    (d). No. of teachers already working and teaching modern subjects. Please give details including year of recruitment; whether they are trained teachers (with pre-service qualifications as per NCTE norms); information be given disaggregated by level of teaching (primary/upper primary/secondary/ senior secondary subjects); Subject wise break up be also mentioned :
                                                </div><!-- sub_table1 -->
                                                <div class="sub_table2">
                                                    <?= $result[20] . ' &nbsp;Teacher' ?>
                                                    <br/>

                                                    <div id="elmt_cont" style="width:200px;min-height:50px;">
                                                        <div id="elm1">
                                                            <?php if($qualification_teachers1==1){
                                                                echo "Post Graduation";
                                                            }
                                                            if($qualification_teachers1==2){
                                                                echo "Graduation";
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>                          
                                                </div><!-- sub_table2 -->
                                            </div><!-- main_table -->
                                            <?php
                                        }

                                        if ($count_techer1 == 2) {
                                            ?>
                                            <div class="main_table">
                                                <div class="sub_table1">
                                                    (d). No. of teachers already working and teaching modern subjects. Please give details including year of recruitment; whether they are trained teachers (with pre-service qualifications as per NCTE norms); information be given disaggregated by level of teaching (primary/upper primary/secondary/ senior secondary subjects); Subject wise break up be also mentioned :
                                                </div><!-- sub_table1 -->
                                                <div class="sub_table2">
                                                    <?= $result[20] . '&nbsp;Teachers' ?><br/>
                                                    <input type="hidden" name="elmt_count" id="elmt_count" value="1">
                                                        <div id="elmt_cont" style="width:200px;min-height:50px;">
                                                            <div id="elm1">
                                                                <?php if($qualification_teachers1==1){
                                                                echo "Post Graduation";
                                                                echo "<br/>";
                                                            }
                                                            if($qualification_teachers1==2){
                                                                echo "Graduation";
                                                                 echo "<br/>";
                                                            }
                                                           if($qualification_teachers2==1){
                                                                echo "Post Graduation";
                                                                 echo "<br/>";
                                                            }
                                                            if($qualification_teachers2==2){
                                                                echo "Graduation";
                                                                 echo "<br/>";
                                                            } ?>
                                                            </div>
                                                        </div>                          
                                                </div><!-- sub_table2 -->
                                            </div><!-- main_table -->

                                            <?php
                                        }
                                        if ($count_techer1 == 3) {
                                            ?>
                                            <div class="main_table">
                                                <div class="sub_table1">
                                                    (d). No. of teachers already working and teaching modern subjects. Please give details including year of recruitment; whether they are trained teachers (with pre-service qualifications as per NCTE norms); information be given disaggregated by level of teaching (primary/upper primary/secondary/ senior secondary subjects); Subject wise break up be also mentioned :
                                                </div><!-- sub_table1 -->
                                                <div class="sub_table2">
                                                    <?= $result[20] .'&nbsp;Teachers' ?><br/>
                                                    <div id="elmt_cont" style="width:200px;min-height:50px;">
                                                        <div id="elm1">
                                                            
                                                             <?php if($qualification_teachers1==1){
                                                                echo "Post Graduation";
                                                                echo "<br/>";
                                                            }
                                                            if($qualification_teachers1==2){
                                                                echo "Graduation";
                                                                 echo "<br/>";
                                                            }
                                                           if($qualification_teachers2==1){
                                                                echo "Post Graduation";
                                                                 echo "<br/>";
                                                            }
                                                            if($qualification_teachers2==2){
                                                                echo "Graduation";
                                                                 echo "<br/>";
                                                            } 
                                                            if($qualification_teachers3==1){
                                                                echo "Post Graduation";
                                                                 echo "<br/>";
                                                            }
                                                            if($qualification_teachers3==2){
                                                                echo "Graduation";
                                                                 echo "<br/>";
                                                            } 
                                                            ?>
                                                            
                                                        </div>
                                                    </div>                          
                                                </div><!-- sub_table2 -->
                                            </div><!-- main_table -->

                                        <?php } ?>
                                        <!--
                                                             <script> $("#drop1 option[value='<?php //echo $result[20];  ?>']").attr('selected', 'selected') </script>
                                        -->
                                        <div style="clear:both;"></div>
                                        <div class="main_table" id="noprint_pg">
                                            <div class="sub_table1">
                                                (f). School Level  :
                                            </div><!-- sub_table1 -->
                                            <div class="sub_table2">
                                                <? $school_level=$result[22];
                                                if($school_level==1){
                                                    echo "Primary Level";
                                                }
                                                if($school_level==2){
                                                    echo "Upper Primary Level";
                                                }
                                                if($school_level==3){
                                                    echo "Secondary Level";
                                                }
                                                if($school_level==4){
                                                    echo "Sr.Secondary Level";
                                                }
                                                ?>
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table --> 

                                        <div style="clear:both;"></div>

                                        <div class="main_table">                        
                                            6. <b>Infrastructure details of the Madrasa</b>  
                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>

                                        <div class="main_table">
                                            <div class="sub_table1">
                                                (a). Whether the Madrasa is located in its own or rented building? Give details :
                                            </div><!-- sub_table1 -->
                                            <div class="sub_table2">   
                                                <?php
                                                $rent = $result[23];
                                                if ($rent == 'own') {
                                                    ?>
                                                    <?= $result[23]; ?><br/> 
                                                    <?= $result[24]; ?>
                                                    <?php
                                                }
                                                if ($rent == 'rent') {
                                                    ?>
                                                    <?= $result[23]; ?><br/>
                                                    <?= $result[24]; ?>
                                                <?php }
                                                ?>

                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table --> 

                                        <div style="clear:both;"></div>

                                        <div class="main_table">
                                            <div class="sub_table1">
                                                (b). No of rooms available for teaching & administrative purposes :
                                            </div><!-- sub_table1 -->
                                            <div class="sub_table2">                        
                                                <?= $result[25]; ?>
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->  

                                        <div style="clear:both;"></div>

                                        <div class="main_table">
                                            <div class="sub_table1">
                                                (c). If the present accommodation sufficient for the teaching of traditional as well as modern subjects ? Give details :
                                            </div><!-- sub_table1 -->
                                            <div class="sub_table2">

                                                <?php
                                                $accommodation = $result[26];
                                                if ($accommodation == 'No') {
                                                    ?>
                                                    <?= $result[26]; ?>

                                                    <?php
                                                }
                                                if ($accommodation == 'Yes') {
                                                    ?>
                                                    <?= $result[26]; ?><br/>                                
                                                    <?= $result[27] ?>
                                                <?php }
                                                ?>                            

                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>

                                        <div class="main_table">
                                            <div class="sub_table1">
                                                (d). Whether the Madarssa has a seperate room(s) for Science Laboratories & Computer education labs etc. [Applicable only for Secondary/ Sr.secondary level Madarassas(s)] Give details :
                                            </div><!-- sub_table1 -->
                                            <div class="sub_table2">
                                                <?php
                                                $seperate_room = $result[28];
                                                if ($seperate_room == 'No') {
                                                    ?>
                                                    <?= $result[28]; ?><br/> 
                                                    <?= $result[29]; ?>
                                                    <?php
                                                }
                                                if ($seperate_room == 'Yes') {
                                                    ?>
                                                    <?= $result[28]; ?>
                                                <?php }
                                                ?>         

                                            </div><!--completed value--><!-- sub_table2 -->
                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>


                                        <div class="main_table">                        
                                            7. <b>Accreditation with NIOS</b>  
                                        </div><!-- main_table -->

                                        <div class="main_table">                        
                                            <div class="sub_table1">
                              (a). If already accrediated by NIOS give details of 
                         </div><!-- sub_table1 -->
                         
                                        
                                        <div class="sub_table2">
                                                <?php
                                               $radio6 = $result[30];
                                                if ($radio6 == 'No') {
                                                    ?>
                                                    <?= $radio6;
                                                }
                                                if ($radio6 == 'Yes') {
                                                    ?>
                                                    <?= $radio6; ?>
                                            </div><!-- sub_table2 -->
                                            </div><!-- main_table -->
                                            <div id="ifYes">       
                                                                                            <div class="main_table">
                                                                                                <div class="sub_table1">
                                                                                                    (i). Number of students registered with NIOS :                            
                                                                                                </div><!-- sub_table1 -->
                                                                                                <div class="sub_table2">
                                                                                                    <?=$result[31]?>
                                                                                                </div><!-- sub_table2 -->
                                                                                            </div><!-- main_table -->

                                                                                            <div style="clear:both;"></div>

                                                                                            <div class="main_table">
                                                                                                <div class="sub_table1">
                                                                                                    (ii). Number of students who have obtained certification from NIOS for class 3,5,8,10 and 12 sepeartely for each of the year of accrediation :

                                                                                                </div><!-- sub_table1 -->
                                                                                                <div class="sub_table2">
                                                                                                    Class 3 :&nbsp;&nbsp;<br/>
                                                                                                    Number of students:<?=$result[32]?><br/>  
                                                                                                           Year Of Passing : <?=$result[33]?>
                                                                                                </div><!-- sub_table2 -->
                                                                                                <div style="clear:both;"></div>                         

                                                                                                <div class="sub_table2">
                                                                                                    Class 5 :&nbsp;&nbsp;<br/>
                                                                                                    Number of students:<?=$result[34]?><br/>  
                                                                                                           Year Of Passing : <?=$result[35]?>
                                                                                                </div><!-- sub_table2 -->
                                                                                                <div style="clear:both;"></div>

                                                                                                <div class="sub_table2">
                                                                                                    Class 8 :&nbsp;&nbsp;<br/>
                                                                                                    Number of students:<?=$result[36]?><br/>  
                                                                                                       Year Of Passing : <?=$result[37]?>
                                                                                                </div><!-- sub_table2 -->
                                                                                                <div style="clear:both;"></div>                        

                                                                                                <div class="sub_table2">
                                                                                                    Class 10 :<br/>
                                                                                                    Number of students:<?=$result[38]?><br/>                                                                                                   
                                                                                                       Year Of Passing : <?=$result[39]?>
                                                                                                </div><!-- sub_table2 -->
                                                                                                <div style="clear:both;"></div>                        

                                                                                                <div class="sub_table2">
                                                                                                    Class 12 :<br/>
                                                                                                    Number of students:<?=$result[40]?><br/>
                                                                                                   Year Of Passing : <?=$result[41]?>
                                                                                                </div><!-- sub_table2 -->
                                                                                            </div><!-- main_table -->

                                                                                            <div style="clear:both;"></div>

                                                                                            <div class="main_table">
                                                                                                <div class="sub_table1">
                                                                                                    (b). If not yet accrediated, whether the Madrassa seeking financial assistance is interested in NIOS accrediation ? If so, whether applied to NIOS (refernce number of application be given) and by when NIOS accrediation is expected for (a) academic stream (class 3,5,8,10 & 12) and /or (b) vocational stream (Secondary & Sr. seconadry)) :

                                                                                                </div><!-- sub_table1 -->
                                                                                                <div class="sub_table2">
                                                                                                    <?=$result[42]?>
                                                                                                </div><!-- sub_table2 -->
                                                                                            </div><!-- main_table -->
                                                                                        </div>
                                            
                                                <?php }
                                                ?>  
                                            
                                        <div style="clear:both;"></div>

                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>


                                        <div class="main_table"> 
                                            <div class="sub_table1">                       
                                                8. <b>Details of proposal for financial assistance :</b>  
                                            </div>
                                            <div class="sub_table2">
                                                <?= $result[43] ?>
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>

                                        <div class="main_table">
                                            <div class="sub_table1">
                                                (i). No of addtional teachers and amount required for teaching modern subjects as well as provision for their training :
                                            </div><!-- sub_table1 -->
                                            <div class="sub_table2">
                                                <?= $result[44] ?>
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>

                                        <div class="main_table"> 
                                            <div class="sub_table1">                       
                                                <b>Requirements be given in Format I enclosed :</b>  
                                            </div>
                                            <div class="sub_table2">
                                                <?= $result[45] ?>
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>

                                        <div class="main_table"> 
                                            <div class="sub_table1">                       
                                                (These appointments may be on contract basis. This scheme doesnot provide for a cadre or regular appointment. These are purely on the short term basis.) :  
                                            </div>
                                            <div class="sub_table10">
                                                <?//= $result[46] ?>
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>

                                       <div class="main_table">
                                                                                            <div class="sub_table1">
                                                                                                (ii). Number and amount required for Libraries/Book banks/Text books/Science labs/Computer labs/Science & Maths Kits tc. for teaching modern subjects :
                                                                                            </div><!-- sub_table1 -->
                                                                                            <div class="sub_table2">                          
                                                                                                Computer rate :<?= $result[46] ?>
                                                                                            </div>
                                                                                            <div style="padding-bottom: 25px;padding-top:20px"></div>
                                                                                            <div class="sub_table2">
                                                                                                For Training :&nbsp;&nbsp;&nbsp; <?= $result[47] ?>                       
                                                                                            </div>
                                                                                            <div style="padding-bottom: 25px;padding-top:20px"></div>
                                                                                            <div class="sub_table2">
                                                                                                Annual Grand : <?= $result[48] ?>
                                                                                            </div>
                                                                                            <div style="padding-bottom: 25px;padding-top:20px"></div>        
                                                                                            <div class="sub_table2">
                                                                                                Library :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                                <?= $result[49] ?>                        
                                                                                            </div>                      
                                                                                            <div style="padding-bottom: 25px;padding-top:20px"></div>
                                                                                            <div class="sub_table2">
                                                                                                Total : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                                <?= $result[50] ?>
                                                                                            </div><!-- sub_table2 -->
                                                                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>

                                        <div class="main_table"> 
                                            <div class="sub_table1">                       
                                                <b>Requirements be given in Format II enclosed :</b> <br/>
                                                (Laboratories/Science labs/Computer labs are for secondary & senior secondary level only) 
                                            </div>
                                            <div class="sub_table2">
                                                <?= $result[51] ?>
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>

                                        <div class="main_table"> 
                                            <div class="sub_table1">                       
                                                (iii) For library books has a selction criteria been developed and a purchase commitee been set up by the madrassa ? :
                                            </div>
                                            <div class="sub_table2">
                                                <?php
                                                
                                                $library = $result[52] ;
                                                if ($library  == 'Yes') {
                                                    ?>
                                                    <?= $result[52]; ?><br/> 
                                                    <?= $result[53]; ?>
                                                    <?php
                                                }
                                                if ($library  == 'No') {
                                                    ?>
                                                    <?= $result[52]; ?>
                                                <?php }
                                                ?>     
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>

                                        <div class="main_table"> 
                                            <div class="sub_table1">                       
                                                (iv) Amount required by Madrassas opting for NIOS accrediation for academic stream in modern subjects :
                                            </div>
                                            <div class="sub_table2">
                                                <?= $result[54] ?>
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>

                                        <div class="main_table">                                              
                                            <b>Requirements be given in Format III enclosed :</b> 
                                        </div><!-- main_table -->

                                        <div class="main_table"> 
                                            <div class="sub_table1">                       
                                                (v) Amount required by Madrassas opting for NIOS accrediation for vocational stream :
                                            </div>
                                            <div class="sub_table2">
                                                <?= $result[55] ?>
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>

                                        <div class="main_table"> 
                                            <div class="sub_table1">                       
                                                10. Total Amount required :
                                            </div>
                                            <div class="sub_table2">
                                                
                                                For salary : <?=$result[56] ?> <br/>
                                                Others : <?= $result[57] ?>  <br/>                
                                                Total : <?= $result[58]; ?><br/>
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>

                                        <div class="main_table"> 
                                            <div class="sub_table1">                       
                                                (11) Whether the Madrasa is getting any financial assistance for teaching of modern subjects from an y other source. If so, the amount and the purpose for which it is intended, be mentioned. (No duplication should be done) :
                                            </div>
                                            <div class="sub_table2">
                                                <?= $result[59]; ?>
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>

                                        <div class="main_table"> 
                                            <div class="sub_table1">                       
                                                (12) Net amount requested from Government (10-11) :
                                            </div>
                                            <div class="sub_table2">
                                                Net Amount :  <?= $result[60];
                                                $word=$result[60];?><br/>
                                                In Words : 
                                                 </div><!-- sub_table2 -->
                                                  <div style="clear:both;"></div>
                                                 <div class="sub_table2_words">
                                                <? echo no_to_words($word).'&nbsp;Only.'; ?>
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>
 
                    <div class="main_table"> 
                        <div class="sub_table7">                       
                            Date : <br/>
                            Place : <br/>
                        </div>                         
                    </div><!-- main_table -->
                    
                     <div style="clear:both;"></div>
                     
                     <div class="main_table"> 
                        <div class="sub_table8">
                           Signature of President / Chairman / Secretary
                        </div><!-- sub_table2 -->
                    </div><!-- main_table -->
                    
                     <div style="clear:both;"></div>
                                    </div> <!--print_style-->
                                    <div class="noprint">
                                         <div style="margin-top: 40px;">
                    <div>
                        <!-- <input type="submit" name="submit" value="Print"  />-->
                    </div>
                        
                        <div style="float:right;margin-top: -20px;margin-right: 71px;">
                            <a href="print-format1.php?id=<?php echo $result[1]; ?>&fileno=<?php echo $result[2]; ?>&value=<?php echo $radio6;?>" onclick="javascript:window.print();">Print</a>
                        </div>

                   </div>
                                        <div>
                                          
                                        </div>       
                                        
                                    </div>
                                    </div>
                                    <?php
                               
                                    } else {
                                    if ($seracch == $fileno || $seracch=='' || $fileno=='') {
                                        echo "<div style='color:red;padding-top:10px;text-align:center;'>Invalid Application Id / No !</div>";
                                    }
                                }
                            }
                           /* if(isset($_POST['submit'])){
                               echo $id=$result[1];
                               echo $fileid=$result[2];
                                 echo "<script>window.location='print-format1.php?id=$id&fileno=$fileid'</script>";
                            }*/
                            ?>

                        </form> 
                    </div>
                   

</div>
</div>
</div>
<div id="footerouter3">
<div id="footer">© All Rights Reserved. IT@School, General Education Department, Govt of Kerala. </div>
</div>
</div>	

</body>
</html>
<?php function no_to_words($no)
{   
 $words = array('0'=> 'Zero' ,'1'=> 'One' ,'2'=> 'Two' ,'3' => 'Three','4' => 'Four','5' => 'Five','6' => 'Six','7' => 'Seven','8' => 'Eight','9' => 'Nine','10' => 'Ten','11' => 'Eleven','12' => 'Twelve','13' => 'Thirteen','14' => 'Fouteen','15' => 'Fifteen','16' => 'Sixteen','17' => 'Seventeen','18' => 'Eighteen','19' => 'Nineteen','20' => 'Twenty','30' => 'Thirty','40' => 'Fourty','50' => 'Fifty','60' => 'Sixty','70' => 'Seventy','80' => 'Eighty','90' => 'Ninty','100' => 'Hundred &','1000' => 'Thousand','100000' => 'Lakh','10000000' => 'Crore');
    if($no == 0)
        return 'Zero';
    else {
	$novalue='';
	$highno=$no;
	$remainno=0;
	$value=100;
	$value1=1000;       
            while($no>=100)    {
                if(($value <= $no) &&($no  < $value1))    {
                $novalue=$words["$value"];
                $highno = (int)($no/$value);
                $remainno = $no % $value;
                break;
                }
                $value= $value1;
                $value1 = $value * 100;
            }       
          if(array_key_exists("$highno",$words))
              return $words["$highno"]." ".$novalue." ".no_to_words($remainno);
          else {
             $unit=$highno%10;
             $ten =(int)($highno/10)*10;            
             return $words["$ten"]." ".$words["$unit"]." ".$novalue." ".no_to_words($remainno);
           }
    }
}?>

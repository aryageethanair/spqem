<?php 
session_start();
if(!($_SESSION['type']=='N' && $_SESSION['username'])){
    echo "<script>window.location='login.php'</script>";    
}
include 'library/dbconnect.php';
 $myusername=$_SESSION['username']; 		
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link rel="stylesheet" type="text/css" href="style.css" />
<script src="JSCal2/js/jscal2.js"></script>
	<script src="JSCal2/js/lang/en.js"></script>
	<link rel="stylesheet" type="text/css" href="JSCal2/css/jscal2.css" />
	<link rel="stylesheet" type="text/css" href="JSCal2/css/border-radius.css" />
	<link rel="stylesheet" type="text/css" href="JSCal2/css/steel/steel.css" />
<script src="JSCal2/js/gen_validatorv31.js" type="text/javascript"></script>
<script>
            function list_details(){
              //   alert("hai");
                var detail=$("#districts").val();
              //  alert("hai");
               // alert(detail);
                $.post('detailemployes.php',{detail:detail}).done(
                function(result){
                    if(result!='empty'){		
                        $("#res").html(result);
                    }
                });
            }


        </script>
 <script type="text/javascript" src="ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<style>
    .sub_table25{
        float: left;
width: 15%;
padding-left: 20px;
    }
    .sub_table26{
        float: right;
width: 82%;
padding-top: 6px;
    }
    </style>
</head>

<div id="container">
<div id="header">
	<div id="header_title">CENTRAL SPONSORED SCHEME FOR PROVIDING QUALITY EDUCATION IN MADRASA (SPQEM)</div>
</div>
<div id="menubar">
    <ul>
        <li><a href="DPI.php">Home</a></li>                
        <li><a href="ddacceptlist.php">Total No of Accepted List By DD</a></li>
        <li><a href="ddrejectlist.php">Total No of Rejected List By DD</a></li>
        <li><a href="takereport.php">Consolidated Report</a></li>
        <li><a href="logout.php">Logout</a></li>
        <li style="float: right;color: blue;">Welcome <?php echo $myusername;?></li>
    </ul>
</div>
<div id="body">    
        <div class="heading_head">ACCEPTED LISTS BY DD</div>
        <table  class="tab" id="content" border="1" style="border-color: #FFFFFF; font-size: 12px;text-align: center;" cellspacing="0" align="center" width="956" >		
	<div class="main_table">
                <div class="sub_table25">
                    Revenue District :
                </div><!-- sub_table1 -->
                <div class="sub_table26">                          
                    <select name="districts" id="districts" class="districts" onchange="javascrpt:list_details();">
                        <option value="">Select One</option> 
                        <option value="Thiruvanathapuram">Thiruvanathapuram</option> 
                        <option value="Kollam">Kollam</option>
                        <option value="Pathanamthitta">Pathanamthitta</option>
                        <option value="Alappuzha">Alappuzha</option>
                        <option value="Kottayam">Kottayam</option>
                        <option value="Idukki">Idukki</option>
                        <option value="Ernakulam">Ernakulam</option>
                        <option value="Thrissur">Thrissur</option>
                        <option value="Palakkad">Palakkad</option>
                        <option value="Malappuram">Malappuram</option>
                        <option value="Kozhikode">Kozhikode</option>
                        <option value="Wayanad">Wayanad</option>
                        <option value="Kannur">Kannur</option>
                        <option value="Kasargod">Kasargod</option>
                    </select>
                </div> <!-- sub_table2 -->
          </div><!-- main_table -->
           <div style="clear:both;"></div>
                <div id="res">
                </div>
        </table>   
</div>
<div id="footerouter">
<div id="footer">© All Rights Reserved. IT@School, General Education Department, Govt of Kerala. </div>
</div>
</div>

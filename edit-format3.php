<?php
include 'library/dbconnect.php';
//include_once("submit.inc.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link rel="stylesheet" type="text/css" href="style.css" />
        <script src="JSCal2/js/jscal2.js"></script>
        <script src="JSCal2/js/lang/en.js"></script>
        <link rel="stylesheet" type="text/css" href="JSCal2/css/jscal2.css" />
        <link rel="stylesheet" type="text/css" href="JSCal2/css/border-radius.css" />
        <link rel="stylesheet" type="text/css" href="JSCal2/css/steel/steel.css" />

        <script src="JSCal2/js/gen_validatorv31.js" type="text/javascript"></script>
        <script>
            var pathToImages = 'images/';
        </script>
        <script src="js/dhtmlgoodies_calendar.js"></script>
        <link rel="stylesheet" type="text/css" href="dhtmlgoodies_calendar.css"></link>
    </head>
    <body>
        <form name="forms" id="forms" method="post" action="">
            <div id="container">
                <div id="header">
                    <div id="header_title">CENTRAL SPONSORED SCHEME FOR PROVIDING QUALITY EDUCATION IN MADRASA (SPQEM)</div>


                </div>
                <div id="menubar"><ul>
                        <li><a href="index.php">Home</a></li>
                        <li><a href="input.php">New Application</a></li>
                        <li><a href="edit.php">Edit Application</a></li>
                        <li><a href="print.php">Print Application</a></li>
                        <li><a href="status.php">View Application Status</a></li>
                        <li><a href="login.php">Login</a></li>

                    </ul></div>
                <div id="body">

                    <div id="inner" id="inner">
                        <div id="middle1" style="padding:20px;">

                            <div><font color="RED"><?php //echo $msg;  ?></font></div>


                            <form action="" method="post" name="test">    
                         
                               <?php
                                 if(isset($_GET['id']) || isset($_POST['submit_format2'])){ 
                                    $serach=$_REQUEST['id'];
                                  $file_no=$_REQUEST['fileno'];
                                    $contact_exist = mysql_query("select * from wp_format3 where mater_application_id='$serach' and master_id_no='$file_no' and status=1");
                                    // echo "select * from wp_master where application_id='$serach'";
                                    if (mysql_num_rows($contact_exist) != 0) {
                                        $result = mysql_fetch_row($contact_exist);
                                        //print_r($result);
                                        //echo $result[5];
                                    }
                                }
                                if (isset($result))
                                    if ($result) {
                                        ?>
                                        <input type="hidden" name="application_no" id="name" value="<?= $result[1] ?>" />
                                        <input type="hidden" name="id_no" id="name" value="<?= $result[2] ?>" />
                                        <div style="clear:both;"></div>  
<div style="border: 1px solid black;padding: 10px;width: 910px;margin-top: 14px;">
      <div class="main_heading">
                        <div class="application">
                            <?php                     
                                echo "<b>FORMAT-III - NA</b>";
                            ?>
                        </div><!--application-->
                        <div class="part">
                            <?php                     
                                echo "Madrasas opting for NIOS <br/> Accrediation (Academic) <br/> (To be submitted under joint signature of Madrasa & NIOS)";
                            ?>
                        </div><!--part--> 
                    </div><!--main_heading-->   

                 <div style="clear:both;"></div>
                                        <div class="main_table">
                                            <div class="sub_table1">
                                                1. Name of Madrasa with full address/tehsil/distt State :
                                            </div><!-- sub_table1 -->
                                            <div class="sub_table2">
                                                <textarea readonly name="address_full" id="address_full" style="width: 209px;"><?= $result[3] ?></textarea>
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->    

                                        <div style="clear:both;"></div>  

                                        <div class="main_table">
                                            <div class="sub_table1">
                                                2. Date of accrediation with NIOS (attach photocopy od accrediation letter) :
                                            </div><!-- sub_table1 -->
                                            <div class="sub_table2">
                                                <input type="text" readonly onclick="displayCalendar(document.getElementById('dob'), 'dd/mm/yyyy', this);" name="date_accrediation" id="dob" value="<?= $result[4] ?>"> 
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->    

                                        <div style="clear:both;"></div>  

                                        <div class="main_table">
                                            <div class="sub_table1">
                                                3. No of students registered with NIOS (students per class to be given) :
                                            </div><!-- sub_table1 -->
                                            <div class="sub_table2">
                                                <textarea  readonly name="no_registered" id="no_registered" style="width: 209px;"><?= $result[5] ?></textarea>
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->    

                                        <div style="clear:both;"></div>

                                        <div class="main_table">
                                            <div class="sub_table1">
                                                4. <b>Requirement of funds </b>
                                            </div><!-- sub_table1 -->
                                        </div><!-- main_table -->    

                                        <div style="clear:both;"></div>

                                        <div class="main_table">
                                            <div class="sub_table1">
                                                (a). Accreditation fee £(copy of receipt be attached)
                                            </div><!-- sub_table1 -->                        

                                            <div style="clear:both;"></div>

                                            <div class="sub_table11">
                                                (i) Open basic Edn.(class 3,5 & 8) :
                                            </div><!-- sub_table2 -->

                                            <div class="sub_table12">
                                                Class 3 :&nbsp;&nbsp;
                                                Number of students: <input type="text" readonly name="no_stud3" id="class3" style="width:65px" value="<?= $result[6] ?>" onkeypress="return numbersonly(event)">                          
                                            </div><!-- sub_table2 -->

                                            <div style="clear:both;"></div>  

                                            <div class="sub_table12">
                                                Class 5 :&nbsp;&nbsp;
                                                Number of students: <input type="text" readonly name="no_stud5" id="class5" style="width:65px" value="<?= $result[7] ?>" onkeypress="return numbersonly(event)">                          
                                            </div><!-- sub_table2 -->

                                            <div style="clear:both;"></div>  

                                            <div class="sub_table12">
                                                Class 8 :&nbsp;&nbsp;
                                                Number of students: <input type="text" readonly name="no_stud8" id="class8" style="width:65px" value="<?= $result[8] ?>" onkeypress="return numbersonly(event)">                          
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->  

                                        <div style="clear:both;"></div>

                                        <div class="main_table">
                                            <div class="sub_table1">
                                                (b). Registration fees (includes cost of materials)
                                            </div><!-- sub_table1 -->                        

                                            <div style="clear:both;"></div>

                                            <div class="sub_table11">
                                                (i) No of Children :
                                            </div><!-- sub_table12 -->

                                            <div class="sub_table12">
                                                Boys : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="text" name="no_boys" id="no_boys" style="width:65px" value="<?= $result[9] ?>" onkeypress="return numbersonly(event)">                          

                                                    Girls : &nbsp;&nbsp;<input type="text" name="no_girls" id="no_girls" style="width:65px" value="<?= $result[10] ?>" onkeypress="return numbersonly(event)">                          
                                                        </div><!-- sub_table12 -->

                                                        <div style="clear:both;"></div>  

                                                        <div class="sub_table12">
                                                            CWSN** :&nbsp; <input type="text" name="cwsn" id="cwsn" style="width:65px" value="<?= $result[11] ?>" onkeypress="return numbersonly(event)">                          

                                                                Total : &nbsp;<input type="text" name="total" id="total" style="width:65px" value="<?= $result[12] ?>" onkeypress="return numbersonly(event)">                          
                                                                    </div><!-- sub_table12 -->

                                                                    <div style="clear:both;"></div>  

                                                                    <div class="sub_table12">
                                                                        Unit Cost : <input type="text" name="unit_cost" id="unit_cost" style="width:150px" value="<?= $result[13] ?>" onkeypress="return numbersonly(event)">                          
                                                                    </div><!-- sub_table12 -->

                                                                    <div style="clear:both;"></div>

                                                                    <div class="sub_table11">
                                                                        (ii) Amount required (Total) :
                                                                    </div><!-- sub_table11 -->

                                                                    <div class="sub_table12">
                                                                        <input type="text" name="total_cost" id="total_cost" style="width:240px" value="<?= $result[14] ?>" onkeypress="return numbersonly(event)">                          
                                                                    </div><!-- sub_table12 -->

                                                                    </div><!-- main_table -->   

                                                                    <div style="clear:both;"></div>  

                                                                    <div class="main_table">
                                                                        <div class="sub_table1">
                                                                            (c). Examination fees
                                                                        </div><!-- sub_table1 -->                        

                                                                        <div style="clear:both;"></div>

                                                                        <div class="sub_table11">
                                                                            (i) No of Children :
                                                                        </div><!-- sub_table11 -->

                                                                        <div class="sub_table12">
                                                                            <input type="text" name="no_child" id="no_child" value="<?= $result[15] ?>" onkeypress="return numbersonly(event)">                          
                                                                        </div><!-- sub_table12 -->

                                                                        <div style="clear:both;"></div> 

                                                                        <div class="sub_table11">
                                                                            (ii) Unit Cost :
                                                                        </div><!-- sub_table12 -->

                                                                        <div class="sub_table12">
                                                                            <input type="text" name="unit_costs" id="unit_costs" value="<?= $result[16] ?>" onkeypress="return numbersonly(event)">                          
                                                                        </div><!-- sub_table12 -->

                                                                        <div style="clear:both;"></div>

                                                                        <div class="sub_table11">
                                                                            (ii) Amount required :
                                                                        </div><!-- sub_table11 -->

                                                                        <div class="sub_table12">
                                                                            <input type="text" name="total_costs" id="total_costs" value="<?= $result[17] ?>" onkeypress="return numbersonly(event)">                          
                                                                        </div><!-- sub_table12 -->

                                                                    </div><!-- main_table -->   

                                                                    <div style="clear:both;"></div>  

                                                                    <div class="main_table">
                                                                        <div class="sub_table1">
                                                                            5. Grand total of Amt req.[5(a) + 5(b) + 5(c)] : 
                                                                        </div><!-- sub_table1 -->                        

                                                                        <div class="sub_table12">
                                                                            <input type="text" name="grand_costs" id="grand_costs" value="<?= $result[18] ?>" onkeypress="return numbersonly(event)">                          
                                                                        </div><!-- sub_table12 -->

                                                                    </div>
                                                                    <div style="clear:both;"></div>
                                                                     
                                                                                   <div style="margin-top: 40px;">
                    <div style="float: left;">
                        <input type="submit" name="submit_format3" value="Save" class="save_button1"/>
                    </div>
                         <div>
                        <input type="submit" name="submit_cancel" value="Cancel" class="cancel_button1"/>
                     </div> 
                        <div style="float:right;margin-top: -14px;">
                            <a href="edit-format4.php?id=<?php echo $result[1]; ?>&fileno=<?php echo $result[2]; ?>">Edit Next Form</a>
                        </div>

                   </div>
  <?php
                                                                } else {
                                                                    echo "<div style='color:red;font-weight:bold;padding-top:10px;text-align:center;'>Application not found!</div>";
                                                                }
                                                            ?>


                                                            <?php
                                                            if (isset($_POST['submit_format3'])) {
                                                                $slot = $_POST['application_no'];
                                                                $alocate = $_POST['id_no'];
                                                                $address_full = $_POST['address_full'];
                                                                $date_accrediation = $_POST['date_accrediation'];
                                                                $no_registered = $_POST['no_registered'];
                                                                $no_stud3 = $_POST['no_stud3'];
                                                                $no_stud5 = $_POST['no_stud5'];
                                                                $no_stud8 = $_POST['no_stud8'];
                                                                $no_boys = $_POST['no_boys'];
                                                                $no_girls = $_POST['no_girls'];
                                                                $cwsn = $_POST['cwsn'];
                                                                $total = $_POST['total'];
                                                                $unit_cost = $_POST['unit_cost'];
                                                                $total_cost = $_POST['total_cost'];
                                                                $no_child = $_POST['no_child'];
                                                                $unit_costs = $_POST['unit_costs'];
                                                                $total_costs = $_POST['total_costs'];
                                                                $grand_costs = $_POST['grand_costs'];

                                                                $sql = mysql_query("update wp_format3 set address_full='$address_full',date_accrediation='$date_accrediation',no_registered='$no_registered',no_stud3='$no_stud3',no_stud5='$no_stud5',no_stud8='$no_stud8',no_boys='$no_boys',no_girls='$no_girls',cwsn='$cwsn',total='$total',unit_cost='$unit_cost',total_cost='$total_cost',no_child='$no_child',unit_costs='$unit_costs',total_costs='$total_costs',grand_costs='$grand_costs' where mater_application_id=$slot and master_id_no=$alocate");
                                                                // echo "update wp_format4 set address_full='$address_full',date_accrediation='$date_accrediation',no_registered='$no_registered',no_stud3='$no_stud3',no_stud5='$no_stud5',no_stud8='$no_stud8',no_boys='$no_boys',no_girls='$no_girls',cwsn='$cwsn',total='total',unit_cost='$unit_cost',total_cost='$total_cost',no_child='$no_child',unit_costs='$unit_costs',total_costs='$total_costs',grand_costs='$grand_costs' where mater_application_id=$slot and master_id_no=$alocate";
                                                           echo "<script>window.location='edit-format4.php?id=$slot&fileno=$alocate'</script>";
                }?>
                <?php
        if(isset($_POST['submit_cancel'])){ 
     
     echo"<script>window.location='edit.php'</script>";
 }
                                                            ?>

                                                            </form> 
                                                            </div>
                                                            </div>
                                                            </div>
                                                            <div id="footerouter">
                                                                <div id="footer">© All Rights Reserved. IT@School, General Education Department, Govt of Kerala. </div>
                                                            </div>
                                                            </div>	
                                                            </form>
                                                            </body>
                                                            </html>

                                                            <script type="text/javascript">
                                                           function numbersonly(e) {
                                                               var keynum;
                                                               var keychar;
                                                               var numcheck;
                                                               if (window.event) // IE
                                                               {
                                                                   keynum = e.keyCode;
                                                               }
                                                               else if (e.which) // netscape/Firefox/opera
                                                               {
                                                                   keynum = e.which;
                                                               }

                                                               var tabkey = e.keyCode;

                                                               if (keynum == 8 || tabkey == 9 || tabkey == 37 || tabkey == 39)
                                                               {
                                                                   return true;
                                                               }

                                                               else
                                                               {
                                                                   keychar = String.fromCharCode(keynum);
                                                                   //numcheck = /\d/;
                                                                   numcheck = /[.0-9]/;
                                                                   return numcheck.test(keychar);
                                                               }
                                                           }
                                                            </script>

                                                            <script type="text/javascript">
                                                                function findTotal3() {
                                                                    var arr = document.getElementsByName('qty4[]');
                                                                    var tot = 0;
                                                                    for (var i = 0; i < arr.length; i++) {
                                                                        if (parseInt(arr[i].value))
                                                                            tot += parseInt(arr[i].value);
                                                                    }
                                                                    document.getElementById('totals').value = tot;
                                                                }
                                                            </script>
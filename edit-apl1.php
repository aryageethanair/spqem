<?php
include 'library/dbconnect.php';
//include_once("submit.inc.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link rel="stylesheet" type="text/css" href="style.css" />
<script src="JSCal2/js/jscal2.js"></script>
	<script src="JSCal2/js/lang/en.js"></script>
	<link rel="stylesheet" type="text/css" href="JSCal2/css/jscal2.css" />
	<link rel="stylesheet" type="text/css" href="JSCal2/css/border-radius.css" />
	<link rel="stylesheet" type="text/css" href="JSCal2/css/steel/steel.css" />
<script src="JSCal2/js/gen_validatorv31.js" type="text/javascript"></script>
<script>
var pathToImages = 'images/';
</script>
<script src="js/dhtmlgoodies_calendar.js"></script>
<link rel="stylesheet" type="text/css" href="dhtmlgoodies_calendar.css"></link>
 <script src="js/jquery.min.js"></script>
        <script src="js/jquery.validate.js"></script>
                <script type="text/javascript">
            $(document).ready(function() {

                $("#test").validate({
                    onkeyup: false,
                    onfocusout: function(element) {
                        $(element).valid();
                    },
                    onblur: function(element) {
                        $(element).valid();
                    },
                    rules: {
                      //  application_number: {
                        //    required: true,
                       // },  
                        
                        //file_no: {
                          //  required: true,
                       // },
                    },
                    errorPlacement: function(error, element) {
                        error.insertBefore(element);
                    }
                });

            });
        </script>
        <style type="text/css">
            .sub_table0 label{
                float: right;
                color: red;
            }
           .sub_table label{
                float: right;
                color: red;
                margin-right: 350px;
                margin-top: -7px;
            }
        </style>
</head>
<body>
	
<div id="container">
<div id="header">
	<div id="header_title">CENTRAL SPONSORED SCHEME FOR PROVIDING QUALITY EDUCATION IN MADRASA (SPQEM)</div>
  
  
</div>
<div id="menubar"><ul>
    	 	 <li><a href="index.php">Home</a></li>
         <li><a href="input.php">New Application</a></li>
         <li><a href="edit.php">Edit Application</a></li>
         <li><a href="print.php">Print Application</a></li>
         <li><a href="status.php">View Application Status</a></li>
         <li><a href="login.php">Login</a></li>
        
        </ul></div>
<div id="body">

<div id="inner" id="inner">
<div id="middle1" style="padding:20px;">

	<div><font color="RED"><?php //echo $msg; ?></font></div>
 
      <form action="" method="post" name="test" id="test" style="padding:20px;">    
                     
    <?php 
                 if(isset($_POST['submit_form_five'])){ 
                    $serach=$_POST['application_number'];
                    $file_no=$_POST['file_no'];
                    if(($serach=="")||($file_no=="")||($serach==$file_no))
                    {
						echo "<div style='color:red;padding-top:10px;text-align:center;'>Invalid Application Id / No !</div>";
					}
					else
					{
                   $contact_exist=mysql_query("select * from wp_master where application_id='$serach' and id_no='$file_no' and status=1");
                  // echo "select * from wp_master where application_id='$serach'";
                   	if(mysql_num_rows($contact_exist)!=0){
                            $result=mysql_fetch_row($contact_exist);
                            $seracch= $result[1];
                            $fileno= $result[2];
                            //echo $result[5];
                       }}
if(isset($result))
if($result)
{
 ?>
                   
                   <input type="hidden" name="application_no" id="name" value="<?=$result[1] ?>" />
                   <input type="hidden" name="id_no" id="name" value="<?=$result[2] ?>" />
                   <div style="border: 1px solid black;padding: 10px;width: 910px;margin-top: 14px;">
                         <div class="main_heading">
                                        <div class="application">
                                            <?php
                                            echo "(To be submitted in duplicate)<br/><b>Scheme for Providing Quality Education to Madrasas <br/>APPLICATION FORM</b>";
                                            ?>
                                        </div><!--application-->
                                        <div class="part">
                                            <?php
                                            echo "<b>PART I</b><br/>(To be filled by applicant)";
                                            ?>
                                        </div><!--part--> 
                                    </div><!--main_heading-->
                        <div style="clear:both;"></div>
                   <div class="main_table">
                        <div class="sub_table1">
                            1. <b>Name of organisation/Society</b> running the Madrasa * :
                            <br/>(With complete address)
                            </div><!-- sub_table1 -->
                        <div class="sub_table2">
                            <textarea name="society_address1" id="society_address1" style="width: 209px;"><?=$result[4] ?></textarea>
                        </div><!-- sub_table2 -->
                       </div><!-- main_table -->
                       
                       <div style="clear:both;"></div>
                       
                          <div class="main_table">
                        <div class="sub_table1">
                            2. <b>Name with address of Madrasa</b> seeking Financial Assistance :
                        </div><!-- sub_table1 -->
                      <div class="sub_table2">
                         <textarea name="name_address" id="name_address" style="width: 209px;"><?=$result[5] ?></textarea>
                      
                  </div> 
                    </div><!-- main_table -->
                   
                     <div style="clear:both;"></div>
                       <div class="main_table">
                        <div class="sub_table1">
                            (a). Select District :
                        </div><!-- sub_table1 -->
                        <div class="sub_table2">    
                            <?php  $state=$result[6] ?>
                               
                                   <?php 
                                   if($state=='Thiruvanathapuram'){?>
                             <select name="country" id="country" style="width: 209px;">
                                       <option value="">Select One</option> 
                                    <option value="Thiruvanathapuram" selected>Thiruvanathapuram</option> 
                                    <option value="Kollam">Kollam</option>
                                    <option value="Pathanamthitta">Pathanamthitta</option>
                                    <option value="Alappuzha">Alappuzha</option>
                                    <option value="Kottayam">Kottayam</option>
                                    <option value="Idukki">Idukki</option>
                                    <option value="Ernakulam">Ernakulam</option>
                                    <option value="Thrissur">Thrissur</option>
                                    <option value="Palakkad">Palakkad</option>
                                    <option value="Malappuram">Malappuram</option>
                                    <option value="Kozhikode">Kozhikode</option>
                                    <option value="Wayanad">Wayanad</option>
                                    <option value="Kannur">Kannur</option>
                                    <option value="Kasargod">Kasargod</option>
                             </select> 
                                    
                                  <?php }
                                   
                                   if($state=='Kollam'){?>
                                     <select name="country" id="country" style="width: 209px;">
                                       <option value="">Select One</option> 
                                    <option value="Thiruvanathapuram">Thiruvanathapuram</option> 
                                    <option value="Kollam" selected>Kollam</option>
                                    <option value="Pathanamthitta">Pathanamthitta</option>
                                    <option value="Alappuzha">Alappuzha</option>
                                    <option value="Kottayam">Kottayam</option>
                                    <option value="Idukki">Idukki</option>
                                    <option value="Ernakulam">Ernakulam</option>
                                    <option value="Thrissur">Thrissur</option>
                                    <option value="Palakkad">Palakkad</option>
                                    <option value="Malappuram">Malappuram</option>
                                    <option value="Kozhikode">Kozhikode</option>
                                    <option value="Wayanad">Wayanad</option>
                                    <option value="Kannur">Kannur</option>
                                    <option value="Kasargod">Kasargod</option>
                                     </select>
                                  <?php }
                                  if($state=='Pathanamthitta'){?>
                                     <select name="country" id="country" style="width: 209px;">
                                       <option value="">Select One</option> 
                                    <option value="Thiruvanathapuram">Thiruvanathapuram</option> 
                                    <option value="Kollam">Kollam</option>
                                    <option value="Pathanamthitta" selected>Pathanamthitta</option>
                                    <option value="Alappuzha">Alappuzha</option>
                                    <option value="Kottayam">Kottayam</option>
                                    <option value="Idukki">Idukki</option>
                                    <option value="Ernakulam">Ernakulam</option>
                                    <option value="Thrissur">Thrissur</option>
                                    <option value="Palakkad">Palakkad</option>
                                    <option value="Malappuram">Malappuram</option>
                                    <option value="Kozhikode">Kozhikode</option>
                                    <option value="Wayanad">Wayanad</option>
                                    <option value="Kannur">Kannur</option>
                                    <option value="Kasargod">Kasargod</option> 
                                     </select>
                                  <?php }
                                   if($state=='Alappuzha'){?>
                                     <select name="country" id="country" style="width: 209px;">
                                       <option value="">Select One</option> 
                                    <option value="Thiruvanathapuram">Thiruvanathapuram</option> 
                                    <option value="Kollam">Kollam</option>
                                    <option value="Pathanamthitta">Pathanamthitta</option>
                                    <option value="Alappuzha" selected>Alappuzha</option>
                                    <option value="Kottayam">Kottayam</option>
                                    <option value="Idukki">Idukki</option>
                                    <option value="Ernakulam">Ernakulam</option>
                                    <option value="Thrissur">Thrissur</option>
                                    <option value="Palakkad">Palakkad</option>
                                    <option value="Malappuram">Malappuram</option>
                                    <option value="Kozhikode">Kozhikode</option>
                                    <option value="Wayanad">Wayanad</option>
                                    <option value="Kannur">Kannur</option>
                                    <option value="Kasargod">Kasargod</option> 
                                     </select>
                                  <?php }
                                   if($state=='Kottayam'){?>
                                     <select name="country" id="country" style="width: 209px;">
                                       <option value="">Select One</option> 
                                    <option value="Thiruvanathapuram">Thiruvanathapuram</option> 
                                    <option value="Kollam">Kollam</option>
                                    <option value="Pathanamthitta">Pathanamthitta</option>
                                    <option value="Alappuzha">Alappuzha</option>
                                    <option value="Kottayam" selected>Kottayam</option>
                                    <option value="Idukki">Idukki</option>
                                    <option value="Ernakulam">Ernakulam</option>
                                    <option value="Thrissur">Thrissur</option>
                                    <option value="Palakkad">Palakkad</option>
                                    <option value="Malappuram">Malappuram</option>
                                    <option value="Kozhikode">Kozhikode</option>
                                    <option value="Wayanad">Wayanad</option>
                                    <option value="Kannur">Kannur</option>
                                    <option value="Kasargod">Kasargod</option> 
                                     </select>
                                  <?php }
                                  if($state=='Idukki'){?>
                                     <select name="country" id="country" style="width: 209px;">
                                       <option value="">Select One</option> 
                                    <option value="Thiruvanathapuram">Thiruvanathapuram</option> 
                                    <option value="Kollam">Kollam</option>
                                    <option value="Pathanamthitta">Pathanamthitta</option>
                                    <option value="Alappuzha">Alappuzha</option>
                                    <option value="Kottayam">Kottayam</option>
                                    <option value="Idukki" selected>Idukki</option>
                                    <option value="Ernakulam">Ernakulam</option>
                                    <option value="Thrissur">Thrissur</option>
                                    <option value="Palakkad">Palakkad</option>
                                    <option value="Malappuram">Malappuram</option>
                                    <option value="Kozhikode">Kozhikode</option>
                                    <option value="Wayanad">Wayanad</option>
                                    <option value="Kannur">Kannur</option>
                                    <option value="Kasargod">Kasargod</option> 
                                     </select>
                                  <?php }
                                    if($state=='Ernakulam'){?>
                                     <select name="country" id="country" style="width: 209px;">
                                    <option value="">Select One</option> 
                                    <option value="Thiruvanathapuram">Thiruvanathapuram</option> 
                                    <option value="Kollam">Kollam</option>
                                    <option value="Pathanamthitta">Pathanamthitta</option>
                                    <option value="Alappuzha">Alappuzha</option>
                                    <option value="Kottayam">Kottayam</option>
                                    <option value="Idukki">Idukki</option>
                                    <option value="Ernakulam" selected>Ernakulam</option>
                                    <option value="Thrissur">Thrissur</option>
                                    <option value="Palakkad">Palakkad</option>
                                    <option value="Malappuram">Malappuram</option>
                                    <option value="Kozhikode">Kozhikode</option>
                                    <option value="Wayanad">Wayanad</option>
                                    <option value="Kannur">Kannur</option>
                                    <option value="Kasargod">Kasargod</option> 
                                     </select>
                                  <?php }
                                   if($state=='Thrissur'){?>
                                     <select name="country" id="country" style="width: 209px;">
                                    <option value="">Select One</option> 
                                    <option value="Thiruvanathapuram">Thiruvanathapuram</option> 
                                    <option value="Kollam">Kollam</option>
                                    <option value="Pathanamthitta">Pathanamthitta</option>
                                    <option value="Alappuzha">Alappuzha</option>
                                    <option value="Kottayam">Kottayam</option>
                                    <option value="Idukki">Idukki</option>
                                    <option value="Ernakulam">Ernakulam</option>
                                    <option value="Thrissur" selected>Thrissur</option>
                                    <option value="Palakkad">Palakkad</option>
                                    <option value="Malappuram">Malappuram</option>
                                    <option value="Kozhikode">Kozhikode</option>
                                    <option value="Wayanad">Wayanad</option>
                                    <option value="Kannur">Kannur</option>
                                    <option value="Kasargod">Kasargod</option> 
                                     </select>
                                  <?php }
                                   if($state=='Palakkad'){?>
                                     <select name="country" id="country" style="width: 209px;">
                                    <option value="">Select One</option> 
                                    <option value="Thiruvanathapuram">Thiruvanathapuram</option> 
                                    <option value="Kollam">Kollam</option>
                                    <option value="Pathanamthitta">Pathanamthitta</option>
                                    <option value="Alappuzha">Alappuzha</option>
                                    <option value="Kottayam">Kottayam</option>
                                    <option value="Idukki">Idukki</option>
                                    <option value="Ernakulam">Ernakulam</option>
                                    <option value="Thrissur">Thrissur</option>
                                    <option value="Palakkad" selected>Palakkad</option>
                                    <option value="Malappuram">Malappuram</option>
                                    <option value="Kozhikode">Kozhikode</option>
                                    <option value="Wayanad">Wayanad</option>
                                    <option value="Kannur">Kannur</option>
                                    <option value="Kasargod">Kasargod</option> 
                                     </select>
                                  <?php }
                                  if($state=='Malappuram'){?>
                                     <select name="country" id="country" style="width: 209px;">
                                    <option value="">Select One</option> 
                                    <option value="Thiruvanathapuram">Thiruvanathapuram</option> 
                                    <option value="Kollam">Kollam</option>
                                    <option value="Pathanamthitta">Pathanamthitta</option>
                                    <option value="Alappuzha">Alappuzha</option>
                                    <option value="Kottayam">Kottayam</option>
                                    <option value="Idukki">Idukki</option>
                                    <option value="Ernakulam">Ernakulam</option>
                                    <option value="Thrissur">Thrissur</option>
                                    <option value="Palakkad">Palakkad</option>
                                    <option value="Malappuram" selected>Malappuram</option>
                                    <option value="Kozhikode">Kozhikode</option>
                                    <option value="Wayanad">Wayanad</option>
                                    <option value="Kannur">Kannur</option>
                                    <option value="Kasargod">Kasargod</option> 
                                     </select>
                                  <?php }
                                  if($state=='Kozhikode'){?>
                                     <select name="country" id="country" style="width: 209px;">
                                    <option value="">Select One</option> 
                                    <option value="Thiruvanathapuram">Thiruvanathapuram</option> 
                                    <option value="Kollam">Kollam</option>
                                    <option value="Pathanamthitta">Pathanamthitta</option>
                                    <option value="Alappuzha">Alappuzha</option>
                                    <option value="Kottayam">Kottayam</option>
                                    <option value="Idukki">Idukki</option>
                                    <option value="Ernakulam">Ernakulam</option>
                                    <option value="Thrissur">Thrissur</option>
                                    <option value="Palakkad">Palakkad</option>
                                    <option value="Malappuram">Malappuram</option>
                                    <option value="Kozhikode" selected>Kozhikode</option>
                                    <option value="Wayanad">Wayanad</option>
                                    <option value="Kannur">Kannur</option>
                                    <option value="Kasargod">Kasargod</option> 
                                     </select>
                                  <?php }
                                  if($state=='Wayanad'){?>
                                     <select name="country" id="country" style="width: 209px;">
                                    <option value="">Select One</option> 
                                    <option value="Thiruvanathapuram">Thiruvanathapuram</option> 
                                    <option value="Kollam">Kollam</option>
                                    <option value="Pathanamthitta">Pathanamthitta</option>
                                    <option value="Alappuzha">Alappuzha</option>
                                    <option value="Kottayam">Kottayam</option>
                                    <option value="Idukki">Idukki</option>
                                    <option value="Ernakulam">Ernakulam</option>
                                    <option value="Thrissur">Thrissur</option>
                                    <option value="Palakkad">Palakkad</option>
                                    <option value="Malappuram">Malappuram</option>
                                    <option value="Kozhikode">Kozhikode</option>
                                    <option value="Wayanad" selected>Wayanad</option>
                                    <option value="Kannur">Kannur</option>
                                    <option value="Kasargod">Kasargod</option> 
                                     </select>
                                  <?php }
                                  if($state=='Kannur'){?>
                                     <select name="country" id="country" style="width: 209px;">
                                    <option value="">Select One</option> 
                                    <option value="Thiruvanathapuram">Thiruvanathapuram</option> 
                                    <option value="Kollam">Kollam</option>
                                    <option value="Pathanamthitta">Pathanamthitta</option>
                                    <option value="Alappuzha">Alappuzha</option>
                                    <option value="Kottayam">Kottayam</option>
                                    <option value="Idukki">Idukki</option>
                                    <option value="Ernakulam">Ernakulam</option>
                                    <option value="Thrissur">Thrissur</option>
                                    <option value="Palakkad">Palakkad</option>
                                    <option value="Malappuram">Malappuram</option>
                                    <option value="Kozhikode">Kozhikode</option>
                                    <option value="Wayanad">Wayanad</option>
                                    <option value="Kannur" selected>Kannur</option>
                                    <option value="Kasargod">Kasargod</option> 
                                     </select>
                                  <?php }
                                   if($state=='Kasargod'){?>
                                     <select name="country" id="country" style="width: 209px;">
                                    <option value="">Select One</option> 
                                    <option value="Thiruvanathapuram">Thiruvanathapuram</option> 
                                    <option value="Kollam">Kollam</option>
                                    <option value="Pathanamthitta">Pathanamthitta</option>
                                    <option value="Alappuzha">Alappuzha</option>
                                    <option value="Kottayam">Kottayam</option>
                                    <option value="Idukki">Idukki</option>
                                    <option value="Ernakulam">Ernakulam</option>
                                    <option value="Thrissur">Thrissur</option>
                                    <option value="Palakkad">Palakkad</option>
                                    <option value="Malappuram">Malappuram</option>
                                    <option value="Kozhikode">Kozhikode</option>
                                    <option value="Wayanad">Wayanad</option>
                                    <option value="Kannur">Kannur</option>
                                    <option value="Kasargod" selected>Kasargod</option> 
                                     </select>
                                  <?php }
                                   ?>
                              
                  </div> <!-- sub_table2 -->
                    </div><!-- main_table -->
                   
                    <div style="clear:both;"></div>
                     <div class="main_table">
                      <div class="sub_table1">
                            (a). Email :
                        </div><!-- sub_table1 -->
                      <div class="sub_table2">
                           <input type="text"  name="email" id="email" value="<?=$result[7]?>">
                        </div><!-- sub_table2 -->
                    </div><!-- main_table -->
                    
                    <div style="clear:both;"></div>
                    
                      <div class="main_table">
                      <div class="sub_table1">
                            3. <b>Objectives and activities</b> {give brief history of the organization/society running the Madras(s)} :
                        </div><!-- sub_table1 -->
                      <div class="sub_table2">
                            <textarea name="obj_actv" id="society_address1" style="width: 209px;"><?=$result[8]?></textarea>
                        </div><!-- sub_table2 -->
                    </div><!-- main_table -->
                    
                    <div style="clear:both;"></div>
                      
                    <div class="main_table">
                        <div class="sub_table1">
                            4. <b>Whether Registered</b> under central or state WAKF Acts/State Madrasa Board or accredited center of NIOS. If yes,<b> Regn No.</b>( A copy of the registration / accrediation certificate may be attached) :
                        </div><!-- sub_table1 -->
                        <div class="sub_table2">
                            <?php 
                             $value=$result[9];
                            if($value=='Yes'){?>
                            <input type="radio" name="radio1" onclick="showText(0)" value="Yes" checked>Yes
                            <input type="radio" name="radio1" onclick="showText(1)" value="No">No<br/>  
                                <?php $selectvalue=$result[10];
                                if($selectvalue==1){?>
                                <select name="madrasa" id="madrasa" <?//=$result[10]?>>
                                    <option value="">Select One</option>
                                    <option value="1" selected>WAKF Acts/</option>
                                    <option value="2">Madrasa</option>
                                    <option value="3">NIOS</option>
                                    <option value="4">Others</option>
                                </select>
                                <?php } 
                                if($selectvalue==2){?>
                                <select name="madrasa" id="madrasa" <?//=$result[10]?>>
                                    <option value="">Select One</option>
                                    <option value="1">WAKF Acts/</option>
                                    <option value="2" selected>Madrasa</option>
                                    <option value="3">NIOS</option>
                                    <option value="4">Others</option>
                                </select>
                                <?php } 
                                if($selectvalue==3){?>
                                <select name="madrasa" id="madrasa" <?//=$result[10]?>>
                                    <option value="">Select One</option>
                                    <option value="1">WAKF Acts/</option>
                                    <option value="2">Madrasa</option>
                                    <option value="3" selected>NIOS</option>
                                    <option value="4">Others</option>
                                </select>
                                <?php }
                                 if($selectvalue==4){?>
                                <select name="madrasa" id="madrasa" <?//=$result[10]?>>
                                    <option value="">Select One</option>
                                    <option value="1">WAKF Acts/</option>
                                    <option value="2">Madrasa</option>
                                    <option value="3" >NIOS</option>
                                    <option value="4" selected>Others</option>
                                </select>
                                <?php } ?>
                                
                                <div style="padding-top:10px;"></div>
                               <input type="text" id="mytext" name="reg_no" onkeypress="return numbersonly(event)" value="<?=$result[11]?>">
                               <div style="padding-top:10px;"></div>
                               <input type="text" readonly  onclick="displayCalendar(document.getElementById('dob'),'dd/mm/yyyy',this);" name="reg_date" id="dob" value="<?=$result[12]?>">
                          <!--  <input type="text" id="mytext" name="reg_no" value="<?//=$result[10]?>" onkeypress="return numbersonly(event)">-->
                           <?php  }
                            if($value=='No'){?>
                                <input type="radio" name="radio1" onclick="showText(0)" value="Yes" >Yes
                                <input type="radio" name="radio1" onclick="showText(1)" value="No" checked>No<br/>                            
                                <input type="text" id="mytext" name="reg_no" hidden>
                               <?php }
                           ?>
                            
                        </div><!-- sub_table2 -->
                    </div><!-- main_table -->
                    
                     <div style="clear:both;"></div>
                     
                       <div class="main_table">
                        <div class="sub_table1">
                        5. <b>Specific educational activities in modern subjects of the Madrasa seeking financial assistance under the scheme.</b>                                          
                        </div><!-- sub_table1 -->
                        <div class="sub_table2">
                            <textarea name="educational" id="society_address1" style="width: 209px;"><?=$result[13]?></textarea>
                        </div><!-- sub_table2 -->                    
                    </div><!-- main_table -->
                    
                    <div style="clear:both;"></div>
                   
                      <div class="main_table">
                        <div class="sub_table1">
                            (a). Whether the Madrasa seeking financial assistance has any experience in teaching of subjects like science (Phy., chem.., Bio.), Maths , social studies (history, geography, civics etc.); Languages (State language/Hindi/English) etc.? If so, brief description may be given:
                        </div><!-- sub_table1 -->
                        <div class="sub_table2">
                            <?php
                             $values=$result[14];
                            if($value=='Yes'){?>
                            <input type="radio" name="radio2" onclick="showText1(0)" value="Yes" checked>Yes
                            <input type="radio" name="radio2" onclick="showText1(1)" value="No">No<br/> 
                            <textarea id="mytext1" name="financial" style="resize:none;"><?=$result[15]?></textarea>
                           <?php  }
                            if($value=='No'){?>
                                <input type="radio" name="radio2" onclick="showText1(0)" value="Yes" >Yes
                                <input type="radio" name="radio2" onclick="showText1(1)" value="No" checked>No<br/>
                                <textarea id="mytext1" name="financial" hidden style="resize:none;"></textarea>
                               <?php }
                           ?>
                        </div><!-- sub_table2 -->
                    </div><!-- main_table -->
                   
                   <div style="clear:both;"></div>
                   
                      <div class="main_table">
                        <div class="sub_table1">
                            (b). Whether State curriculum, NIOS or any other curriculum followed; please specify; :
                        </div><!-- sub_table1 -->
                        <div class="sub_table2">
                            <?php $nios=$result[16]?>
                            <?php if($nios==1){?>
                                <select name="nios" id="nios" class="nios">
                                <option value="">Select One</option>
                                <option value="1" selected>State curriculum</option>
                                <option value="2">NIOS curriculum</option>
                                <option value="3">Others</option>                                
                           </select>
                           <?php } if($nios==2){?>
                                <select name="nios" id="nios" class="nios">
                                <option value="">Select One</option>
                                <option value="1">State curriculum</option>
                                <option value="2" selected>NIOS curriculum</option>
                                <option value="3">Others</option>                                
                           </select>
                           <?php } if($nios==3){?>
                                <select name="nios" id="nios" class="nios">
                                <option value="">Select One</option>
                                <option value="1">State curriculum</option>
                                <option value="2">NIOS curriculum</option>
                                <option value="3" selected>Others</option>
                                <input type="text" name="curriculum" id="curriculum" value="<?=$result[17]?>"/>
                           </select>
                           <?php }?>
                           
                           
                            
                        </div><!-- sub_table2 -->
                        
                    </div><!-- main_table -->
                    
                    <div style="clear:both;"></div>
                   
                    <div class="main_table">
                        <div class="sub_table1">
                            (c). Number of children studying these subjects by class and by gender.If there are any children with special needs (disabled children), number and class may be mentioned :
                        </div><!-- sub_table1 -->
                        <div class="sub_table2">    
                            <?php 
                            $total=$result[18];
                            $newArray = explode(",",$total);
                            implode(" ",$newArray);
                            $boy= $newArray[0];
                            $girl= $newArray[1];
                            ?>
                                                  
                          Boys: <input onblur="findTotal()" type="text" name="qty[]" id="qty2" style="width: 65px;" value="<?=$boy?>" onkeypress="return numbersonly(event)"/>
                          Girls: <input onblur="findTotal()" type="text" name="qty[]" id="qty2" style="width: 65px;" value="<?=$girl?>" onkeypress="return numbersonly(event)"/>
                         <br/><br/>  Total : <input type="text" name="total" id="total" readonly style="width: 65px;" value="<?=$result[19];?>"/>
                        </div><!-- sub_table2 -->
                    </div><!-- main_table -->
                    
                    <div style="clear:both;"></div>
                   
                   
                      <?php 
                           $count_techer1=$result[20];
                           $qualification_teacher=$result[21];
                            $qualification_teacher1 = explode(",",$qualification_teacher);
                            implode(" ",$qualification_teacher1);
                             $qualification_teachers1= $qualification_teacher1[0]; 
                             $qualification_teachers2= $qualification_teacher1[1]; 
                            $qualification_teachers3= $qualification_teacher1[2];                            
                            if($count_techer1==1){?>
                            <div class="main_table">
                                <div class="sub_table1">
                                    (d). No. of teachers already working and teaching modern subjects. Please give details including year of recruitment; whether they are trained teachers (with pre-service qualifications as per NCTE norms); information be given disaggregated by level of teaching (primary/upper primary/secondary/ senior secondary subjects); Subject wise break up be also mentioned :
                                </div><!-- sub_table1 -->
                                <div class="sub_table2">
                                  <select name="drop1" id="drop1" class="drop1">
                                    <option value="1">1 Teacher</option>
                                    <option value="2">2 Teacher</option>
                                    <option value="3">3 Teacher</option>
                                </select><br/>
                                <input type="hidden" name="elmt_count" id="elmt_count" value="1">
                                <div id="elmt_cont" style="width:200px;min-height:50px;">
                                    <div id="elm1">
                                      <!--  <textarea name="qualification_teacher[]" id="qualification_teacher" style="width: 209px;">
                                <?//=$qualification_teachers1?></textarea>-->
                                
                                    <?php 
                                      if($qualification_teachers1==1){?>
                                          <select name="qualification_teacher[]" id="qualification_teacher" class="qualification_teacher">
                                   <option value="">SelectOne</option>
                                   <option value="1" selected>Post Graduation</option>
                                   <option value="2">Graduation</option>
                                   <option value="3">Plus Two</option>
                                   <option value="4">SSLC</option>
                                   <option value="5">Below SSLC</option>
                                </select>
                                      <?php } if($qualification_teachers1==2){?>
                                 <select name="qualification_teacher[]" id="qualification_teacher" class="qualification_teacher">
                                     <option value="">SelectOne</option>
                                   <option value="1">Post Graduation</option>
                                   <option value="2"  selected>Graduation</option>
                                   <option value="3">Plus Two</option>
                                   <option value="4">SSLC</option>
                                   <option value="5">Below SSLC</option>
                                </select>
                                      <?php }
                                      if($qualification_teachers1==3){?>
                                 <select name="qualification_teacher[]" id="qualification_teacher" class="qualification_teacher">
                                     <option value="">SelectOne</option>
                                   <option value="1">Post Graduation</option>
                                   <option value="2">Graduation</option>
                                   <option value="3"  selected>Plus Two</option>
                                   <option value="4">SSLC</option>
                                   <option value="5">Below SSLC</option>
                                </select>
                                      <?php }
                                       if($qualification_teachers1==4){?>
                                 <select name="qualification_teacher[]" id="qualification_teacher" class="qualification_teacher">
                                     <option value="">SelectOne</option>
                                   <option value="1">Post Graduation</option>
                                   <option value="2">Graduation</option>
                                   <option value="3">Plus Two</option>
                                   <option value="4"  selected>SSLC</option>
                                   <option value="5">Below SSLC</option>
                                </select>
                                      <?php }
                                       if($qualification_teachers1==5){?>
                                 <select name="qualification_teacher[]" id="qualification_teacher" class="qualification_teacher">
                                     <option value="">SelectOne</option>
                                   <option value="1">Post Graduation</option>
                                   <option value="2">Graduation</option>
                                   <option value="3">Plus Two</option>
                                   <option value="4">SSLC</option>
                                   <option value="5" selected>Below SSLC</option>
                                </select>
                                      <?php }
                                      ?>
                                    </div>
                                </div>                          
                                </div><!-- sub_table2 -->
                            </div><!-- main_table -->
                    <?php }
                    
                      if($count_techer1==2){?>
                            <div class="main_table">
                                <div class="sub_table1">
                                    (d). No. of teachers already working and teaching modern subjects. Please give details including year of recruitment; whether they are trained teachers (with pre-service qualifications as per NCTE norms); information be given disaggregated by level of teaching (primary/upper primary/secondary/ senior secondary subjects); Subject wise break up be also mentioned :
                                </div><!-- sub_table1 -->
                                <div class="sub_table2">
                                  <select name="drop1" id="drop1" class="drop1">
                                    <option value="1">1 Teacher</option>
                                    <option value="2" selected>2 Teacher</option>
                                    <option value="3">3 Teacher</option>
                                </select><br/>
                                <input type="hidden" name="elmt_count" id="elmt_count" value="1">
                                <div id="elmt_cont" style="width:200px;min-height:50px;">
                                    <div id="elm2">
                                        <!--<textarea name="qualification_teacher[]" id="qualification_teacher" style="width: 209px;"><?//=$qualification_teachers1?></textarea>
                                        <textarea name="qualification_teacher[]" id="qualification_teacher" style="width: 209px;"><?//=$qualification_teachers2?></textarea>-->
                                      <?php 
                                      if($qualification_teachers2==1){?>
                                          <select name="qualification_teacher[]" id="qualification_teacher" class="qualification_teacher">
                                   <option value="">SelectOne</option>
                                   <option value="1" selected>Post Graduation</option>
                                   <option value="2">Graduation</option>
                                   <option value="3">Plus Two</option>
                                   <option value="4">SSLC</option>
                                   <option value="5">Below SSLC</option>
                                </select>
                                         <select name="qualification_teacher[]" id="qualification_teacher" class="qualification_teacher">
                                   <option value="">SelectOne</option>
                                   <option value="1" selected>Post Graduation</option>
                                   <option value="2">Graduation</option>
                                   <option value="3">Plus Two</option>
                                   <option value="4">SSLC</option>
                                   <option value="5">Below SSLC</option>
                                </select> 
                                   <?php   }
                                   if($qualification_teachers2==2){?>
                                          <select name="qualification_teacher[]" id="qualification_teacher" class="qualification_teacher">
                                   <option value="">SelectOne</option>
                                   <option value="1">Post Graduation</option>
                                   <option value="2"  selected>Graduation</option>
                                   <option value="3">Plus Two</option>
                                   <option value="4">SSLC</option>
                                   <option value="5">Below SSLC</option>
                                </select>
                                         <select name="qualification_teacher[]" id="qualification_teacher" class="qualification_teacher">
                                   <option value="">SelectOne</option>
                                   <option value="1">Post Graduation</option>
                                   <option value="2" selected>Graduation</option>
                                   <option value="3">Plus Two</option>
                                   <option value="4">SSLC</option>
                                   <option value="5">Below SSLC</option>
                                </select> 
                                   <?php   }
                                    if($qualification_teachers2==3){?>
                                          <select name="qualification_teacher[]" id="qualification_teacher" class="qualification_teacher">
                                   <option value="">SelectOne</option>
                                   <option value="1">Post Graduation</option>
                                   <option value="2">Graduation</option>
                                   <option value="3" selected>Plus Two</option>
                                   <option value="4">SSLC</option>
                                   <option value="5">Below SSLC</option>
                                </select>
                                         <select name="qualification_teacher[]" id="qualification_teacher" class="qualification_teacher">
                                   <option value="">SelectOne</option>
                                   <option value="1">Post Graduation</option>
                                   <option value="2">Graduation</option>
                                   <option value="3" selected>Plus Two</option>
                                   <option value="4">SSLC</option>
                                   <option value="5">Below SSLC</option>
                                </select> 
                                   <?php   }
                                    if($qualification_teachers2==4){?>
                                          <select name="qualification_teacher[]" id="qualification_teacher" class="qualification_teacher">
                                   <option value="">SelectOne</option>
                                   <option value="1">Post Graduation</option>
                                   <option value="2">Graduation</option>
                                   <option value="3">Plus Two</option>
                                   <option value="4" selected>SSLC</option>
                                   <option value="5">Below SSLC</option>
                                </select>
                                         <select name="qualification_teacher[]" id="qualification_teacher" class="qualification_teacher">
                                   <option value="">SelectOne</option>
                                   <option value="1">Post Graduation</option>
                                   <option value="2">Graduation</option>
                                   <option value="3">Plus Two</option>
                                   <option value="4" selected>SSLC</option>
                                   <option value="5">Below SSLC</option>
                                </select> 
                                   <?php   }
                                   if($qualification_teachers2==5){?>
                                          <select name="qualification_teacher[]" id="qualification_teacher" class="qualification_teacher">
                                   <option value="">SelectOne</option>
                                   <option value="1">Post Graduation</option>
                                   <option value="2">Graduation</option>
                                   <option value="3">Plus Two</option>
                                   <option value="4">SSLC</option>
                                   <option value="5" selected>Below SSLC</option>
                                </select>
                                         <select name="qualification_teacher[]" id="qualification_teacher" class="qualification_teacher">
                                   <option value="">SelectOne</option>
                                   <option value="1">Post Graduation</option>
                                   <option value="2">Graduation</option>
                                   <option value="3">Plus Two</option>
                                   <option value="4">SSLC</option>
                                   <option value="5" selected>Below SSLC</option>
                                </select> 
                                   <?php   }
                                      ?>
                                       
                                    </div>
                                </div>                          
                                </div><!-- sub_table2 -->
                            </div><!-- main_table -->
                           
                    <?php } 
                     if($count_techer1==3){?>
                            <div class="main_table">
                                <div class="sub_table1">
                                    (d). No. of teachers already working and teaching modern subjects. Please give details including year of recruitment; whether they are trained teachers (with pre-service qualifications as per NCTE norms); information be given disaggregated by level of teaching (primary/upper primary/secondary/ senior secondary subjects); Subject wise break up be also mentioned :
                                </div><!-- sub_table1 -->
                                <div class="sub_table2">
                                  <select name="drop1" id="drop1" class="drop1">
                                    <option value="1">1 Teacher</option>
                                    <option value="2" >2 Teacher</option>
                                    <option value="3" selected>3 Teacher</option>
                                </select><br/>
                                <input type="hidden" name="elmt_count" id="elmt_count" value="1">
                                <div id="elmt_cont" style="width:200px;min-height:50px;">
                                    <div id="elm3">
                                       <!-- <textarea name="qualification_teacher[]" id="qualification_teacher" style="width: 209px;"><?//=$qualification_teachers1?></textarea>
                                        <textarea name="qualification_teacher[]" id="qualification_teacher" style="width: 209px;"><?//=$qualification_teachers2?></textarea>
                                    <textarea name="qualification_teacher[]" id="qualification_teacher" style="width: 209px;"><?//=$qualification_teachers3?></textarea>-->
                                    <?php if($qualification_teachers3==1){?>
                                          <select name="qualification_teacher[]" id="qualification_teacher" class="qualification_teacher">
                                   <option value="">SelectOne</option>
                                   <option value="1" selected>Post Graduation</option>
                                   <option value="2">Graduation</option>
                                   <option value="3">Plus Two</option>
                                   <option value="4">SSLC</option>
                                   <option value="5">Below SSLC</option>
                                </select>
                                         <select name="qualification_teacher[]" id="qualification_teacher" class="qualification_teacher">
                                   <option value="">SelectOne</option>
                                   <option value="1" selected>Post Graduation</option>
                                   <option value="2">Graduation</option>
                                   <option value="3">Plus Two</option>
                                   <option value="4">SSLC</option>
                                   <option value="5">Below SSLC</option>
                                </select> 
                                         <select name="qualification_teacher[]" id="qualification_teacher" class="qualification_teacher">
                                   <option value="">SelectOne</option>
                                   <option value="1" selected>Post Graduation</option>
                                   <option value="2">Graduation</option>
                                   <option value="3">Plus Two</option>
                                   <option value="4">SSLC</option>
                                   <option value="5">Below SSLC</option>
                                </select> 
                                   <?php   }
                                   if($qualification_teachers3==2){?>
                                          <select name="qualification_teacher[]" id="qualification_teacher" class="qualification_teacher">
                                   <option value="">SelectOne</option>
                                   <option value="1">Post Graduation</option>
                                   <option value="2"  selected>Graduation</option>
                                   <option value="3">Plus Two</option>
                                   <option value="4">SSLC</option>
                                   <option value="5">Below SSLC</option>
                                </select>
                                         <select name="qualification_teacher[]" id="qualification_teacher" class="qualification_teacher">
                                   <option value="">SelectOne</option>
                                   <option value="1">Post Graduation</option>
                                   <option value="2" selected>Graduation</option>
                                   <option value="3">Plus Two</option>
                                   <option value="4">SSLC</option>
                                   <option value="5">Below SSLC</option>
                                </select> 
                                         <select name="qualification_teacher[]" id="qualification_teacher" class="qualification_teacher">
                                   <option value="">SelectOne</option>
                                   <option value="1">Post Graduation</option>
                                   <option value="2" selected>Graduation</option>
                                   <option value="3">Plus Two</option>
                                   <option value="4">SSLC</option>
                                   <option value="5">Below SSLC</option>
                                </select> 
                                   <?php   }
                                    if($qualification_teachers3==3){?>
                                          <select name="qualification_teacher[]" id="qualification_teacher" class="qualification_teacher">
                                   <option value="">SelectOne</option>
                                   <option value="1">Post Graduation</option>
                                   <option value="2">Graduation</option>
                                   <option value="3" selected>Plus Two</option>
                                   <option value="4">SSLC</option>
                                   <option value="5">Below SSLC</option>
                                </select>
                                         <select name="qualification_teacher[]" id="qualification_teacher" class="qualification_teacher">
                                   <option value="">SelectOne</option>
                                   <option value="1">Post Graduation</option>
                                   <option value="2">Graduation</option>
                                   <option value="3" selected>Plus Two</option>
                                   <option value="4">SSLC</option>
                                   <option value="5">Below SSLC</option>
                                </select> 
                                          <select name="qualification_teacher[]" id="qualification_teacher" class="qualification_teacher">
                                   <option value="">SelectOne</option>
                                   <option value="1">Post Graduation</option>
                                   <option value="2">Graduation</option>
                                   <option value="3" selected>Plus Two</option>
                                   <option value="4">SSLC</option>
                                   <option value="5">Below SSLC</option>
                                </select> 
                                   <?php   }
                                    if($qualification_teachers3==4){?>
                                          <select name="qualification_teacher[]" id="qualification_teacher" class="qualification_teacher">
                                   <option value="">SelectOne</option>
                                   <option value="1">Post Graduation</option>
                                   <option value="2">Graduation</option>
                                   <option value="3">Plus Two</option>
                                   <option value="4" selected>SSLC</option>
                                   <option value="5">Below SSLC</option>
                                </select>
                                         <select name="qualification_teacher[]" id="qualification_teacher" class="qualification_teacher">
                                   <option value="">SelectOne</option>
                                   <option value="1">Post Graduation</option>
                                   <option value="2">Graduation</option>
                                   <option value="3">Plus Two</option>
                                   <option value="4" selected>SSLC</option>
                                   <option value="5">Below SSLC</option>
                                </select> 
                                            <select name="qualification_teacher[]" id="qualification_teacher" class="qualification_teacher">
                                   <option value="">SelectOne</option>
                                   <option value="1">Post Graduation</option>
                                   <option value="2">Graduation</option>
                                   <option value="3">Plus Two</option>
                                   <option value="4" selected>SSLC</option>
                                   <option value="5">Below SSLC</option>
                                </select> 
                                   <?php   }
                                   if($qualification_teachers3==5){?>
                                          <select name="qualification_teacher[]" id="qualification_teacher" class="qualification_teacher">
                                   <option value="">SelectOne</option>
                                   <option value="1">Post Graduation</option>
                                   <option value="2">Graduation</option>
                                   <option value="3">Plus Two</option>
                                   <option value="4">SSLC</option>
                                   <option value="5" selected>Below SSLC</option>
                                </select>
                                         <select name="qualification_teacher[]" id="qualification_teacher" class="qualification_teacher">
                                   <option value="">SelectOne</option>
                                   <option value="1">Post Graduation</option>
                                   <option value="2">Graduation/option>
                                   <option value="3">Plus Two</option>
                                   <option value="4">SSLC</option>
                                   <option value="5" selected>Below SSLC</option>
                                </select> 
                                         <select name="qualification_teacher[]" id="qualification_teacher" class="qualification_teacher">
                                   <option value="">SelectOne</option>
                                   <option value="1">Post Graduation</option>
                                   <option value="2">Graduation</option>
                                   <option value="3">Plus Two</option>
                                   <option value="4">SSLC</option>
                                   <option value="5" selected>Below SSLC</option>
                                </select> 
                                   <?php   }
                                      ?>
                                  
                                    </div>
                                </div>                          
                                </div><!-- sub_table2 -->
                            </div><!-- main_table -->
                           
                    <?php } ?>
<!--
                     <script> $("#drop1 option[value='<?php //echo $result[21];?>']").attr('selected', 'selected') </script>
-->
                    <div style="clear:both;"></div>
                    
                    
                     <div style="clear:both;"></div>
                     <div class="main_table">
                        <div class="sub_table1">
                            (f). School Level  :
                        </div><!-- sub_table1 -->
                        <div class="sub_table2">
                           
                                  <?php  $school_level=$result[22];
                                          if($school_level==1){?>
                                             <select name="school_level" id="school_level" onclick="schoollevel();">
                                                    <option value="">Select One</option>
                                                    <option value="1" selected>Primary Level</option>
                                                    <option value="2">Upper Primary Level</option>
                                                    <option value="3">Secondary Level</option>
                                                    <option value="4">Sr.Secondary Level</option>
                                                </select> 
                                     <?php     } if($school_level==2){?>
                                             <select name="school_level" id="school_level" onclick="schoollevel();">
                                                    <option value="">Select One</option>
                                                    <option value="1">Primary Level</option>
                                                    <option value="2" selected>Upper Primary Level</option>
                                                    <option value="3">Secondary Level</option>
                                                    <option value="4">Sr.Secondary Level</option>
                                                </select> 
                                     <?php     }if($school_level==3){?>
                                             <select name="school_level" id="school_level" onclick="schoollevel();">
                                                    <option value="">Select One</option>
                                                    <option value="1">Primary Level</option>
                                                    <option value="2">Upper Primary Level</option>
                                                    <option value="3" selected>Secondary Level</option>
                                                    <option value="4">Sr.Secondary Level</option>
                                                </select> 
                                     <?php     }if($school_level==4){?>
                                             <select name="school_level" id="school_level" onclick="schoollevel();">
                                                    <option value="">Select One</option>
                                                    <option value="1">Primary Level</option>
                                                    <option value="2">Upper Primary Level</option>
                                                    <option value="3">Secondary Level</option>
                                                    <option value="4" selected>Sr.Secondary Level</option>
                                                </select> 
                                     <?php     }
                                          ?>
                                                
                                              <!--<input type="text" name="school_level" id="school_level"/>-->
                                       
                         
                        </div><!-- sub_table2 -->
                    </div><!-- main_table --> 
                    
                     <div style="clear:both;"></div>
                     
                      <div class="main_table">                        
                        6. <b>Infrastructure details of the Madrasa</b>  
                    </div><!-- main_table -->
                    
                    <div style="clear:both;"></div>
                      
                     <div class="main_table">
                        <div class="sub_table1">
                            (a). Whether the Madrasa is located in its own or rented building? Give details :
                        </div><!-- sub_table1 -->
                        <div class="sub_table2">   
                         <?php
                             $rent=$result[23];
                            if($rent=='own'){?>
                            <input type="radio" name="radio4"  value="own" checked>Own
                            <input type="radio" name="radio4"  value="rent">Rent<br/> 
                            <textarea name="located" id="located" onclick="teachercount()"><?=$result[24];?></textarea>
                           <?php  }
                            if($rent=='rent'){?>
                                <input type="radio" name="radio4" value="own">Own
                                <input type="radio" name="radio4" value="rent" checked>Rent<br/>
                                <textarea name="located" id="located" onclick="teachercount()"><?=$result[24];?></textarea>
                               <?php }
                           ?>
                           
                        </div><!-- sub_table2 -->
                    </div><!-- main_table --> 
                    
                   <div style="clear:both;"></div>
                   
                   <div class="main_table">
                        <div class="sub_table1">
                            (b). No of rooms available for teaching & administrative purposes :
                        </div><!-- sub_table1 -->
                        <div class="sub_table2">                        
                          <input type="text" name="room_avaliable" id="room_avaliable" value="<?=$result[25];?>" onkeypress="return numbersonly(event)">
                        </div><!-- sub_table2 -->
                    </div><!-- main_table -->  
                    
                    <div style="clear:both;"></div>
                    
                      <div class="main_table">
                        <div class="sub_table1">
                            (c). If the present accommodation sufficient for the teaching of traditional as well as modern subjects ? Give details :
                        </div><!-- sub_table1 -->
                        <div class="sub_table2">
                            
                              <?php
                            $accommodation=$result[26];
                            if($accommodation=='No'){?>
                            <input type="radio" name="radio3" onclick="showText2(0)" value="Yes" >Yes
                            <input type="radio" name="radio3" onclick="showText2(1)" value="No" checked>No<br/> 
                            <textarea id="mytext2" name="accommodation" hidden style="resize:none;" style="width: 209px;"></textarea>
                           <?php  }
                            if($accommodation=='Yes'){?>
                                <input type="radio" name="radio3" onclick="showText2(0)" value="Yes" checked>Yes
                                <input type="radio" name="radio3" onclick="showText2(1)" value="No">No<br/>                                
                                <textarea id="mytext2" name="accommodation" style="resize:none;" style="width: 209px;"><?=$result[27]?></textarea>
                               <?php }
                           ?>                            
                            
                        </div><!-- sub_table2 -->
                    </div><!-- main_table -->
                    
                    <div style="clear:both;"></div>

                    <div class="main_table">
                        <div class="sub_table1">
                            (d). Whether the Madarssa has a seperate room(s) for Science Laboratories & Computer education labs etc. [Applicable only for Secondary/ Sr.secondary level Madarassas(s)] Give details :
                        </div><!-- sub_table1 -->
                        <div class="sub_table2">
                            <?php
                             $seperate_room=$result[28];
                            if($seperate_room=='No'){?>
                            <input type="radio" name="radio5" onclick="showText3(0)" value="No" checked>No
                            <input type="radio" name="radio5" onclick="showText3(1)" value="Yes" >Yes<br/> 
                             <textarea id="mytext3" name="seperate_room" style="resize:none;" style="width: 209px;"><?=$result[29];?></textarea>
                           <?php  }
                            if($seperate_room=='Yes'){?>
                                <input type="radio" name="radio5" onclick="showText3(0)" value="No">No
                                <input type="radio" name="radio5" onclick="showText3(1)" value="Yes" checked>Yes<br/>                                
                                <textarea id="mytext3" name="seperate_room" hidden style="resize:none;" style="width: 209px;"></textarea>
                               <?php }
                           ?>         
                           
                        </div><!-- sub_table2 -->
                    </div><!-- main_table -->
                   
                    <div style="clear:both;"></div>
                    
                    
                      <div class="main_table">                        
                        7. <b>Accreditation with NIOS</b>  
                    </div><!-- main_table -->
                    
                     <div class="main_table">   
                         <div class="sub_table1">
                              (a). If already accrediated by NIOS give details of 
                         </div><!-- sub_table1 -->
                         
                              <?php $radio6=$result[30];
                              if($radio6=='Yes'){?>
                          <div class="sub_table2">
                             <input type="radio" onclick="javascript:yesnoCheck();" name="radio6" id="yesCheck" value="Yes" checked/>Yes
                              <input type="radio" onclick="javascript:yesnoCheck();" name="radio6" id="noCheck" value="No"/>No  
                               </div><!-- sub_table2 -->
                                                        </div><!-- main_table -->  

                                                        <div style="clear:both;"></div>
                             <div id="ifYes">       
                                                                                            <div class="main_table">
                                                                                                <div class="sub_table1">
                                                                                                    (i). Number of students registered with NIOS :                            
                                                                                                </div><!-- sub_table1 -->
                                                                                                <div class="sub_table2">
                                                                                                    <input type="text" name="resgistered" id="resgistered" onkeypress="return numbersonly(event)" value="<?=$result[31]?>">
                                                                                                </div><!-- sub_table2 -->
                                                                                            </div><!-- main_table -->

                                                                                            <div style="clear:both;"></div>

                                                                                            <div class="main_table">
                                                                                                <div class="sub_table1">
                                                                                                    (ii). Number of students who have obtained certification from NIOS for class 3,5,8,10 and 12 sepeartely for each of the year of accrediation :

                                                                                                </div><!-- sub_table1 -->
                                                                                                <div class="sub_table2">
                                                                                                    Class 3 :&nbsp;&nbsp;<br/>
                                                                                                    Number of students:<br/>
                                                                                                    <input type="text" name="no_stud_cert" id="class3" onkeypress="return numbersonly(event)" value="<?=$result[32]?>">
                                                                                                        <input type="hidden" name="year_passing" id="year_passing"/>
                                                                                                </div><!-- sub_table2 -->
                                                                                                <div style="clear:both;"></div>                         

                                                                                                <div class="sub_table2">
                                                                                                    Class 5 :&nbsp;&nbsp;<br/>
                                                                                                    Number of students:<br/>
                                                                                                    <input type="text"  name="no_stud_cert1" id="class5"  onkeypress="return numbersonly(event)"  value="<?=$result[34]?>">
                                                                                                        <input type="hidden" name="year_passing1" id="year_passing1"/>
                                                                                                </div><!-- sub_table2 -->
                                                                                                <div style="clear:both;"></div>

                                                                                                <div class="sub_table2">
                                                                                                    Class 8 :&nbsp;&nbsp;<br/>
                                                                                                    Number of students: <br/>
                                                                                                    <input type="text"  name="no_stud_cert2" id="class8"  onkeypress="return numbersonly(event)" value="<?=$result[36]?>">
                                                                                                        <input type="hidden" name="year_passing2" id="year_passing2" />
                                                                                                </div><!-- sub_table2 -->
                                                                                                <div style="clear:both;"></div>                        

                                                                                                <div class="sub_table2">
                                                                                                    Class 10 :<br/>
                                                                                                    Number of students:<br/>
                                                                                                    <input type="text"name="no_stud_cert3" id="class10" onkeypress="return numbersonly(event)" value="<?=$result[38]?>">
                                                                                                        <input type="hidden" name="year_passing3" id="year_passing3" />
                                                                                                </div><!-- sub_table2 -->
                                                                                                <div style="clear:both;"></div>                        

                                                                                                <div class="sub_table2">
                                                                                                    Class 12 :<br/>
                                                                                                    Number of students: <br/>
                                                                                                    <input type="text" name="no_stud_cert4" id="class12"  onkeypress="return numbersonly(event)" value="<?=$result[40]?>">
                                                                                                        <input type="hidden" name="year_passing4" id="year_passing4" />
                                                                                                </div><!-- sub_table2 -->
                                                                                            </div><!-- main_table -->

                                                                                            <div style="clear:both;"></div>

                                                                                            <div class="main_table">
                                                                                                <div class="sub_table1">
                                                                                                    (b). If not yet accrediated, whether the Madrassa seeking financial assistance is interested in NIOS accrediation ? If so, whether applied to NIOS (refernce number of application be given) and by when NIOS accrediation is expected for (a) academic stream (class 3,5,8,10 & 12) and /or (b) vocational stream (Secondary & Sr. seconadry)) :

                                                                                                </div><!-- sub_table1 -->
                                                                                                <div class="sub_table2">
                                                                                                    <textarea name="financila_assisment" id="financila_assisment" style="width: 209px;"><?=$result[42]?></textarea>
                                                                                                </div><!-- sub_table2 -->
                                                                                            </div><!-- main_table -->
                                                                                        </div>
                                 <?php }
                              if($radio6=='No'){?>
                                                        <div class="main_table">
                          <div class="sub_table2">
                               <input type="radio" onclick="javascript:yesnoCheck();" name="radio6" id="yesCheck" value="Yes"/>Yes
                              <input type="radio" onclick="javascript:yesnoCheck();" name="radio6" id="noCheck" value="No" checked/>No
                              </div><!-- sub_table2 -->
                     </div><!-- main_table -->
                     <div style="clear:both;"></div>
                                                                                  <div id="ifYes" style="display:none">       
                                                                                            <div class="main_table">
                                                                                                <div class="sub_table1">
                                                                                                    (i). Number of students registered with NIOS :                            
                                                                                                </div><!-- sub_table1 -->
                                                                                                <div class="sub_table2">
                                                                                                    <input type="text" name="resgistered" id="resgistered" onkeypress="return numbersonly(event)">
                                                                                                </div><!-- sub_table2 -->
                                                                                            </div><!-- main_table -->

                                                                                            <div style="clear:both;"></div>

                                                                                            <div class="main_table">
                                                                                                <div class="sub_table1">
                                                                                                    (ii). Number of students who have obtained certification from NIOS for class 3,5,8,10 and 12 sepeartely for each of the year of accrediation :

                                                                                                </div><!-- sub_table1 -->
                                                                                                <div class="sub_table2">
                                                                                                    Class 3 :&nbsp;&nbsp;<br/>
                                                                                                    Number of students:<br/>
                                                                                                    <input type="text" name="no_stud_cert" id="class3" onkeypress="return numbersonly(event)">
                                                                                                        <input type="hidden" name="year_passing" id="year_passing"/>
                                                                                                </div><!-- sub_table2 -->
                                                                                                <div style="clear:both;"></div>                         

                                                                                                <div class="sub_table2">
                                                                                                    Class 5 :&nbsp;&nbsp;<br/>
                                                                                                    Number of students:<br/>
                                                                                                    <input type="text"  name="no_stud_cert1" id="class5"  onkeypress="return numbersonly(event)">
                                                                                                        <input type="hidden" name="year_passing1" id="year_passing1" />
                                                                                                </div><!-- sub_table2 -->
                                                                                                <div style="clear:both;"></div>

                                                                                                <div class="sub_table2">
                                                                                                    Class 8 :&nbsp;&nbsp;<br/>
                                                                                                    Number of students: <br/>
                                                                                                    <input type="text"  name="no_stud_cert2" id="class8"  onkeypress="return numbersonly(event)">
                                                                                                        <input type="hidden" name="year_passing2" id="year_passing2"/>
                                                                                                </div><!-- sub_table2 -->
                                                                                                <div style="clear:both;"></div>                        

                                                                                                <div class="sub_table2">
                                                                                                    Class 10 :<br/>
                                                                                                    Number of students:<br/>
                                                                                                    <input type="text"name="no_stud_cert3" id="class10" onkeypress="return numbersonly(event)">
                                                                                                        <input type="hidden" name="year_passing3" id="year_passing3" />
                                                                                                </div><!-- sub_table2 -->
                                                                                                <div style="clear:both;"></div>                        

                                                                                                <div class="sub_table2">
                                                                                                    Class 12 :<br/>
                                                                                                    Number of students: <br/>
                                                                                                    <input type="text" name="no_stud_cert4" id="class12"  onkeypress="return numbersonly(event)">
                                                                                                        <input type="hidden" name="year_passing4" id="year_passing4"/>
                                                                                                </div><!-- sub_table2 -->
                                                                                            </div><!-- main_table -->

                                                                                            <div style="clear:both;"></div>

                                                                                            <div class="main_table">
                                                                                                <div class="sub_table1">
                                                                                                    (b). If not yet accrediated, whether the Madrassa seeking financial assistance is interested in NIOS accrediation ? If so, whether applied to NIOS (refernce number of application be given) and by when NIOS accrediation is expected for (a) academic stream (class 3,5,8,10 & 12) and /or (b) vocational stream (Secondary & Sr. seconadry)) :

                                                                                                </div><!-- sub_table1 -->
                                                                                                <div class="sub_table2">
                                                                                                    <textarea name="financila_assisment" id="financila_assisment" style="width: 209px;"></textarea>
                                                                                                </div><!-- sub_table2 -->
                                                                                            </div><!-- main_table -->
                                                                                        </div>
                                  <?php }?>
                        
                    
                    <div style="clear:both;"></div>
                                    
                    <div class="main_table"> 
                        <div class="sub_table1">                       
                            8. <b>Details of proposal for financial assistance :</b>  
                        </div>
                         <div class="sub_table2">
                             <?php $proposal=$result[43];
                                     if($proposal==1){?>
                                        <select name="proposal" id="proposal">
                                     <option value="1" selected>Filled format I enclosed</option>
                                     <option value="2">Filled format I not enclosed</option>
                                </select> 
                                   <?php  }if($proposal==2){
                                       ?>
                              <select name="proposal" id="proposal">
                                  <option value="1">Filled format I enclosed</option>
                                     <option value="2" selected>Filled format I not enclosed</option>
                                </select> 
                             <?php
                                   }
?>
                               
                           
                        </div><!-- sub_table2 -->
                    </div><!-- main_table -->
                   
                    <div style="clear:both;"></div>
                   
                     <div class="main_table">
                        <div class="sub_table1">
                            (i). No of addtional teachers and amount required for teaching modern subjects as well as provision for their training :
                        </div><!-- sub_table1 -->
                        <div class="sub_table2">
                           <textarea name="additional_teacher" id="additional_teacher" onkeypress="return numbersonly(event)"><?=$result[44]?></textarea>
                        </div><!-- sub_table2 -->
                    </div><!-- main_table -->
                    
                     <div style="clear:both;"></div>
                     
                      <div class="main_table"> 
                        <div class="sub_table1">                       
                            <b>Requirements be given in Format I enclosed :</b>  
                        </div>
                         <div class="sub_table2">
                            <?php $req=$result[45];
                            if($req==1){?>
                               <select name="requirements" id="requirements">
                                     <option value="1" selected>Filled format I enclosed</option>
                                     <option value="2">Filled format I not enclosed</option>
                           </select> 
                          <?php  }if($req==2){?>
                               <select name="requirements" id="requirements">
                                     <option value="1">Filled format I enclosed</option>
                                     <option value="2" selected>Filled format I not enclosed</option>
                           </select> 
                          <?php  }
                            ?>
                            
                        </div><!-- sub_table2 -->
                    </div><!-- main_table -->
                    
                     <div style="clear:both;"></div>
                    
                     <div class="main_table"> 
                        <div class="sub_table1">                       
                            (These appointments may be on contract basis. This scheme doesnot provide for a cadre or regular appointment. These are purely on the short term basis.) :  
                        </div>
                         <div class="sub_table10">
                              <!--<input type="text" name="appointment" id="appointment" value="<?//=$result[46]?>">-->
                        </div><!-- sub_table2 -->
                    </div><!-- main_table -->
                    
                    <div style="clear:both;"></div>
                   
                         <div class="main_table" hidden>
                                                                                            <div class="sub_table1">
                                                                                                (ii). Number and amount required for Libraries/Book banks/Text books/Science labs/Computer labs/Science & Maths Kits tc. for teaching modern subjects :
                                                                                            </div><!-- sub_table1 -->
                                                                                            <div class="sub_table2">                          
                                                                                                Computer rate : <input onblur="findTotal1()" type="text" name="computer" id="qty3" />
                                                                                            </div>
                                                                                            <div style="padding-bottom: 25px;padding-top:20px"></div>
                                                                                            <div class="sub_table2">
                                                                                                For Training :&nbsp;&nbsp;&nbsp; <input type="text" name="training" id="qty3"/>                        
                                                                                            </div>
                                                                                            <div style="padding-bottom: 25px;padding-top:20px"></div>
                                                                                            <div class="sub_table2">
                                                                                                Annual Grand : <input  type="text" name="annual" id="qty3"  />
                                                                                            </div>
                                                                                            <div style="padding-bottom: 25px;padding-top:20px"></div>        
                                                                                            <div class="sub_table2">
                                                                                                Library :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input  type="text" name="library" id="qty3"/>                        
                                                                                            </div>                      
                                                                                            <div style="padding-bottom: 25px;padding-top:20px"></div>
                                                                                            <div class="sub_table2">
                                                                                                Total : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="total1" id="total1" readonly />
                                                                                            </div><!-- sub_table2 -->
                                                                                        </div><!-- main_table -->
                   
                     <div style="clear:both;"></div>
                   
                     
                     <div class="main_table"> 
                        <div class="sub_table1">                       
                            <b>Requirements be given in Format II enclosed :</b> <br/>
                            (Laboratories/Science labs/Computer labs are for secondary & senior secondary level only) 
                        </div>
                         <div class="sub_table2">
                           <?php $format=$result[51];
                           if($format==1){?>
                               <select name="format" id="format">
                                      <option value="1" selected>Filled format II enclosed</option>
                                       <option value="2">Filled format II not enclosed</option>
                                </select>
                                      
                             <?php
                           } if($format==2){?>
                               <select name="format" id="format">
                                      <option value="1">Filled format II enclosed</option>
                                       <option value="2" selected>Filled format II not enclosed</option>
                                </select>
                           <?php }
                           ?>                                                             
                        </div><!-- sub_table2 -->
                    </div><!-- main_table -->
                   
                   <div style="clear:both;"></div>
                   
                    <div class="main_table"> 
                        <div class="sub_table1">                       
                            (iii) For library books has a selection criteria been developed and a purchase commitee been set up by the madrassa ? :
                        </div>
                         <div class="sub_table2">
                              <?php
                             $purchase=$result[52];
                            if($seperate_room=='Yes'){?>
                            <input type="radio" name="radio7" onclick="showText5(0)" value="No" checked>Yes
                            <input type="radio" name="radio7" onclick="showText5(1)" value="Yes" >No<br/> 
                             <textarea id="purchase" name="purchase" style="resize:none;" style="width: 209px;"><?=$result[53];?></textarea>
                           <?php  }
                            if($seperate_room=='No'){?>
                                <input type="radio" name="radio5" onclick="showText5(0)" value="No">Yes
                                <input type="radio" name="radio5" onclick="showText5(1)" value="Yes" checked>No<br/>                                
                                <textarea id="purchase" name="purchase" hidden style="resize:none;" style="width: 209px;"></textarea>
                               <?php }
                           ?>   
                           
                        </div><!-- sub_table2 -->
                    </div><!-- main_table -->
                   
                   <div style="clear:both;"></div>
                   
                    <div class="main_table"> 
                        <div class="sub_table1">                       
                           (iv) Amount required by Madrassas opting for NIOS accrediation for academic stream in modern subjects :
                        </div>
                         <div class="sub_table2">
                           <input type="text" name="amount_required" id="amount_required" value="<?=$result[54]?>" onkeypress="return numbersonly(event)"/>
                        </div><!-- sub_table2 -->
                    </div><!-- main_table -->
                   
                   <div style="clear:both;"></div>
                   
                   <div class="main_table">                                              
                            <b>Requirements be given in Format III enclosed :</b> 
                  </div><!-- main_table -->
                   
                   <div class="main_table"> 
                        <div class="sub_table1">                       
                           (v) Amount required by Madrassas opting for NIOS accrediation for vocational stream :
                        </div>
                         <div class="sub_table2">
                           <input type="text" name="vocational_stream" id="vocational_stream" value="<?=$result[55]?>" onkeypress="return numbersonly(event)"/>
                        </div><!-- sub_table2 -->
                    </div><!-- main_table -->
                   
                    <div style="clear:both;"></div>
                    
                    <div class="main_table" hidden> 
                        <div class="sub_table1">                       
                           10. Total Amount required :
                        </div><br/><br/>
                         <div class="sub_table2">
                           <div class="sub_table2">
                               For salary : &nbsp;<input  type="text" name="salary" id="salary"/> 
                                <div style="padding-top:10px"></div>
                                   Others : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input  type="text" name="others" id="others" /> 
                               <div style="padding-top:10px"></div>   

                                 <div style="clear:both;"></div>             
                         Total : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="total2" id="total2" />
                                     </div><!-- sub_table2 -->
                        </div><!-- sub_table2 -->
                    </div><!-- main_table -->
                   
                   <div style="clear:both;"></div>
                   
                    <div class="main_table"> 
                        <div class="sub_table1">                       
                            (11) Whether the Madrasa is getting any financial assistance for teaching of modern subjects from an y other source. If so, the amount and the purpose for which it is intended, be mentioned. (No duplication should be done) :
                        </div>
                         <div class="sub_table2">
                           <textarea name="modern_subject" id="modern_subject"><?=$result[59];?></textarea>
                        </div><!-- sub_table2 -->
                    </div><!-- main_table -->
                    
                     <div style="clear:both;"></div>
                   
                    <div class="main_table" hidden> 
                        <div class="sub_table1">                       
                            (12) Net amount requested from Government (10-11) :
                        </div>
                         <div class="sub_table2">
                           Net Amount : &nbsp; 
                            <input type="text" size="18" name="inum" onblur="test.rnum.value = toWords(test.inum.value);"  onkeypress="return numbersonly(event)">
                            <br/>
                           In Words : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<textarea name="rnum" id="rnum"  style="width: 193px;"></textarea>
                        </div><!-- sub_table2 -->
                    </div><!-- main_table -->
                   
                   <div style="clear:both;"></div>
                   <div style="margin-top: 40px;">
                    <div style="float: left;">
                        <input type="submit" name="submit_edit" value="Save" class="save_button"/>
                    </div>
                         <div>
                        <input type="submit" name="submit_cancel" value="Cancel" class="cancel_button1"/>
                     </div> 
                        <div style="float:right;margin-top: -20px;">
                            <a href="edit-format1.php?id=<?php echo $result[1]; ?>&fileno=<?php echo $result[2]; ?>&value=<?php echo $radio6;?>">Edit Next Form</a>
                        </div>

                   </div>
                   </div>
                    <?php  
                      }
                      else{
						  if ($serach!= $file_no){
						  echo "<div style='color:red;padding-top:10px;text-align:center;'>Invalid Application Id / No !</div>";
						  }
					  }
                      }
                
                ?>
                <?php 
                 if(isset($_POST['submit_edit'])){ 
                       $slot= $_POST['application_no'];
                       $alocate = $_POST['id_no'];             
                        //$namehere= $_POST['namehere'];
                        $name = $_POST['society_address1'];
                        $name_address = $_POST['name_address'];
                        $country = $_POST['country'];
                       // $city = $_POST['city'];
                        $email = $_POST['email'];
                       // $name_madara=$_POST['name_madara'];
                        $obj_actv = $_POST['obj_actv'];;
                        $radio1 = $_POST['radio1'];
                        $madrasa = $_POST['madrasa'];
                        $reg_no = $_POST['reg_no'];
                        $reg_date = $_POST['reg_date'];
                        $educational=$_POST['educational'];
                        $radio2 = $_POST['radio2'];
                        $financial = $_POST['financial'];
                        $nios = $_POST['nios'];
                        $curriculum=$_POST['curriculum'];
                        $vals='';
                        foreach($_POST['qty'] as &$value) {
                              $val= $value.',';
                                $vals.=$val; 
                            }
                        $datos_uno=$vals; 
                        $total=$_POST['total'];
                        $drop1 = $_POST['drop1'];
                      // echo "<br/>";
                         $valus='';
                        $salary="";
                        foreach($_POST['qualification_teacher'] as &$values) {
                          //  echo "qualification_teacher".$values;
                          //  echo "<br/>";
                           $valu= $values.',';
                               $valus.=$valu;
                             if($values==1)
                                 $salary=$salary+(12000*12);
                             if($values==2)
                                 $salary=$salary+(6000*12);
                            }
                          //  echo "salary".$salary;
                          //  echo "<br/>";
                        $datos_unos=$valus;
                        $school_level=$_POST['school_level'];
                    //echo "<br/>";
                        $radio4 = $_POST['radio4'];
                        $located = $_POST['located'];
                        $room_avaliable=$_POST['room_avaliable']; ;
                        $radio3 = $_POST['radio3'];                    
                        $accommodation=$_POST['accommodation'];
                        $radio5=$_POST['radio5'];
                        $seperate_room=$_POST['seperate_room'];
                        $radio6=$_POST['radio6'];
                        $resgistered=$_POST['resgistered'];
                        $no_stud_cert=$_POST['no_stud_cert'];
                        $year_passing='2014-2015';
                        $no_stud_cert1=$_POST['no_stud_cert1'];
                        $year_passing1='2014-2015';
                        $no_stud_cert2=$_POST['no_stud_cert2'];
                        $year_passing2='2014-2015';
                        $no_stud_cert3=$_POST['no_stud_cert3'];
                        $year_passing3='2014-2015';
                        $no_stud_cert4=$_POST['no_stud_cert4'];
                        $year_passing4='2014-2015';
                        $financila_assisment=$_POST['financila_assisment'];
                        $proposal=$_POST['proposal'];
                        $additional_teacher=$_POST['$additional_teacher'];
                        $requirements=$_POST['requirements'];
                        if($school_level==1){
                           // echo "hai1";
                        $computer=0;
                       $training=$drop1*1500;
                       //echo "<br/>";
                       $annual=15000;
                       // echo "<br/>";
                       $library=50000; 
                        //echo "<br/>";
                        $total1=$training+$annual+$library;
                        }
                        elseif($school_level==2){
                          //  echo "hai2";
                        $computer=100000;
                        $training=$drop1*1500;
                        $annual=15000;
                        $library=50000;                        
                        $total1=$computer+$training+$annual+$library;
                        }
                        elseif ($school_level==3) 
                        {
                            //echo "hai3";
                        $computer=100000;
                        $training=$drop1*1500;
                        $annual=15000;
                        $library=50000;                        
                        $total1=$computer+$training+$annual+$library;
                         }
                        elseif ($school_level==4) 
                        {
                           // echo "hai4";
                        $computer=100000;
                        $training=$drop1*1500;
                        $annual=15000;
                        $library=50000;                        
                        $total1=$computer+$training+$annual+$library;
                         }
                        $format=$_POST['format'];
                        $radio7 = $_POST['radio7'];
                        $purchase=$_POST['purchase'];
                        $amount_required=$_POST['amount_required'];
                        $vocational_stream=$_POST['vocational_stream'];
                      // $total1;
                       //echo "<br/>";
                        $others=$total1;
                       $total2=$others+$salary;
                        $modern_subject=$_POST['modern_subject'];
                        $net_amount=$total2-$modern_subject; 
                        $in_words=$_POST['rnum']; 
                        
                     $sql=mysql_query("update wp_master set namehere='$country',society_address='$name',name_address='$name_address',country='$country',email='$email',obj_actv='$obj_actv',radio1='$radio1',madrasa='$madrasa',reg_no='$reg_no',reg_date='$reg_date',educational='$educational',radio2='$radio2',financial='$financial',nios='$nios',curriculum='$curriculum',no_boys='$datos_uno',total='$total',drop1='$drop1',qualification_teacher='$datos_unos',school_level='$school_level',radio4='$radio4',located='$located',room_avaliable='$room_avaliable',radio3='$radio3',accommodation='$accommodation',radio5='$radio5',seperate_room='$seperate_room',resgistered='$resgistered',no_stud_cert='$no_stud_cert',year_passing='$year_passing',no_stud_cert1='$no_stud_cert1',year_passing1='$year_passing1',no_stud_cert2='$no_stud_cert2',year_passing2='$year_passing2',no_stud_cert3='$no_stud_cert3',year_passing3='$year_passing3',no_stud_cert4='$no_stud_cert4',year_passing4='$year_passing4',financila_assisment='$financila_assisment',proposal='$proposal',additional_teacher='$additional_teacher',requirements='$requirements',computer='$computer',training='$training',total1='$total1',format='$format',radio7='$radio7',purchase='$purchase',amount_required='$amount_required',vocational_stream='$vocational_stream',salary='$salary',others='$others',total2='$total2',modern_subject='$modern_subject',net_amount='$net_amount',in_words='$in_words' where application_id=$slot and id_no=$alocate");
                     $sql1=mysql_query("update wp_format5 set tehsil='$country',name_address='$name_address',date='$reg_date',no='$reg_no' where mater_application_id=$slot and master_id_no=$alocate");                
//echo "update wp_master set society_address='$name',name_address='$name_address',country='$country',email='$email',obj_actv='$obj_actv',radio1='$radio1',madrasa='$madrasa',reg_no='$reg_no',reg_date='$reg_date',educational='$educational',radio2='$radio2',financial='$financial',nios='$nios',curriculum='$curriculum',no_boys='$datos_uno',total='$total',drop1='$drop1',qualification_teacher='$datos_unos',school_level='$school_level',radio4='$radio4',located='$located',room_avaliable='$room_avaliable',radio3='$radio3',accommodation='$accommodation',radio5='$radio5',seperate_room='$seperate_room',resgistered='$resgistered',no_stud_cert='$no_stud_cert',year_passing='$year_passing',no_stud_cert1='$no_stud_cert1',year_passing1='$year_passing1',no_stud_cert2='$no_stud_cert2',year_passing2='$year_passing2',no_stud_cert3='$no_stud_cert3',year_passing3='$year_passing3',no_stud_cert4='$no_stud_cert4',year_passing4='$year_passing4',financila_assisment='$financila_assisment',proposal='$proposal',requirements='$requirements',computer='$computer',training='$training',total1='$total1',format='$format',radio7='$radio7',purchase='$purchase',amount_required='$amount_required',vocational_stream='$vocational_stream',salary='$salary',others='$others',total2='$total2',modern_subject='$modern_subject',net_amount='$net_amount',in_words='$in_words' where application_id=$slot and id_no=$alocate";
                 //echo $sql;
                   
                     echo "<script>window.location='edit-format1.php?id=$slot&fileno=$alocate&value=$radio6'</script>";
                 }
                 
                ?>

 <?php
        if(isset($_POST['submit_cancel'])){ 
     
     echo"<script>window.location='edit.php'</script>";
 }

?>
          
 </form>       

</div>
</div>
</div>
<div id="footerouter">
<div id="footer">© All Rights Reserved. IT@School, General Education Department, Govt of Kerala. </div>
</div>
</div>
</body>
</html>
<script> 
function getSelectedOptions(oList)
{
   var sdValues = [];
   for(var i = 1; i < oList.options.length; i++)
   {
      if(oList.options[i].selected == true)
      {
      sdValues.push(oList.options[i].value);
      }
   }
   return sdValues;
}
 function showText(num)
            {
                if (num == 0) {
                    document.getElementById('mytext').style.display = 'block';
                    document.getElementById('madrasa').style.display = 'block';
                    document.getElementById('dob').style.display = 'block';
                    document.getElementById('reg_no').style.display = 'block';
                    document.getElementById('reg_date').style.display = 'block';
                }
                else {
                    document.getElementById('mytext').style.display = 'none';
                    document.getElementById('madrasa').style.display = 'none';
                    document.getElementById('dob').style.display = 'none';
                    document.getElementById('reg_no').style.display = 'none';
                    document.getElementById('reg_date').style.display = 'none';
                    return;
                }
            }
            function showText1(num)
            {
                if (num == 0)
                    document.getElementById('mytext1').style.display = 'block';

                else
                    document.getElementById('mytext1').style.display = 'none';

                return;
            }

            function showText2(num)
            {
                if (num == 0)
                    document.getElementById('mytext2').style.display = 'block';

                else
                    document.getElementById('mytext2').style.display = 'none';

                return;
            }

            function showText3(num)
            {
                if (num == 0)
                    document.getElementById('mytext3').style.display = 'block';

                else
                    document.getElementById('mytext3').style.display = 'none';

                return;
            }

            function showText5(num)
            {
                if (num == 0)
                    document.getElementById('purchase').style.display = 'block';

                else
                    document.getElementById('purchase').style.display = 'none';

                return;
            }

            function findTotal() {
                var arr = document.getElementsByName('qty[]');
                var tot = 0;
                for (var i = 0; i < arr.length; i++) {
                    if (parseInt(arr[i].value))
                        tot += parseInt(arr[i].value);
                }
                document.getElementById('total').value = tot;
            }

            function findTotal1() {
                var arr = document.getElementsByName('qty1[]');
                var tot = 0;
                for (var i = 0; i < arr.length; i++) {
                    if (parseInt(arr[i].value))
                        tot += parseInt(arr[i].value);
                }
                document.getElementById('total1').value = tot;
            }

            function findTotal2() {
                var arr = document.getElementsByName('qty2[]');
                var tot = 0;
                for (var i = 0; i < arr.length; i++) {
                    if (parseInt(arr[i].value))
                        tot += parseInt(arr[i].value);
                }
                document.getElementById('total2').value = tot;
            }

</script>

<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script type="text/javascript">

    $(function() {
                /*----------------- Dropdown1 functions -------------------------*/
                $('#drop1').on('change', function() {
                    var current_val = $(this).val();
                    if (current_val == 1) {
                        $('#elm2').remove();
                        $('#elm3').remove();
                        if ($('#elm1').length == 0) {
                            $('#elmt_cont').append('<div id="elm1"><select name="qualification_teacher[]" id="qualification_teacher" class="qualification_teacher"><option value="">SelectOne</option><option value="1">Post Graduation</option><option value="2">Graduation</option><option value="3">Plus Two</option><option value="4">SSLC</option> <option value="5">Below SSLC</option></select></div>');
                        }
                    }
                    if (current_val == 2) {
                        $('#elm3').remove();
                        if ($('#elm2').length == 0) {
                            $('#elmt_cont').append('<div id="elm2"><select name="qualification_teacher[]" id="qualification_teacher" class="qualification_teacher"><option value="">SelectOne</option><option value="1">Post Graduation</option><option value="2">Graduation</option><option value="3">Plus Two</option><option value="4">SSLC</option> <option value="5">Below SSLC</option></select></div>');
                        }
                    }
                    if (current_val == 3) {
                        $('#elm3').remove();
                        if ($('#elm2').length == 0) {
                            $('#elmt_cont').append('<div id="elm2"><select name="qualification_teacher[]" id="qualification_teacher" class="qualification_teacher"><option value="">SelectOne</option><option value="1">Post Graduation</option><option value="2">Graduation</option><option value="3">Plus Two</option> <option value="4">SSLC</option> <option value="5">Below SSLC</option></select></div>');
                        }
                        if ($('#elm3').length == 0) {
                            $('#elmt_cont').append('<div id="elm3"><select name="qualification_teacher[]" id="qualification_teacher" class="qualification_teacher"><option value="">SelectOne</option><option value="1">Post Graduation</option><option value="2">Graduation</option><option value="3">Plus Two</option> <option value="4">SSLC</option> <option value="5">Below SSLC</option></select></div>');
                        }
                    }
                });

            });
</script>
<script type="text/javascript">
// American Numbering System
var th = ['', 'thousand', 'million', 'billion', 'trillion'];

var dg = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];

var tn = ['ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'];

var tw = ['twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];

function toWords(s) {
    s = s.toString();
    s = s.replace(/[\, ]/g, '');
    if (s != parseFloat(s)) return 'not a number';
    var x = s.indexOf('.');
    if (x == -1) x = s.length;
    if (x > 15) return 'too big';
    var n = s.split('');
    var str = '';
    var sk = 0;
    for (var i = 0; i < x; i++) {
        if ((x - i) % 3 == 2) {
            if (n[i] == '1') {
                str += tn[Number(n[i + 1])] + ' ';
                i++;
                sk = 1;
            } else if (n[i] != 0) {
                str += tw[n[i] - 2] + ' ';
                sk = 1;
            }
        } else if (n[i] != 0) {
            str += dg[n[i]] + ' ';
            if ((x - i) % 3 == 0) str += 'hundred ';
            sk = 1;
        }
        if ((x - i) % 3 == 1) {
            if (sk) str += th[(x - i - 1) / 3] + ' ';
            sk = 0;
        }
    }
    if (x != s.length) {
        var y = s.length;
        str += 'point ';
        for (var i = x + 1; i < y; i++) str += dg[n[i]] + ' ';
    }
    return str.replace(/\s+/g, ' ');

}
    </script>
    
     <script type="text/javascript">
		function numbersonly(e){
        var keynum;
        var keychar;
        var numcheck;
        if(window.event) // IE
        {
            keynum = e.keyCode;
        }
        else if(e.which) // netscape/Firefox/opera
        {
            keynum = e.which;
        }

         var tabkey = e.keyCode;

         if( keynum == 8 || tabkey == 9 || tabkey == 37 || tabkey == 39)
        {
                  return true;
        }        

        else
        {
             keychar = String.fromCharCode(keynum);
            //numcheck = /\d/;
            numcheck = /[.0-9]/;
            return numcheck.test(keychar);
        }
    }
    
      $(".nios").change(function() {
                //alert("hai");
                var val = $(this).val();
                //alert(val);
                if (val == 3) {
                    $("#curriculum").show();
                } else {
                    $("#curriculum").hide();
                }
            });

            function yesnoCheck() {
                if (document.getElementById('yesCheck').checked) {
                    document.getElementById('ifYes').style.display = 'block';
                }
                else
                    document.getElementById('ifYes').style.display = 'none';

            }
		</script>

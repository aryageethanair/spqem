<?php
include './library/dbconnect.php';
include_once("./submit.inc.php");
	$value=$_REQUEST['value'];
	$_SESSION['value']=$value;
echo $query1="SELECT * FROM `input_details` WHERE `Fileno`='$value'";
$result=mysql_query($query1) or die("".mysql_error());
$num=mysql_num_rows($result);
$row=mysql_fetch_array($result);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="mystyle.css">
<title>INFRASTRUCTURE DEVELOPMENT</title>
</head>

<body>
<div>
<div id="header">
SCHEME FOR INFRASTRUCTURE DEVELOPMENT PRIVATE AIDED/UNAIDED<br />MINORITY INSTITUTES(IDMI)-(ELEMENTARY SECONDARY/SENIOR<br />SECONDARY SCHOOLS)
<div style="margin-top:30px;">
<span style="font: bold">Application Form</span> 
</div>
</div>
<div id="content">
<form name="fors1" id="fors1" method="post" action="">
<table align="center">
    <tr valign="top">
    	<td>1.</td>
        <td>Name of Voluntary Organisation/Society managing <br /> the school(With complete address):</td>
        <td>:</td>
        <td><textarea name="orgname" id="orgname" readonly="readonly"> <?php echo  $row['orgname'];?></textarea></td>
    </tr>
	<tr>
		<td>2.</td>
        <td>Name with address of the school/institution for <br /> financial assistance required</td>	
        <td>:</td>
        <td><textarea name="schoolname" id="schoolname" readonly="readonly"><?php echo  $row['schoolname'];?></textarea></td>
    </tr>
    <tr>
    	<td>3.</td>
    	<td>Objects and activities(give brief history of the <br />/society managing the school:)</td>
        <td>:</td>
        <td><textarea name="org_history" id="org_history" readonly="readonly"><?php echo  $row['schoolname'];?></textarea></td>
    </tr>
    <tr>
    	<td>4.</td>
    	<td>Specific activities of the school for which financial <br /> assistance is sought under the scheme</td>
        <td>:</td>
        <td><textarea name="school_activities" id="school_activities" readonly="readonly"><?php echo  $row['school_activities'];?></textarea></td>
    </tr>
    <tr>
    	<td>5.</td>
    	<td>Whether registered under the Centra/State Board?<br />If Yes,Registration No.(A copy of	the registration certificate to be enclosed): </td>
        <td>:</td>
        <td><input type="radio" name="registration" id="registration" readonly="readonly"/>Yes
        	<input type="radio" name="registration" id="registration" readonly="readonly" />No
        </td>
    </tr>
    <tr>
    	<td>6.</td>
    	<td>Organisational structure,total staff.their roles and responsibilities,<br />staff turnover of educational <br />institute/school for which assistance is being<br />             sought and the voluntary organisation/society</td>
        <td>:</td>
        <td><textarea name="org_details" id="org_details" readonly="readonly"><?php echo  $row['org_details'];?></textarea></td>
    </tr>
    <tr  valign="top">
    	<td>7.</td>
    	<td>Governing Board/Managing Committe-number of <br />members,their role,meeting held and attendance,<br />their involvement in decision making of<br /> educational             institution/school and the voluntary<br /> organisation/society concerned(List of             members <br />may be enclosed)</td>
        <td>:</td>
        <td><textarea name="Govrning_board" id="Govrning_board" readonly="readonly"><?php echo  $row['Govrning_board'];?></textarea></td>
    </tr>
    <tr>
    	<td>8.</td>
    	<td>Name and address of bankers,auditors,legal<br /> advisors(including details of accounts)of voluntary organisation/society</td>
        <td>:</td>
        <td><textarea name="bankers" id="bankers" readonly="readonly"><?php echo  $row['bankers'];?></textarea></td>
    </tr>
    <tr>
    	<td>9.</td>
    	<td>Details of infra-structural facilities availible with<br /> educational institution/school for whom,<br /> assistance is being sought</td>
    	<td>:</td>
    	<td><textarea name="infra_details" id="infra_details" readonly="readonly"><?php echo  $row['infra_details'];?></textarea></td>	
    </tr>		
    		<tr>
    		<td></td>
    		<td>
    		<ol>
    			<li>Whether the building is rented or own?:</li>
    			<li>No. of rooms availible for classes and<br /> Administrative purposes </li>
    			<li>Sufficiency of accomodation for reading<br /></li>
    			<li>Whether seperate rooms for science<br /> laboratory,library etc,are available. </li>
 				<li>No.of teachers subject-wise already<br />working with their name,qualification etc.<br>(if needed attach sheets) </li>
 				<li>Number of children enrolled in respective<br />classes relevent to purpose for which<br />assistance is being sought.(at least 3 years<br />data be given) </li>
 			</ol>
 			</td>
    		<td></td>
    
        <td>
        	<ol>
        	<textarea name="rented" id="rented" readonly="readonly"><?php echo  $row['rented'];?></textarea><br />
        	<textarea name="classroom" id="classroom" readonly="readonly"><?php echo  $row['classroom'];?></textarea><br />
        	<textarea name="accomodation" id="accomodation" readonly="readonly"><?php echo  $row['accomodation'];?></textarea><br />
        	<textarea name="lab" id="lab" readonly="readonly"><?php echo  $row['lab'];?></textarea><br />
        	<textarea name="teachers" id="teachers" readonly="readonly"><?php echo  $row['teachers'];?></textarea><br />
        	<textarea name="enrolled_child" id="enrolled_child" readonly="readonly"><?php echo  $row['enrolled_child'];?></textarea><br />	
            </ol>
       </td>
    </tr>
    <tr>
    	<td>10.</td>
    	<td>Action Photographs about the school</td>
        <td>:</td>
        <td><input type="text" id="photo" name="photo" readonly="readonly" value="<?php  echo $row['photo']; ?>"/></td>
    </tr>
    <tr>
    	<td>11.</td>
    	<td>Sucess stories/testimonials/award/recognitions<br /> and how the work of school has made difference to the individual,<br />family and for the community</td>
        <td>:</td>
        <td><textarea name="success_stories" id="success_stories" readonly="readonly"><?php echo  $row['success_stories'];?></textarea></td>
        
    </tr>
    <tr>
    	<td>12.</td>
    	<td>Community/client involement in the academic<br /> affairs pof school/educational institution</td>
        <td>:</td>
        <td><textarea name="client_involve" id="client_involve" readonly="readonly"><?php echo  $row['client_involve'];?></textarea></td>
    </tr>
    <tr>
    	<td>13.</td>
    	<td>future plans and sustainability</td>
        <td>:</td>
        <td><textarea name="future_plans" id="future_plans" readonly="readonly"><?php echo  $row['future_plans'];?></textarea></td>
    </tr>
    <tr>
    	<td>14.</td>
    	<td>Audited balance sheet,income and expenditure<br /> statement for the last three years of voluntary <br />agency and educational institution and school for<br /> which assistence is sought</td>
        <td>:</td>
        <td><textarea name="photo1" id="photo1" readonly="readonly"><?php echo  $row['photo1'];?></textarea></td>
    </tr>
    <tr>
    	<td>15.</td>
    	<td>Annual repeot if any of VA/educational institution or school</td>
        <td>:</td>
        <td><textarea name="photo2" id="photo2" readonly="readonly"><?php echo  $row['photo2'];?></textarea></td>
    </tr>
    <tr>
    	<td>16.</td>
    	<td>Inforamtion on existing funding sources with break <br />up grants,loans,and corpus received by VA and <br />educational institution/school for which assistance is being sought</td>
        <td>:</td>
        <td><textarea name="fund_info" id="fund_info" readonly="readonly"><?php echo  $row['fund_info'];?></textarea></td>
    </tr>
    <tr>
    	<td>17.</td>
    	<td>Whether the educational institution/school is<br /> receiving financial assistance for infrastructure<br /> development of any kinds from any other source;if<br /> so details thereof:</td>
        <td>:</td>
        <td><textarea name="fin_assistance" id="fin_assistance" readonly="readonly"><?php echo  $row['fin_assistance'];?></textarea></td>
    </tr>
    <tr>
    	<td>18.</td>
    	<td>Amount required for additional</td>
        <td>:</td>
        <td></td>
    </tr>
    <tr><td colspan="4">
        	<table border="1" align="center"   >
        		<tr>
        			<td>Item</td>
        			<td>Number</td>
        			<td>Amount required*</td>
        			<td>No. of children to be benefited</td>
        		
        		</tr>
        	<tr>
        		<td>classrooms</td>
        		<td><input type="text" name="num1" id="num1" size="15" readonly="readonly" value="<?php echo  $row['num1'];?>" /></td>
        		<td><input type="text" name="am1" id="am1" size="15" readonly="readonly" value="<?php echo  $row['am1'];?>"/></td>
        		<td><input type="text" name="bene1" id="bene1" size="20" readonly="readonly" value="<?php echo  $row['bene1'];?>"/></td>
        	</tr>
        	<tr>
        		<td>Science rooms</td>
        		<td><input type="text" name="num2" id="num2" size="15" readonly="readonly" value="<?php echo  $row['num2'];?>"/></td>
        		<td><input type="text" name="am2" id="am2" size="15" readonly="readonly" value="<?php echo  $row['am2'];?>"/></td>
        		<td><input type="text" name="bene2" id="bene2" size="20" readonly="readonly" value="<?php echo  $row['bene2'];?>"/></td>
        	</tr>
        	<tr>
        		<td>Computer lab rooms</td>
        		<td><input type="text" name="num3" id="num3" size="15" readonly="readonly" value="<?php echo  $row['num3'];?>"/></td>
        		<td><input type="text" name="am3" id="am3" size="15" readonly="readonly" value="<?php echo  $row['am3'];?>"/></td>
        		<td><input type="text" name="bene3" id="bene3" size="20" readonly="readonly" value="<?php echo  $row['bene3'];?>"/></td>
        	</tr>
        	<tr>
        		<td>Library rooms</td>
        		<td><input type="text" name="num4" id="num4" size="15" readonly="readonly" value="<?php echo  $row['num4'];?>"/></td>
        		<td><input type="text" name="am4" id="am4" size="15" readonly="readonly" value="<?php echo  $row['am4'];?>"/></td>
        		<td><input type="text" name="bene4" id="bene4" size="20" readonly="readonly" value="<?php echo  $row['bene4'];?>"/></td>
        	</tr>
        	<tr>
        		<td>Toilets(Girls)</td>
        		<td><input type="text" name="num5" id="num5" size="15" readonly="readonly" value="<?php echo  $row['num5'];?>"/></td>
        		<td><input type="text" name="am5" id="am5" size="15" readonly="readonly" value="<?php echo  $row['am5'];?>"/></td>
        		<td><input type="text" name="bene5" id="bene5" size="20" readonly="readonly" value="<?php echo  $row['bene5'];?>"/></td>
        	</tr>
        	<tr>
        		<td>Toilets(Boys)</td>
        	<td><input type="text" name="num6" id="num6" size="15" readonly="readonly" value="<?php echo  $row['num6'];?>"/></td>
        		<td><input type="text" name="am6" id="am6" size="15" readonly="readonly" value="<?php echo  $row['am6'];?>"/></td>
        		<td><input type="text" name="bene6" id="bene6" size="20" readonly="readonly" value="<?php echo  $row['bene6'];?>"/></td>
        	</tr>
        	<tr>
        		<td>Drinking water facilities</td>
        	<td><input type="text" name="num7" id="num7" size="15" readonly="readonly" value="<?php echo  $row['num7'];?>"/></td>
        		<td><input type="text" name="am7" id="am7" size="15" readonly="readonly" value="<?php echo  $row['am7'];?>"/></td>
        		<td><input type="text" name="bene7" id="bene7" size="20" readonly="readonly" value="<?php echo  $row['bene7'];?>"/></td>
        	</tr>
        	<tr>
        		<td>Hostels for girls</td>
        		<td><input type="text" name="num8" id="num8" size="15" readonly="readonly" value="<?php echo  $row['num8'];?>"/></td>
        		<td><input type="text" name="am8" id="am8" size="15" readonly="readonly" value="<?php echo  $row['am8'];?>"/></td>
        		<td><input type="text" name="bene8" id="bene8" size="20" readonly="readonly" value="<?php echo  $row['bene8'];?>"/></td>
        	</tr>
        	<tr>
        		<td>Hostels for boys</td>
        		<td><input type="text" name="num9" id="num9" size="15" readonly="readonly" value="<?php echo  $row['num9'];?>"/></td>
        		<td><input type="text" name="am9" id="am9" size="15" readonly="readonly" value="<?php echo  $row['am9'];?>"/></td>
        		<td><input type="text" name="bene9" id="bene9" size="20" readonly="readonly" value="<?php echo  $row['bene9'];?>"/></td>
        	</tr>
        	<tr>
        		<td>Educational facilities like ramps/labs for children with special needs</td>
        		<td><input type="text" name="num10" id="num10" size="15" readonly="readonly" value="<?php echo  $row['num10'];?>"/></td>
        		<td><input type="text" name="am10" id="am10" size="15" readonly="readonly" value="<?php echo  $row['am10'];?>"/></td>
        		<td><input type="text" name="bene10" id="bene10" size="20" readonly="readonly" value="<?php echo  $row['bene10'];?>"/></td>
        	</tr>
        	<tr>
        		<td>any other educational infrastucture requirement</td>
        		<td><input type="text" name="num11" id="num11" size="15" readonly="readonly" value="<?php echo  $row['num11'];?>"/></td>
        		<td><input type="text" name="am11" id="am11" size="15" readonly="readonly" value="<?php echo  $row['am11'];?>"/></td>
        		<td><input type="text" name="bene11" id="bene11" size="20" readonly="readonly" value="<?php echo  $row['bene11'];?>"/></td>
        	</tr>
        	<tr>
        		<td>Total</td>
        	<td><input type="text" name="num_tot" id="num_tot" size="15" readonly="readonly" value="<?php echo  $row['num_tot'];?>"/></td>
        		<td><input type="text" name="am_tot" id="am_tot" size="15" readonly="readonly" value="<?php echo  $row['am_tot'];?>"/></td>
        		<td><input type="text" name="bene_tot" id="bene_tot" size="20" readonly="readonly" value="<?php echo  $row['bene_tot'];?>"/></td>
        	</tr>
        	
        	
        	</table></td>
        </tr>
        
    
    <tr>
    	<td>19.</td>
    	<td>Voluntary Organisation/Society's Share equal to<br /> 25% towards item 18 and commitment of<br /> voluntary Organisation/society to provide the same:</td>
        <td>:</td>
        <td><textarea name="org_share" id="org_share" readonly="readonly"><?php echo  $row['org_share'];?></textarea></td>
    </tr>
    <tr>
    	<td>20.</td>
    	<td>Source of investment of voluntary organisations/<br />society's share given against item 18</td>
        <td>:</td>
        <td><textarea name="source_investment" id="" readonly="readonly"><?php echo  $row['source_investment'];?></textarea></td>
    </tr>
    <tr>
    	<td>21.</td>
    	<td>Central Govt share equal to 75% reqiured against item 18</td>
        <td>:</td>
        <td><textarea name="govt_share" id="govt_share" readonly="readonly"><?php echo  $row['govt_share'];?></textarea></td>
    </tr>
   
   	<tr>
   		<td></td>
   		<td></td>
   		<td><input type="submit" name="Approve" id="Approve" value="Approve" readonly="readonly"/>
   		<input type="submit" name="Decline" id="Decline" value="Decline" readonly="readonly" /></td>
   		
   		<td></td>
   	</tr>
</table>
</form>		
</div>
</div>
</body>
</html>
<?php
include 'library/dbconnect.php';
//include_once("submit.inc.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link rel="stylesheet" type="text/css" href="style.css" />
        <script src="JSCal2/js/jscal2.js"></script>
        <script src="JSCal2/js/lang/en.js"></script>
        <link rel="stylesheet" type="text/css" href="JSCal2/css/jscal2.css" />
        <link rel="stylesheet" type="text/css" href="JSCal2/css/border-radius.css" />
        <link rel="stylesheet" type="text/css" href="JSCal2/css/steel/steel.css" />

        <script src="JSCal2/js/gen_validatorv31.js" type="text/javascript"></script>
         <style>
    table{border:1px solid;
    border-collapse: collapse;width: 96%;
margin-top: 12px;}
   
    tr,td{
        border:1px solid;
        padding:3px;
    }
     td table{
        border:none;
    }
</style>
    </head>
    <body>
        <div id="container">
            <div id="header">
                <div id="header_title">CENTRAL SPONSORED SCHEME FOR PROVIDING QUALITY EDUCATION IN MADRASA (SPQEM)</div>


            </div>
            <div id="menubar"><ul>
                    <li><a href="DD.php">Home</a></li>
                     <li><a href="logout.php">Logout</a></li>

                </ul></div>
            <div id="body">

                <div id="inner" id="inner">
                    <div id="middle1" style="padding:20px;">

                        <div><font color="RED"><?php //echo $msg;  ?></font></div>

                        <form action="" method="post" name="test" style="border: 1px solid black;padding-left: 10px;padding-right: 10px;width: 935px;">    
                          
                            <?php
                            if(isset($_GET['id'])){	
                               $id = $_GET['id'];
                              // $district=mysql_query("select master_application_id from wp_format1 where master_application_id='$id'");
                              // $application_id=mysql_fetch_array($district);
                              //echo $applicationid=$application_id[0];
                           //  echo "select * from wp_format1 where master_application_id='$id' and status=1";
                                 $contact_exist=mysql_query("select * from wp_format3 where mater_application_id='$id' and status=3");
                  // echo "select * from wp_master where application_id='$serach'";
                   if(mysql_num_rows($contact_exist)!=0){
                            $result=mysql_fetch_row($contact_exist);
                             $seracch= $result[1];
?>
                   
                   <input type="hidden" name="application_no" id="name" value="<?=$result[1] ?>" />
                   <input type="hidden" name="id_no" id="name" value="<?=$result[2] ?>" />
                     <div class="main_heading">
                        <div class="application">
                            <?php                     
                                echo "<b>FORMAT-III - NA</b>";
                            ?>
                        </div><!--application-->
                        <div class="part">
                            <?php                     
                                echo "Madrasas opting for NIOS <br/> Accrediation (Academic) <br/> (To be submitted under joint signature of Madrasa & NIOS)";
                            ?>
                        </div><!--part--> 
                    </div><!--main_heading-->   

                 <div style="clear:both;"></div>
               <div id="print_style">
 <div class="main_table">
                      <div class="sub_table1">
                            1. Name of Madrasa with full address/tehsil/distt State :
                        </div><!-- sub_table1 -->
                      <div class="sub_table2">
                           <?=$result[3] ?>
                        </div><!-- sub_table2 -->
                    </div><!-- main_table -->    
                    
                    <div style="clear:both;"></div>  
                    
                     <div class="main_table">
                      <div class="sub_table1">
                            2. Date of accrediation with NIOS (attach photocopy od accrediation letter) :
                        </div><!-- sub_table1 -->
                      <div class="sub_table2">
                            <?=$result[4] ?>
                        </div><!-- sub_table2 -->
                    </div><!-- main_table -->    
                    
                    <div style="clear:both;"></div>  
                    
                    <div class="main_table">
                      <div class="sub_table1">
                            3. No of students registered with NIOS (students per class to be given) :
                        </div><!-- sub_table1 -->
                      <div class="sub_table2">
                            <?=$result[5]?>
                        </div><!-- sub_table2 -->
                    </div><!-- main_table -->    
                    
                    <div style="clear:both;"></div>
                    
                     <div class="main_table">
                      <div class="sub_table1">
                            4. <b>Requirement of funds </b>
                        </div><!-- sub_table1 -->
                    </div><!-- main_table -->    
                    
                     <div style="clear:both;"></div>
                    
                      <div class="main_table">
                          <div class="sub_table1">
                            (a). Accreditation fee £(copy of receipt be attached)
                        </div><!-- sub_table1 --> 
                        
                       <div class="sub_table11">
                            (i) Open basic Edn.(class 3,5 & 8) :
                        </div><!-- sub_table2 -->
                        
                        <div class="sub_table12">
                        Class 3 :&nbsp;&nbsp;
                           Number of students: <?=$result[6]?>                          
                        </div><!-- sub_table2 -->
                        
                         <div style="clear:both;"></div>  
                        
                        <div class="sub_table12">
                        Class 5 :&nbsp;&nbsp;
                           Number of students: <?=$result[7]?>                       
                        </div><!-- sub_table2 -->
                        
                         <div style="clear:both;"></div>  
                         
                        <div class="sub_table12">
                        Class 8 :&nbsp;&nbsp;
                           Number of students: <?=$result[8]?>                          
                        </div><!-- sub_table2 -->
                    </div><!-- main_table -->  
                    
                     <div style="clear:both;"></div>
                    
                      <div class="main_table">
                          <div class="sub_table1">
                            (b). Registration fees (includes cost of materials)
                        </div><!-- sub_table1 -->                        
                        
                        <div style="clear:both;"></div>
                        
                       <div class="sub_table11">
                            (i) No of Children :
                        </div><!-- sub_table12 -->
                        
                        <div class="sub_table12">
                        Boys : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$result[9]?><br/>
                        Girls : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$result[10]?>   <br/>     
                        CWSN :&nbsp;&nbsp;&nbsp;&nbsp <?=$result[11]?><br/>
                        Total :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?=$result[12]?>  <br/>    
                        Unit Cost : &nbsp;&nbsp;<?=$result[13]?>                         
                        </div><!-- sub_table12 -->
                        
                        <div style="clear:both;"></div>
                        
                         <div class="sub_table11">
                            (ii) Amount required (Total) :
                        </div><!-- sub_table11 -->
                        
                        <div class="sub_table12">
                        <?=$result[14]?>                          
                        </div><!-- sub_table12 -->
                        
                    </div><!-- main_table -->   
                    
                    <div style="clear:both;"></div>  
                    
                      <div class="main_table">
                          <div class="sub_table1">
                            (c). Examination fees
                        </div><!-- sub_table1 -->   
                        
                       <div class="sub_table2">
                            (i) No of Children :
                        <?=$result[15]?>   <br/>                       
                        
                            (ii) Unit Cost :
                        
                       <?=$result[16]?>    <br/>                      
                        
                            (ii) Amount required :
                        
                            <?=$result[17]?>                          
                        </div><!-- sub_table12 -->
                        
                    </div><!-- main_table -->   
                    
                    <div style="clear:both;"></div>  
                    
                    <div class="main_table">
                          <div class="sub_table1">
                            5. Grand total of Amt req.[5(a) + 5(b) + 5(c)] : 
                        </div><!-- sub_table1 -->                        
                        
                        <div class="sub_table12">
                            <?=$result[18]?>                      
                        </div><!-- sub_table12 -->
                        
                        </div>
                        <div style="clear:both;"></div>
                
               </div><!--print_style-->
                   <div class="noprint">
                         <div>
                             <a href="view_dd_reject_format4.php?id=<?php echo $result[1];?>" style="float: right;margin-top: 20px;">View Next Form</a>
                          </div>
                    </div>
               <?php }
                  
              }
             ?>

                        </form> 
                    </div>
                </div>
            </div>
            <div id="footerouter">
                <div id="footer">© All Rights Reserved. IT@School, General Education Department, Govt of Kerala. </div>
            </div>
        </div>	
    </body>
</html>
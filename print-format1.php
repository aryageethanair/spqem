<?php
include 'library/dbconnect.php';
//include_once("submit.inc.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link rel="stylesheet" type="text/css" href="style.css" />
        <script src="JSCal2/js/jscal2.js"></script>
        <script src="JSCal2/js/lang/en.js"></script>
        <link rel="stylesheet" type="text/css" href="JSCal2/css/jscal2.css" />
        <link rel="stylesheet" type="text/css" href="JSCal2/css/border-radius.css" />
        <link rel="stylesheet" type="text/css" href="JSCal2/css/steel/steel.css" />

        <script src="JSCal2/js/gen_validatorv31.js" type="text/javascript"></script>
    </head>
    <body>
        <style type="text/css">

@media print
{
#header {display:none;}
.main_table0{display:none;}
.noprint{display:none;}
#noprint_pg{display:none;}
.breadcrumb-list{display:none;}
#footer{display:none;}
#menubar{display:none;}
#print_style{font-size:12px;}
.main_heading{text-align: center;}
.application{text-align: center;}
.part{text-align: center;}
.print_new{border:none !important}
.main_heading{margin-right: 170px !important;}
}
</style>

        <div id="container">
            <div id="header">
                <div id="header_title">CENTRAL SPONSORED SCHEME FOR PROVIDING QUALITY EDUCATION IN MADRASA (SPQEM)</div>


            </div>
            <div id="menubar"><ul>
                    <li><a href="index.php">Home</a></li>
                    <li><a href="input.php">New Application</a></li>
                    <li><a href="edit.php">Edit Application</a></li>
                    <li><a href="print.php">Print Application</a></li>
                     <li><a href="status.php">View Application Status</a></li>
                    <li><a href="login.php">Login</a></li>

                </ul></div>
            <div id="body">

                <div id="inner" id="inner">
                    <div id="middle1" style="padding:20px;">

                        <div><font color="RED"><?php //echo $msg;  ?></font></div>
                    <form action="" method="post" name="test">                 
                
                <?php 
                if(isset($_GET['id'])){ 
               $serach=$_REQUEST['id'];
                $file_no=$_REQUEST['fileno'];
                  $radio6=$_REQUEST['value'];
                     $contact_exist=mysql_query("select * from wp_format1 where master_application_id='$serach'and master_id_no='$file_no' and status=1");
                  // echo "select * from wp_master where application_id='$serach'";
                   	if(mysql_num_rows($contact_exist)!=0){
                            $result=mysql_fetch_row($contact_exist);
                            $seracch= $result[1];?>
                   
                   <input type="hidden" name="application_no" id="name" value="<?=$result[1] ?>" />
                   <input type="hidden" name="id_no" id="name" value="<?=$result[2] ?>" />
                     <div style="border: 1px solid black;padding: 10px;width: 910px;margin-top: 14px;" class="print_new">
                   <div class="main_heading">
                        <div class="application">
                            <?php                     
                                echo "<b>FORMAT-I<br/>(Physical & Financial)</b>";
                            ?>
                        </div><!--application-->
                        <div class="part">
                            <?php                     
                                echo "<b>Fund Requirements for Teacher Salary & Teacher Training</b>";
                            ?>
                        </div><!--part--> 
                    </div><!--main_heading-->  

                 <div style="clear:both;"></div>
                 <div id="print_style">
                 <div class="main_table">
                            <div class="sub_table1">
                                1. Total Nos of teachers in position in Madrassa :
                                <br/> (For Modern Subjects) 
                            </div><!-- sub_table1 -->
                            <div class="sub_table2">
                            <?php echo $result[3];                            
                            ?>                            
                            </div><!-- sub_table2 -->
                       </div><!-- main_table -->
                       
                       <div style="clear:both;"></div>               
                  
                       <div class="main_table">
                            <div class="sub_table1">
                                2. Total No of children Madrassa :
                                <br/> (For Modern Subjects) 
                            </div><!-- sub_table1 -->
                            <div class="sub_table2">
                            <?php echo $result[4]; ?>
                            </div><!-- sub_table2 -->
                       </div><!-- main_table -->
                       
                       <div style="clear:both;"></div>               
               
                        <div class="main_table">
                            <div class="sub_table1">
                                3. Teacher pupil ratio :
                            </div><!-- sub_table1 -->
                            <div class="sub_table2">                           
                              Primary/Upper : <?=$result[5]?><br/>
                              Secondary/Sr.sec : <?=$result[6]?>
                            </div><!-- sub_table2 -->
                       </div><!-- main_table -->
                       
                       <div style="clear:both;"></div>
                          <div class="main_table">
                            <div class="sub_table1">
                                4. Total No of teachers proposed under SPQEM for the year (cannot exceed 3 per Madrassa) :
                            </div><!-- sub_table1 -->
                            <div class="sub_table2">                           
                             <?php echo $result[3];?>
                            </div><!-- sub_table2 -->
                       </div><!-- main_table -->
                       
                       <div style="clear:both;"></div>
                         <div class="main_table">
                            <div class="sub_table1">
                                5. How many teachers proposed in column 4 are existing & how many to be newly recruited :
                            </div><!-- sub_table1 -->
                            <div class="sub_table2">                           
                             Existing : <?php echo $result[7];?>
                             <br/>
                             To be recruited : <?=$result[8];?><br/>
                             Total :<?=$result[9];?>                            
                            </div><!-- sub_table2 -->
                       </div><!-- main_table -->
                       
                       <div style="clear:both;"></div>
                       
                       <div class="main_table">
                            <div class="sub_table1">
                                6. No of teachers proposed in column 4 to be deployed by level of education £ :
                            </div><!-- sub_table1 -->
                            <div class="sub_table2">                           
                                 For primary level :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                 <?=$result[10];?><br/>
                                 For upper primary level : 
                                 <?=$result[11];?><br/>
                                 For secondary level :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                  <?=$result[12];?><br/>
                                 For DSr.secondary level : 
                                <?=$result[13];?>
                            </div><!-- sub_table2 -->
                       </div><!-- main_table -->
                       
                       <div style="clear:both;"></div>
                       
                        <div class="main_table">
                            <div class="sub_table1">
                                7. No of teachers proposed in column 4 by subject £ £ :
                            </div><!-- sub_table1 -->
                            <div class="sub_table2">                           
                                 Sc : <?=$result[14];?><br/>
                                 Maths : <?=$result[15];?><br/>
                                 Lang : <?=$result[16];?><br/>
                                 Soc. Study : <?=$result[17];?><br/>
                                 Computer Edn : <?=$result[18];?>
                            </div><!-- sub_table2 -->
                       </div><!-- main_table -->
                       
                       <div style="clear:both;"></div>
                       
                        <div class="main_table">
                            <div class="sub_table1">
                                8. Total Amt. required (not more than 3 teachers per Madrasa) :
                            </div><!-- sub_table1 -->
                            <div class="sub_table2">  
                                 <b>Graduate Teacher</b><br/>                         
                                 No of teachers : <?php echo $result[19];?>
                                 <br/>
                                 Unit Cost : <?php echo "6000";?><br/>
                                 Total : <?=$result[20];?><br/>
                                 <b>Post Graduate B.Ed</b><br/>                         
                                 No of teachers : <?php echo $result[21];?>
                                 <br/>
                                 Unit Cost :<?php  echo "12000";?><br/>
                                 Total : <?=$result[22];?><br/>
                                 Grand Total : <?=$result[23];?>
                            </div><!-- sub_table2 -->
                       </div><!-- main_table -->
                       
                       <div style="clear:both;"></div>
                        <div class="main_table">
                            <div class="sub_table1">
                                9. No of teachers to be provided annual trg. in modern subjects, by level :
                            </div><!-- sub_table1 -->
                            <div class="sub_table2">  
                                  <?php 
                            $qty4=$result[24];
                            $news = explode(",",$qty4);
                            implode(" ",$news);
                            $ee= $news[0];
                            $se= $news[1];
                            $see= $news[2];
                            $sse= $news[3];
                            $lee= $news[4];
                            $lse= $news[5];
                            $mee= $news[6];
                            $mse= $news[7];
                            ?>                 
                                 <b>Sc</b><br/>                         
                                 EE* : <?=$ee?>
                                 SE* :<?=$se?><br/>
                                 <b>Soc .Sc</b><br/>                         
                                 EE* : <?=$see?>
                                 SE* : <?=$sse?><br/>
                                 <b>Lang</b><br/>                         
                                 EE* : <?=$lee?>
                                 SE* : <?=$lse?><br/>
                                 <b>Maths</b><br/>                         
                                 EE* :<?=$mee?>
                                 SE* : <?=$mse?><br/>
                                 Total : <?=$result[25]?>
                            </div><!-- sub_table2 -->
                       </div><!-- main_table -->
                       
                       <div style="clear:both;"></div>  
                       
                         <div class="main_table">
                            <div class="sub_table1">
                                10. Agency/s through which trg. will be imparted :
                            </div><!-- sub_table1 -->
                            <div class="sub_table2">                           
                            <?=$result[26];?>
                            </div><!-- sub_table2 -->
                       </div><!-- main_table -->
                       
                       <div style="clear:both;"></div> 
                       
                        <div class="main_table">
                            <div class="sub_table1">
                                11. Total Amt required :
                            </div><!-- sub_table1 -->
                            <div class="sub_table2">                           
                            Unit Cost : <?=$result[27];?><br/>
                            Total : <?=$result[28];?>
                            </div><!-- sub_table2 -->
                       </div><!-- main_table -->
                       
                       <div style="clear:both;"></div>
               </div><!--print_style-->
                  </div> <!--print_style-->
                   <div class="noprint">
 <div style="margin-top: 40px;">
                    <div>
                    <!--    <input type="submit" name="submit" value="Print"  onclick="javascript:window.print();">-->
                    </div>
                       
                        <div style="float:right;margin-top: -14px;margin-right: 77px;">
                            <a href="print-format2.php?id=<?php echo $result[1]; ?>&fileno=<?php echo $result[2]; ?>"  onclick="javascript:window.print();">Print</a>
                        </div>

                   </div>
                   </div>
                 <?php }else{
if($serach!=$seracch){
                            echo  "<div style='color:red;font-weight:bold;padding-top:10px;text-align:center;'>Application not found!</div>";}
}
                      }
                
                ?>

                            </form> 
                    </div>
                </div>
            </div>
            <div id="footerouter">
                <div id="footer">© All Rights Reserved. IT@School, General Education Department, Govt of Kerala. </div>
            </div>
        </div>
    </body>
</html>

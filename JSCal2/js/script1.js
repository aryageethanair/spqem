// JavaScript Document


// Check browser version

var isNav4 = false, isNav5 = false, isIE4 = false
var strSeperator = "/"; 
// If you are using any Java validation on the back side you will want to use the / because 
// Java date validations do not recognize the dash as a valid date separator.
var vDateType = 3; // Global value for type of date format
//                1 = mm/dd/yyyy
//                2 = yyyy/dd/mm  (Unable to do date check at this time)
//                3 = dd/mm/yyyy
var vYearType = 4; //Set to 2 or 4 for number of digits in the year for Netscape
var vYearLength = 2; // Set to 4 if you want to force the user to enter 4 digits for the year before validating.
var err = 0; // Set the error code to a default of zero
if(navigator.appName == "Netscape") {
if (navigator.appVersion < "5") {
isNav4 = true;
isNav5 = false;
}
else
if (navigator.appVersion > "4") {
isNav4 = false;
isNav5 = true;
   }
}
else {
isIE4 = true;
}

function DateFormat(vDateName, vDateValue, e, dateCheck, dateType) 
{
vDateType = dateType;
// vDateName = object name
// vDateValue = value in the field being checked
// e = event
// dateCheck 
// True  = Verify that the vDateValue is a valid date
// False = Format values being entered into vDateValue only
// vDateType
// 1 = mm/dd/yyyy
// 2 = yyyy/mm/dd
// 3 = dd/mm/yyyy

//Enter a tilde sign for the first number and you can check the variable information.
if (vDateValue == "~") {
alert("AppVersion = "+navigator.appVersion+" \nNav. 4 Version = "+isNav4+" \nNav. 5 Version = "+isNav5+" \nIE Version = "+isIE4+" \nYear Type = "+vYearType+" \nDate Type = "+vDateType+" \nSeparator = "+strSeperator);
vDateName.value = "";
vDateName.focus();
return true;
}

var whichCode = (window.Event) ? e.which : e.keyCode;
// Check to see if a seperator is already present.
// bypass the date if a seperator is present and the length greater than 8
if (vDateValue.length > 8 && isNav4) {
if ((vDateValue.indexOf("-") >= 1) || (vDateValue.indexOf("/") >= 1))
return true;
}

//Eliminate all the ASCII codes that are not valid
var alphaCheck = " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/-";
if (alphaCheck.indexOf(vDateValue) >= 1) {
if (isNav4) {
vDateName.value = "";
vDateName.focus();
vDateName.select();
return false;
}
else {
vDateName.value = vDateName.value.substr(0, (vDateValue.length-1));
return false;
   }
}
if (whichCode == 8) //Ignore the Netscape value for backspace. IE has no value
return false;
else {
//Create numeric string values for 0123456789/
//The codes provided include both keyboard and keypad values
var strCheck = '47,48,49,50,51,52,53,54,55,56,57,58,59,95,96,97,98,99,100,101,102,103,104,105';
if (strCheck.indexOf(whichCode) != -1) {
if (isNav4) {
if (((vDateValue.length < 6 && dateCheck) || (vDateValue.length == 7 && dateCheck)) && (vDateValue.length >=1)) {
alert("Invalid Date\nPlease Re-Enter");
vDateName.value = "";
vDateName.focus();
vDateName.select();
return false;
}
if (vDateValue.length == 6 && dateCheck) {
var mDay = vDateName.value.substr(2,2);
var mMonth = vDateName.value.substr(0,2);
var mYear = vDateName.value.substr(4,4)
//Turn a two digit year into a 4 digit year
if (mYear.length == 2 && vYearType == 4) {
var mToday = new Date();
//If the year is greater than 30 years from now use 19, otherwise use 20
var checkYear = mToday.getFullYear() + 30; 
var mCheckYear = '20' + mYear;
if (mCheckYear >= checkYear)
mYear = '19' + mYear;
else
mYear = '20' + mYear;
}

var vDateValueCheck = mMonth+strSeperator+mDay+strSeperator+mYear;
alert(vDateValueCheck);
if (!dateValid(vDateValueCheck)) {
alert("Invalid Date\nPlease Re-Enter");
vDateName.value = "";
vDateName.focus();
vDateName.select();
return false;
}
return true;
}
else {
// Reformat the date for validation and set date type to a 1
if (vDateValue.length >= 8  && dateCheck) {
if (vDateType == 1) // mmddyyyy
{
var mDay = vDateName.value.substr(2,2);
var mMonth = vDateName.value.substr(0,2);
var mYear = vDateName.value.substr(4,4)
vDateName.value = mMonth+strSeperator+mDay+strSeperator+mYear;
}
if (vDateType == 2)// yyyymmdd
{
var mYear = vDateName.value.substr(0,4)
var mMonth = vDateName.value.substr(4,2);
var mDay = vDateName.value.substr(6,2);
vDateName.value = mYear+strSeperator+mMonth+strSeperator+mDay;
}
if (vDateType == 3)// ddmmyyyy
{
var mMonth = vDateName.value.substr(2,2);
var mDay = vDateName.value.substr(0,2);
var mYear = vDateName.value.substr(4,4)
vDateName.value = mDay+strSeperator+mMonth+strSeperator+mYear;
}
//Create a temporary variable for storing the DateType and change
//the DateType to a 1 for validation.
var vDateTypeTemp = vDateType;
vDateType = 1;
var vDateValueCheck = mMonth+strSeperator+mDay+strSeperator+mYear;
if (!dateValid(vDateValueCheck)) {
alert("Invalid Date\nPlease Re-Enter");
vDateType = vDateTypeTemp;
vDateName.value = "";
vDateName.focus();
vDateName.select();
return false;
}
vDateType = vDateTypeTemp;
return true;
}
else {
if (((vDateValue.length < 8 && dateCheck) || (vDateValue.length == 9 && dateCheck)) && (vDateValue.length >=1)) {
alert("Invalid Date\nPlease Re-Enter");
vDateName.value = "";
vDateName.focus();
vDateName.select();
return false;
         }
      }
   }
}
else {
// Non isNav Check
if (((vDateValue.length < 8 && dateCheck) || (vDateValue.length == 9 && dateCheck)) && (vDateValue.length >=1)) {
alert("Invalid Date\nPlease Re-Enter");
vDateName.value = "";
vDateName.focus();
return true;
}
// Reformat date to format that can be validated. mm/dd/yyyy
if (vDateValue.length >= 8 && dateCheck) {
// Additional date formats can be entered here and parsed out to
// a valid date format that the validation routine will recognize.
if (vDateType == 1) // mm/dd/yyyy
{
var mMonth = vDateName.value.substr(0,2);
var mDay = vDateName.value.substr(3,2);
var mYear = vDateName.value.substr(6,4)
}
if (vDateType == 2) // yyyy/mm/dd
{
var mYear = vDateName.value.substr(0,4)
var mMonth = vDateName.value.substr(5,2);
var mDay = vDateName.value.substr(8,2);
}
if (vDateType == 3) // dd/mm/yyyy
{
var mDay = vDateName.value.substr(0,2);
var mMonth = vDateName.value.substr(3,2);
var mYear = vDateName.value.substr(6,4)
}
if (vYearLength == 4) {
if (mYear.length < 4) {
alert("Invalid Date\nPlease Re-Enter");
vDateName.value = "";
vDateName.focus();
return true;
   }
}
// Create temp. variable for storing the current vDateType
var vDateTypeTemp = vDateType;
// Change vDateType to a 1 for standard date format for validation
// Type will be changed back when validation is completed.
vDateType = 1;
// Store reformatted date to new variable for validation.
var vDateValueCheck = mMonth+strSeperator+mDay+strSeperator+mYear;
if (mYear.length == 2 && vYearType == 4 && dateCheck) {
//Turn a two digit year into a 4 digit year
var mToday = new Date();
//If the year is greater than 30 years from now use 19, otherwise use 20
var checkYear = mToday.getFullYear() + 30; 
var mCheckYear = '20' + mYear;
if (mCheckYear >= checkYear)
mYear = '19' + mYear;
else
mYear = '20' + mYear;
vDateValueCheck = mMonth+strSeperator+mDay+strSeperator+mYear;
// Store the new value back to the field.  This function will
// not work with date type of 2 since the year is entered first.
if (vDateTypeTemp == 1) // mm/dd/yyyy
vDateName.value = mMonth+strSeperator+mDay+strSeperator+mYear;
if (vDateTypeTemp == 3) // dd/mm/yyyy
vDateName.value = mDay+strSeperator+mMonth+strSeperator+mYear;
} 
if (!dateValid(vDateValueCheck)) {
alert("Invalid Date\nPlease Re-Enter");
vDateType = vDateTypeTemp;
vDateName.value = "";
vDateName.focus();
return true;
}
vDateType = vDateTypeTemp;
return true;
}
else {
if (vDateType == 1) {
if (vDateValue.length == 2) {
vDateName.value = vDateValue+strSeperator;
}
if (vDateValue.length == 5) {
vDateName.value = vDateValue+strSeperator;
   }
}
if (vDateType == 2) {
if (vDateValue.length == 4) {
vDateName.value = vDateValue+strSeperator;
}
if (vDateValue.length == 7) {
vDateName.value = vDateValue+strSeperator;
   }
} 
if (vDateType == 3) {
if (vDateValue.length == 2) {
vDateName.value = vDateValue+strSeperator;
}
if (vDateValue.length == 5) {
vDateName.value = vDateValue+strSeperator;
   }
}
return true;
   }
}
if (vDateValue.length == 10&& dateCheck) {
if (!dateValid(vDateName)) {
// Un-comment the next line of code for debugging the dateValid() function error messages
//alert(err);  
alert("Invalid Date\nPlease Re-Enter");
vDateName.focus();
vDateName.select();
   }
}
return false;
}
else {
// If the value is not in the string return the string minus the last
// key entered.
if (isNav4) {
vDateName.value = "";
vDateName.focus();
vDateName.select();
return false;
}
else
{
vDateName.value = vDateName.value.substr(0, (vDateValue.length-1));
return false;
         }
      }
   }
}
function dateValid(objName) {
var strDate;
var strDateArray;
var strDay;
var strMonth;
var strYear;
var intday;
var intMonth;
var intYear;
var booFound = false;
var datefield = objName;
var strSeparatorArray = new Array("-"," ","/",".");
var intElementNr;
// var err = 0;
var strMonthArray = new Array(12);
strMonthArray[0] = "Jan";
strMonthArray[1] = "Feb";
strMonthArray[2] = "Mar";
strMonthArray[3] = "Apr";
strMonthArray[4] = "May";
strMonthArray[5] = "Jun";
strMonthArray[6] = "Jul";
strMonthArray[7] = "Aug";
strMonthArray[8] = "Sep";
strMonthArray[9] = "Oct";
strMonthArray[10] = "Nov";
strMonthArray[11] = "Dec";
//strDate = datefield.value;
strDate = objName;
if (strDate.length < 1) {
return true;
}
for (intElementNr = 0; intElementNr < strSeparatorArray.length; intElementNr++) {
if (strDate.indexOf(strSeparatorArray[intElementNr]) != -1) {
strDateArray = strDate.split(strSeparatorArray[intElementNr]);
if (strDateArray.length != 3) {
err = 1;
return false;
}
else {
strDay = strDateArray[0];
strMonth = strDateArray[1];
strYear = strDateArray[2];
}
booFound = true;
   }
}
if (booFound == false) {
if (strDate.length>5) {
strDay = strDate.substr(0, 2);
strMonth = strDate.substr(2, 2);
strYear = strDate.substr(4);
   }
}
//Adjustment for short years entered
if (strYear.length == 2) {
strYear = '20' + strYear;
}
strTemp = strDay;
strDay = strMonth;
strMonth = strTemp;
intday = parseInt(strDay, 10);
if (isNaN(intday)) {
err = 2;
return false;
}
intMonth = parseInt(strMonth, 10);
if (isNaN(intMonth)) {
for (i = 0;i<12;i++) {
if (strMonth.toUpperCase() == strMonthArray[i].toUpperCase()) {
intMonth = i+1;
strMonth = strMonthArray[i];
i = 12;
   }
}
if (isNaN(intMonth)) {
err = 3;
return false;
   }
}
intYear = parseInt(strYear, 10);
if (isNaN(intYear)) {
err = 4;
return false;
}
if (intMonth>12 || intMonth<1) {
err = 5;
return false;
}
if ((intMonth == 1 || intMonth == 3 || intMonth == 5 || intMonth == 7 || intMonth == 8 || intMonth == 10 || intMonth == 12) && (intday > 31 || intday < 1)) {
err = 6;
return false;
}
if ((intMonth == 4 || intMonth == 6 || intMonth == 9 || intMonth == 11) && (intday > 30 || intday < 1)) {
err = 7;
return false;
}
if (intMonth == 2) {
if (intday < 1) {
err = 8;
return false;
}
if (LeapYear(intYear) == true) {
if (intday > 29) {
err = 9;
return false;
   }
}
else {
if (intday > 28) {
err = 10;
return false;
      }
   }
}
return true;
}
function LeapYear(intYear) {
if (intYear % 100 == 0) {
if (intYear % 400 == 0) { return true; }
}
else {
if ((intYear % 4) == 0) { return true; }
}
return false;
}
//  End 



		

function checkname(e)
{
var unicode=e.charCode? e.charCode : e.keyCode
//alert(unicode);
if(unicode!=8 && unicode!=32 && unicode!=46 && unicode!=9)
{
	if(unicode<65 || unicode > 122)
		return false
		else if(unicode > 90 && unicode < 97)
		return false 
}
}
function Validdated(dtbox,dtval)
{ //alert(dtval)
	if (dtval.length<10) 
		{	alert("The date format is nor correct pls enter in 'dd/mm/yyyy' format");
			dtbox.focus();
			dtbox.value='';
			return(false);
		}
		
		else{
			//alert(dtval);
		
		var leap=0;
		var mflg=0;
		
		var mon1=dtval.substr(3,2);
		//alert(mon1);
		var month = parseInt(mon1,10);
		 //alert(month);
		   if ((month < 1) || (month > 12)) 
   		    {
		     alert("The month is invalid");
			 dtbox.value=''
			dtbox.focus();
			return(false);
			}
   // Validation of day
		   day = parseInt(dtval.substr(0,2),10);
		   if (day < 1) 
		   {	    
		    	alert("This Date is invalid with this month");
			 	dtbox.value=''
				dtbox.focus();
				return(false);
   			}
			var year=parseInt(dtval.substr(6,4),10);
			//alert(day+month+year)
   // Validation leap-year / february / day 
		   if ((year % 4 == 0) || (year % 100 == 0) || (year % 400 == 0)) 
		   {
		      leap = 1;
		   }
		   //alert(day);
		   if ((month == 2) && (leap == 1) && (day > 29)) 
		   {
		    	alert("This Date is invalid with this Month and year");
			 	dtbox.value=''
				dtbox.focus();
				return(false);
		   }
		   if ((month == 2) && (leap != 1) && (day > 28)) 
		   {
		      	alert("This day is invalid with this Month and Year");
			 	dtbox.value=''
				dtbox.focus();
				return(false);
   		   }
   // Validation of other months
		   if ((day > 31) && ((month == "01") || (month == "03") || (month == "05") || (month == "07") || (month == "08") || (month == "10") || (month == "12"))) 
		   {
			    alert("This day is invalid with this Month and Year");
			 	dtbox.value=''
				dtbox.focus();
				return(false);
		      
		   }
		   if ((day > 30) && ((month == "04") || (month == "06") || (month == "09") || (month == "11"))) 
		   {
		      	alert("This day is invalid with this Month and Year");
			 	dtbox.value=''
				dtbox.focus();
				return(false);
		   }
   // if 00 ist entered, no error, deleting the entry 
		   if ((day == 0) && (month == 0) && (year == 00)) 
		   {
		      err = 0; day = ""; month = ""; year = ""; seperator = "";
		   }
   // if no error, write the completed date to Input-Field (e.g. 13.12.2001) 
		   if (err == 0) 
		   {
		      return day + seperator + month + seperator + year;
		   }
		  
  // Error-message if err != 0 
			else
		    {
				return false;
			 }
		
		
		
		
	   //check date filed contains characters 
 	    if (datevar.indexOf("/") == -1) 
		   {	
			return(false);
		   }
		    var  goodchars="0123456789/"
			var i=0
			num=datevar;
			for(i=0;i<=num.length-1;i++)
			{
				if(goodchars.indexOf(num.charAt(i))==-1)
					{
						return false;
					}
			}
		   // Delete all chars except 0..9 
		   for (i = 0; i < DateValue.length; i++) 
		   {
			  if (checkstr.indexOf(DateValue.substr(i,1)) >= 0) 
			  	{
			     DateTemp = DateTemp + DateValue.substr(i,1);
	  			}
		   }
		   DateValue = DateTemp;
	   // Always change date to 8 digits - string
  		// if year is entered as 2-digit / always assume 20xx 
		   if (DateValue.length == 6) 
		   	{
	          DateValue = DateValue.substr(0,4) + '20' + DateValue.substr(4,2); 
			 }
		   if (DateValue.length != 8) 
		   	{
		      err = 19;
			}
   // year is wrong if year = 0000 
		   year = DateValue.substr(4,4);
		   if (year == 0) 
		   	{
		      err = 20;
			}
			if(year<1950)
			 {
			  err=28
			 }
   // Validation of month
		  
			 
		}
}


function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
//-->

function getschool1(val)
{
    xmlHttp=GetXmlHttpObject()
		
    if (xmlHttp==null)
    {
    alert ("Browser does not support HTTP Request")
    return
    }
    var url="./ajax/get_school1.php?sch="+val;

    xmlHttp.onreadystatechange=statechanging
    xmlHttp.open("GET",url,true)
    xmlHttp.send(null)	

}
function getsch(val)
{//alert("hai");

xmlHttp=GetXmlHttpObject()
		
			if (xmlHttp==null)
 			{
 			alert ("Browser does not support HTTP Request")
 			return
 			}
			var url="./ajax/get_school.php?sch="+val;
		
			xmlHttp.onreadystatechange=statechanging
			xmlHttp.open("GET",url,true)
			xmlHttp.send(null)	
}

function statechanging() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete"){ document.getElementById("schname").innerHTML=xmlHttp.responseText;}
} 	
function GetXmlHttpObject()
{
	var xmlHttp=null;
	try
 	{
 		// Firefox, Opera 8.0+, Safari
 		xmlHttp=new XMLHttpRequest();
 	}
	catch (e)
	{
 		//Internet Explorer
 		try
  		{
  		xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
  		}
 		catch (e)
  		{
  		xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
  		}
	}
return xmlHttp;
}
	
function isnum(val)
{
if(isNaN(val.value))
	{ 
	alert("You can Enter only digits here");
	val.value='';
	val.focus();
	return false;
	}
}
function validate()
{
	if(document.ex_sh.scode.value=='' || document.ex_sh.scode.value=='0')
		{
			alert("please enter the School Code	");
			document.ex_sh.scode.focus();
			return false;
		}
		
		if(document.ex_sh.sname.value=='auto')
		{
			alert("Please press 'TAB KEY' after entering the School Code. ");
			document.ex_sh.scode.focus();
			return false;
		}
		
		if(document.ex_sh.schflg.value!='1')
		{
			alert("The Entered School code is not Valid");
			document.ex_sh.scode.focus();
			return false;
		}
		
		if(document.ex_sh.name_hm.value=='')
		{
			alert("Please enter the Name of HM");
			document.ex_sh.name_hm.focus();
			return false;
		}
		if(document.getElementById("radio1").checked==false && document.getElementById("radio2").checked==false)
		{
			alert("Select any of the Irregularity Option");
			return false;
		}
		if((document.ex_sh.rice.value=='') && (document.ex_sh.gg.value=='') && (document.ex_sh.bgb.value=='') && (document.ex_sh.bgs.value=='') && (document.ex_sh.lobia.value=='') && (document.ex_sh.pd.value=='') && (document.ex_sh.td.value=='') && (document.ex_sh.ggd.value==''))
		{
			alert("Please enter any of the excess/shortage quantity in Kg.");
			document.ex_sh.rice.focus();
			return false;
		}
		if(document.ex_sh.reason.value=='')
		{
			alert("please enter the Reason for the Irregularity");
			document.ex_sh.reason.focus();
			return false;
		}
		if(document.ex_sh.date_visit.value=='')
		{
			alert("	please enter the Inspecting Date	");
			document.ex_sh.date_visit.focus();
			return false;
		}
		if(document.ex_sh.date_visit.value==document.ex_sh.date_today.value)
		{
			var r;  
			var msg="The Inspecting date is set as today-"+document.ex_sh.date_visit.value+" Are You sure?";
  			r=confirm(msg);
  			if(r==true)
  				 	{  
    			
					}
				else 
					{
					document.ex_sh.date_visit.value='';
					document.ex_sh.date_visit.focus();
					return false;
					}
		}
		
		if(document.ex_sh.name_inspect.value=='')
		{
			alert("please enter the Name of Inspectiong Officer");
			document.ex_sh.name_inspect.focus();
			return false;
		}
}
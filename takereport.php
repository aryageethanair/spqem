<?php 
session_start();
if(!($_SESSION['type']=='N' && $_SESSION['username'])){
    echo "<script>window.location='login.php'</script>";    
}
include 'library/dbconnect.php';
$myusername=$_SESSION['username']; 
$currentYear=date("Y");
$nextYear=$currentYear+1;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dth">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<link rel="stylesheet" type="text/css" href="style.css" />
<script src="JSCal2/js/jscal2.js"></script>
	<script src="JSCal2/js/lang/en.js"></script>
	<link rel="stylesheet" type="text/css" href="JSCal2/css/jscal2.css" />
	<link rel="stylesheet" type="text/css" href="JSCal2/css/border-radius.css" />
	<link rel="stylesheet" type="text/css" href="JSCal2/css/steel/steel.css" />
<script src="JSCal2/js/gen_validatorv31.js" type="text/javascript"></script>
<style type="text/css">
@media print { 
    .heading_heads{
        font-size: 35px !important;
        font-weight: bold !important;
    }
    table tr{      
        font-size: 35px !important;
        font-weight: bold !important;
    }  
    .footer_sign{
        font-size: 35px !important;
        font-weight: bold !important;
    }
    .signature{
        font-size: 35px !important;
        font-weight: bold !important;
    }
/*    #print_div
    {
        font-size: 35px !important;
    }*/
}
</style>
<style type="text/css">
    .print{
        color:blue;
        cursor: pointer;
        padding: 10px;
    }
</style>
<div id="container">
<div id="header">
	<div id="header_title">CENTRAL SPONSORED SCHEME FOR PROVIDING QUALITY EDUCATION IN MADRASA (SPQEM)</div>
</div>
<div id="menubar">
    <ul>
        <li><a href="DPI.php">Home</a></li>      
        <li><a href="ddacceptlist.php">Total No of Accepted List By DD</a></li>
        <li><a href="ddrejectlist.php">Total No of Rejected List By DD</a></li> 
        <li><a href="takereport.php">Consolidated Report</a></li>
        <li><a href="logout.php">Logout</a></li>
        <li style="float: right;color: blue;">Welcome <?php echo $myusername;?></li>
    </ul>
</div>
    <div id="body" style="overflow: auto;">
        <div class="print" onClick="printdiv('print_div')" style="float: right;" id ="printbtn">Click Here To Take Print</div>
        <div id="print_div">
        <div class="heading_heads">
            CONSOLIDATED REPORT IN <?php echo $currentYear .'-'.$nextYear;?>
        </div>
<!--        <div style="width: 100px ! important; border: 1px solid red;">-->
        <table  border='1'>
              <tr style="font-size: 14px;">
                <th rowspan="3">Application No</th>
                <th rowspan='3'>District</th>
                <th rowspan='3'>Name & Address</th>
                <th rowspan='3'>Level</th>
                <th colspan='5'>Date of Estt. & Regn. With Wakaf or NIOS</th>
                <th rowspan="3">Details of Assistance received in the past</th>
                <th rowspan="3">No. of Students</th>
                <th rowspan="3">If NIOS Accreditted, No. of Students appeared for Certification of Classes 3,5,8,10 & 12</th>
                <th rowspan="3">Total No. of Teachers for whome Assistance is proposed</th>
                <th rowspan="3">Details of Equipments & Teaching Learning Materials (As per Format-II)</th>
                <th rowspan="3">Details of Books(As per Format-II)</th>
                <th rowspan="3">Whether Accrediation with NIOS is required</th>
                <th colspan='6'>Amount Required</th>
                <th rowspan='3'>Remarks</th>
                <th rowspan="3">Whether Proposal have been received in the specified application form and Annexures</th>
                <th rowspan="3">Whether Proposals with NIOS Linkages have the Concurrance of NIOS</th>
                <th rowspan="3">Whether Proposals have been scruitanized and are in accordance with the eligibility and financial Parameters of the scheme.</th>
                <th rowspan="3">Whether state Government has fecilitated and made arrangements for training of Madrasa teachers </th>
                <th rowspan="3">Whether it has been ascertained that Madrasa's being recommended for funding are not duplicating funds receiving from states or GOI Schemes</th>
                <th rowspan="3">Whether the madrassa is being recommended has furnished Audited accounts , UCs ,Anual Reports Which were due </th>
                <th rowspan="3">Priority No.</th>
            </tr>
              <tr style="font-size: 14px;">
                <th rowspan='2'>Date of Estt.</th>
                <th colspan='4'>Date of Registration</th>
                <th colspan='3'>Format I(Teachers)</th>
                <th>Format II</th>
                <th>Format III</th>
                <th rowspan='2'>G.Total</th>
            </tr>
            <tr style="font-size: 14px;">
                <th>MB</th>
                <th>WB</th>
                <th>NIOS</th>
                <th>Char.Regn</th>
                <th>Graduate/PG/Bed</th>
                <th>Total Salary</th>
                <th>Training</th>
                <th>Materials & Eqpts</th>
                <th>NIOS</th>
            </tr>       
            <?php
                $get_Details=mysql_query("select master.*,format2.* from wp_master as master,wp_format2 as format2 where master.status_dpi=4 and master.`application_id`=format2.mater_application_id ORDER BY master.country ASC");           
                $i=1;
                while($res=mysql_fetch_array($get_Details)){
            ?>                
                <tr  style="font-size: 14px;">
                    <td style="text-align: center;"><?php echo $i;?></td>
                    <td style="text-align: center;"><?php echo $res['country'];?></td>
                    <td><?php echo $res['society_address'];?></td>
                    <td>
                        <?php $school_level= $res['school_level'];
                        if($school_level==1){
                            echo "Primary Level";
                        }
                        if($school_level==2){
                            echo "Upper Primary Level";
                        }
                        if($school_level==3){
                            echo "Secondary Level";
                        }
                        if($school_level==4){
                            echo "Sr.Secondary Level";
                        }
                        ?>
                    </td>
                    <td>
                        <?php $reg_date= $res['reg_date'];
                        if($reg_date==''){
                           echo "<div style='text-align:center'>-</div>" ;
                        }else{                        
                            $reg_dates=explode("/",$reg_date);
                            echo $reg_dates[2];
                        }
                        ?>
                    </td>                    
                        <?php  $madrasa= $res['madrasa'];?>                    
                    <td> 
                        <?php
                            if($madrasa==2){
                               echo "Madrasa" ;
                            }
                        ?>
                    </td>
                    <td>
                        <?php
                            if($madrasa==1){
                               echo "WAKF Acts" ;
                            }
                        ?>
                    </td>
                    <td>
                         <?php
                            if($madrasa==3){
                               echo "NIOS" ;
                            }
                        ?>
                    </td> 
                    <td>
                         <?php
                            if($madrasa==4){
                               echo "OTHERS" ;
                            }
                         ?>
                    </td>
                    <td style="text-align: center;"><?php echo "NIL";?></td>
                    <td style="text-align: center;"><?php echo $res['total'];?></td>
                    <td style="text-align: center;"><?php echo "N";?></td>
                    <td style="text-align: center;"><?php echo $res['drop1'];?></td>
                    <td style="text-align: left;">
                        <?php  $science_kit=$res['science_kit']."/".$maths_kit=$res['maths_kit']."/".$science_lab=$res['science_lab']."/".$computer_lab=$res['computer_lab'];?>
                        <?php 
                            if($science_kit!=''){
                                echo "SCIENCE KIT";
                            }
                            if($maths_kit!=''){
                                echo ",<br/>MATHS KIT";
                            }
                            if($science_lab!=''){
                                echo ",<br/>SCIENCE LAB";
                            }
                            if($computer_lab!=''){
                                echo ",<br/>COMPUTER LAB";
                            }
                        ?>
                    </td>
                    <td  style="text-align: center;"><?php echo "LIBRARY BOOKS";?></td>
                    <td style="text-align: center;"><?php "-";?></td>
                    <td>  
                       <?php  $qualification_teacher=$res['qualification_teacher'];?>
                        <?php 
                           $qualification_teach=explode(",",$qualification_teacher);
                           $array1=$qualification_teach[0];
                           $array2=$qualification_teach[1];
                           $array3=$qualification_teach[2];
                           if($array1==2){
                               echo "Graduation";
                           } else{
                             if($array1==1){
                               echo "<br/>Post Graduation";
                             }
                           }
                           
                           if($array2==2){
                               echo "<br/>Graduation";
                           } else{
                             if($array2==1){
                               echo "<br/>Post Graduation";
                             }
                           }
                           
                           if($array3==2){
                               echo "<br/>Graduation";
                           } else{
                             if($array3==1){
                               echo "<br/>Post Graduation";
                             }
                           
                           }
                            ?>
                        </td>
                        <td><?php echo $salary	=$res['salary'];?></td>
                        <td style="text-align: center;"><?php echo $training=$res['training'];?></td>
                        <td style="text-align: center;"><?php echo $grand_total=$res['grand_total'];?></td>
                        <td style="text-align: center;"><?php echo $nios="-";?></td>
                       
                        <td><?php echo $g_total=$grand_total+$training+$nios+$salary;?></td>
                        <td></td>
                        <td style="text-align: center;"><?php echo "Y";?></td>
                        <td style="text-align: center;"><?php echo "N";?></td>
                        <td style="text-align: center;"><?php echo "Y";?></td>
                        <td style="text-align: center;"><?php echo "Y";?></td>
                        <td style="text-align: center;"><?php echo "Y";?></td>
                        <td style="text-align: center;"><?php echo "Y";?></td>
                        <td></td>                       
                </tr>            
            
            <?php
                $i++;
                }
            ?>
           
        </table>

        <div class="footer_sign">
               The applications has been examined and it is certified that the organizations are eligible for assistance and has the capacity for taking up a programme applied for
        </div>
        <div class="signature">
              Signature of Member Secretary of SGIAC
        </div>
      </div>
    </div>
    <div id="footerouter">
        <div id="footer">© All Rights Reserved. IT@School, General Education Department, Govt of Kerala. </div>
    </div>
</div>
<script type="text/javascript">
function printdiv(printdivname)
{
var headstr = "<html><head><title>Cook Salary Slip</title></head><body>";
var footstr = "</body>";
var newstr = document.getElementById(printdivname).innerHTML;
var oldstr = document.body.innerHTML;
document.body.innerHTML = headstr+newstr+footstr;
window.print();
document.body.innerHTML = oldstr;
return false;
}  
</script>
<?php
session_start();
if (!($_SESSION['type'] == 'DD' && $_SESSION['username'])) {
    echo "<script>window.location='login.php'</script>";
}
include 'library/dbconnect.php';
$myusername = $_SESSION['username'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link rel="stylesheet" type="text/css" href="style.css" />
        <script src="JSCal2/js/jscal2.js"></script>
        <script src="JSCal2/js/lang/en.js"></script>
        <link rel="stylesheet" type="text/css" href="JSCal2/css/jscal2.css" />
        <link rel="stylesheet" type="text/css" href="JSCal2/css/border-radius.css" />
        <link rel="stylesheet" type="text/css" href="JSCal2/css/steel/steel.css" />
        <script src="JSCal2/js/gen_validatorv31.js" type="text/javascript"></script>
        <script src="js/jQuery1.10.js" type="text/javascript"></script>
    </head>
           <style type="text/css">

            @media print
            {               
                .main_table0{display:none;}
                .noprint{display:none;}
                .breadcrumb-list{display:none;}
                #footer{display:none;}
                #menubar{display:none;}
                .print_heading{display:block !important;padding-bottom: 20px;color: mediumblue;}
                #header_title{margin-right:250px !important;}
                .container{padding:20px;width:300px;}
            /*    .print_new{border:none !important}
                .main_heading{margin-right: 120px !important;}
                .id_no{margin-right: 30px !important;}*/
                /*.sub_table2_words{margin-right: 120px !important;}*/
                /*.sub_table8{margin-right: 120px !important;}*/
            }
        </style>
    <body>
        <div id="container">

            <div id="header">
                <div id="header_title">CENTRAL SPONSORED SCHEME FOR PROVIDING QUALITY EDUCATION IN MADRASA (SPQEM)</div>  
            </div>
            <div id="menubar"><ul>
                    <li><a href="DD.php">Home</a></li>
                    <li><a href="printreport.php">Print Report</a></li>
                    <li><a href="logout.php">Logout</a></li>
                    <li style="float: right;color: blue;">Welcome <?php echo $myusername; ?></li>
                </ul>
            </div>
            <div class="noprint">
                <ul id="tabs">
                    <li><a id="tab1">Approved List</a></li>
                    <li><a id="tab2">Rejected List</a></li>
                </ul>
            </div>
            <div class="print_style">
                <div class="container" id="tab1C">
                    <table border="1" style="border-color: #FFFFFF;font-size: 14px;" cellspacing="0" align="center" width="750">
                    
                        <div style="float:right;margin-right: 102px;margin-bottom: 7px;color: mediumBlue;">
                            <a class="noprint" onclick="javascript:window.print();">Click Here for Print</a>
                        </div>
                        
                        <div class="print_heading" style="display:none;">Approved List Details</div>
                           <div style="clear:both;"></div>
                        
                        <tr>																		 
                            <th style="color: #FF0000">Sl. No</th>
                            <th style="color: #FF0000">Application No:</th>
                            <th style="color: #FF0000">Application Id:</th>
                            <th style="color: #FF0000; width:150px;">Organization Name</th>
                            <th style="color: #FF0000; width:150px;">DD Memo</th>
                        </tr>
                        <?php
                        $district = mysql_query("select district from wp_user_master where username='$myusername'");
                        $districts = mysql_fetch_array($district);
                        $district_name = $districts[0];
                        $get_contacts = mysql_query("select * from wp_master where namehere='$district_name' and status=2");
                        $i = 1;
                        while ($results = mysql_fetch_array($get_contacts)) {
                            //  print_r($results);
                            $appl_id=$results['application_id'];
                            ?>
                            <tr>
                                <td  style="text-align:center;width:10px;"><?= $i ?></td>
                                <td  style="text-align:center;width:20px;"><?= $results['application_id'] ?></td>
                                <td  style="text-align:center;width:20px;"><?= $results['id_no'] ?></td>
                                <td style="width:50px;"><?= $results['society_address'] ?></td>
                                <td>
                                    <?php
                                    $dd_comment = mysql_query("select comments from wp_dd_comments where district='$district_name' and master_application_no=$appl_id");
                                    $dd_comments = mysql_fetch_array($dd_comment);
                                    echo $ddcomments = $dd_comments[0];
                               
                                    ?>

                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>
                       
                    </table>
 </div>
                </div>
            <div class="print_style">
                <div class="container" id="tab2C">
                   <table border="1" style="border-color: #FFFFFF;font-size: 14px;" cellspacing="0" align="center" width="750">
                    
                        <div style="float:right;margin-right: 102px;margin-bottom: 7px;color: mediumBlue;">
                            <a class="noprint" onclick="javascript:window.print();">Click Here for Print</a>
                        </div>
                        
                        <div class="print_heading" style="display:none;">Rejected List Details</div>
                           <div style="clear:both;"></div>
                        <tr>																				 
                            <th style="color: #FF0000">Sl. No</th>
                            <th style="color: #FF0000">Application No:</th>
                            <th style="color: #FF0000">Application Id:</th>
                            <th style="color: #FF0000; width:150px;">Organization Name</th>
                            <th style="color: #FF0000; width:150px;">DD Memo</th>
                        </tr>
                          <?php
                        $district_name = mysql_query("select district from wp_user_master where username='$myusername'");
                        $districts_name = mysql_fetch_array($district_name);
                        $districts_names = $districts_name[0];
                        $get_contact_detail = mysql_query("select * from wp_master where namehere='$districts_names' and status=3");
                        $j = 1;
                        while ($result = mysql_fetch_array($get_contact_detail)) {
                            //  print_r($results);
                            $appl_ids=$result['application_id'];
                            ?>
                            <tr>
                                <td  style="text-align:center;width:10px;"><?= $j ?></td>
                                <td  style="text-align:center;width:20px;"><?= $result['application_id'] ?></td>
                                <td  style="text-align:center;width:20px;"><?= $result['id_no'] ?></td>
                                <td style="width:50px;"><?= $result['society_address'] ?></td>
                                <td>
                                    <?php
                                    $dd_comment_memo = mysql_query("select comments from wp_dd_comments where district='$districts_names' and master_application_no=$appl_ids");
                                    $dd_comment_memos = mysql_fetch_array($dd_comment_memo);
                                    echo $ddcommentmemos = $dd_comment_memos[0];
                               
                                    ?>

                                </td>
                            </tr>
                            <?php
                            $j++;
                        }
                        ?>
                        
                    </table>
</div>
            </div>
            <div id="footerouter">
                <div id="footer">© All Rights Reserved. EDP-cell, General Education Department, Govt of Kerala. </div>
            </div>
        </div>
    </body>
</html>
<script>
    $(document).ready(function() {

        $('#tabs li a:not(:first)').addClass('inactive');
        $('.container').hide();
        $('.container:first').show();

        $('#tabs li a').click(function() {
            var t = $(this).attr('id');
            if ($(this).hasClass('inactive')) { //this is the start of our condition 
                $('#tabs li a').addClass('inactive');
                $(this).removeClass('inactive');

                $('.container').hide();
                $('#' + t + 'C').fadeIn('slow');
            }
        });

    });

</script>
<style>
    #tabs {

        width: 100%;
        height:30px; 
        border-bottom: solid 1px #CCC;
        padding-right: 2px;
        margin-top: 30px;

    }
    a {cursor:pointer;}

    #tabs li {
        float:left; 
        list-style:none; 
        border-top:1px solid #ccc; 
        border-left:1px solid #ccc; 
        border-right:1px solid #ccc; 
        margin-right:5px; 
        border-top-left-radius:3px;  
        border-top-right-radius:3px;
        outline:none;
    }

    #tabs li a {

        font-family:Arial, Helvetica, sans-serif; 
        font-size: small;
        font-weight: bold; 
        color: #5685bc;;
        padding-top: 5px;
        padding-left: 7px;
        padding-right: 7px;
        padding-bottom: 8px; 
        display:block; 
        background: #FFF;
        border-top-left-radius:3px; 
        border-top-right-radius:3px; 
        text-decoration:none;
        outline:none;

    }

    #tabs li a.inactive{
        padding-top:5px;
        padding-bottom:8px;
        padding-left: 8px;
        padding-right: 8px;
        color:#666666;
        background: #EEE;
        outline:none;
        border-bottom: solid 1px #CCC;

    }

    #tabs li a:hover, #tabs li a.inactive:hover {


        color: #5685bc;
        outline:none;
    }

    .container {

        clear:both;           
/*        width:100%; */
/*        border-bottom: solid 1px #CCC;*/
        padding-right: 10px;
/*        padding-top: 10px;*/
        padding-bottom: 25px;
/*        padding-left: 0px;*/

    }

/*    .container h2 { margin-left: 15px;  margin-right: 15px;  margin-bottom: 10px; color: #5685bc; }*/

/*    .container p { margin-left: 15px; margin-right: 15px;  margin-top: 10px; margin-bottom: 10px; line-height: 1.3; font-size: small; }

    .container ul { margin-left: 25px; font-size: small; line-height: 1.4; list-style-type: disc; }

    .container li { padding-bottom: 5px; margin-left: 5px;}*/

</style>
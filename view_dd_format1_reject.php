<?php
include 'library/dbconnect.php';
//include_once("submit.inc.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link rel="stylesheet" type="text/css" href="style.css" />
        <script src="JSCal2/js/jscal2.js"></script>
        <script src="JSCal2/js/lang/en.js"></script>
        <link rel="stylesheet" type="text/css" href="JSCal2/css/jscal2.css" />
        <link rel="stylesheet" type="text/css" href="JSCal2/css/border-radius.css" />
        <link rel="stylesheet" type="text/css" href="JSCal2/css/steel/steel.css" />

        <script src="JSCal2/js/gen_validatorv31.js" type="text/javascript"></script>
    </head>
    <body>
        <div id="container">
            <div id="header">
                <div id="header_title">CENTRAL SPONSORED SCHEME FOR PROVIDING QUALITY EDUCATION IN MADRASA (SPQEM)</div>


            </div>
            <div id="menubar"><ul>
                    <li><a href="DD.php">Home</a></li>                    
                     <li><a href="ddrejectlist.php">Rejected List By DD</a></li>
                     <li><a href="logout.php">Logout</a></li>

                </ul></div>
            <div id="body">

                <div id="inner" id="inner">
                    <div id="middle1" style="padding:20px;">

                        <div><font color="RED"><?php //echo $msg;  ?></font></div>

                        <form action="" method="post" name="test" style="border: 1px solid black;padding-left: 10px;padding-right: 10px;width: 935px;">    
                          
                            <?php
                            if(isset($_GET['id'])){	
                               $id = $_GET['id'];
                              // $district=mysql_query("select master_application_id from wp_format1 where master_application_id='$id'");
                              // $application_id=mysql_fetch_array($district);
                              //echo $applicationid=$application_id[0];
                           //  echo "select * from wp_format1 where master_application_id='$id' and status=1";
          $contact_exist=mysql_query("select * from wp_format1 where master_application_id='$id' and status=3");
                  // echo "select * from wp_master where application_id='$serach'";
                   	if(mysql_num_rows($contact_exist)!=0){
                            $result=mysql_fetch_row($contact_exist);
                            $seracch= $result[1];?>
                   
                   <input type="hidden" name="application_no" id="name" value="<?=$result[1] ?>" />
                   <input type="hidden" name="id_no" id="name" value="<?=$result[2] ?>" />
                   <div class="main_heading">
                        <div class="application">
                            <?php                     
                                echo "<b>FORMAT-I<br/>(Physical & Financial)</b>";
                            ?>
                        </div><!--application-->
                        <div class="part">
                            <?php                     
                                echo "<b>Fund Requirements for Teacher Salary & Teacher Training</b>";
                            ?>
                        </div><!--part--> 
                    </div><!--main_heading-->  

                 <div style="clear:both;"></div>
                       <div id="print_style">
                 <div class="main_table">
                            <div class="sub_table1">
                                1. Total Nos of teachers in position in Madrassa :
                                <br/> (For Modern Subjects) 
                            </div><!-- sub_table1 -->
                            <div class="sub_table2">
                            <?php echo $result[3];                            
                            ?>                            
                            </div><!-- sub_table2 -->
                       </div><!-- main_table -->
                       
                       <div style="clear:both;"></div>               
                  
                       <div class="main_table">
                            <div class="sub_table1">
                                2. Total No of children Madrassa :
                                <br/> (For Modern Subjects) 
                            </div><!-- sub_table1 -->
                            <div class="sub_table2">
                            <?php echo $result[4]; ?>
                            </div><!-- sub_table2 -->
                       </div><!-- main_table -->
                       
                       <div style="clear:both;"></div>               
               
                        <div class="main_table">
                            <div class="sub_table1">
                                3. Teacher pupil ratio :
                            </div><!-- sub_table1 -->
                            <div class="sub_table2">                           
                              Primary/Upper : <?=$result[5]?><br/>
                              Secondary/Sr.sec : <?=$result[6]?>
                            </div><!-- sub_table2 -->
                       </div><!-- main_table -->
                       
                       <div style="clear:both;"></div>
                          <div class="main_table">
                            <div class="sub_table1">
                                4. Total No of teachers proposed under SPQEM for the year (cannot exceed 3 per Madrassa) :
                            </div><!-- sub_table1 -->
                            <div class="sub_table2">                           
                             <?php echo $result[3];?>
                            </div><!-- sub_table2 -->
                       </div><!-- main_table -->
                       
                       <div style="clear:both;"></div>
                         <div class="main_table">
                            <div class="sub_table1">
                                5. How many teachers proposed in column 4 are existing & how many to be newly recruited :
                            </div><!-- sub_table1 -->
                            <div class="sub_table2">                           
                             Existing : <?php echo $result[7];?>
                             <br/>
                             To be recruited : <?=$result[8];?><br/>
                             Total :<?=$result[9];?>                            
                            </div><!-- sub_table2 -->
                       </div><!-- main_table -->
                       
                       <div style="clear:both;"></div>
                       
                       <div class="main_table">
                            <div class="sub_table1">
                                6. No of teachers proposed in column 4 to be deployed by level of education £ :
                            </div><!-- sub_table1 -->
                            <div class="sub_table2">                           
                                 For primary level :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                 <?=$result[10];?><br/>
                                 For upper primary level : 
                                 <?=$result[11];?><br/>
                                 For secondary level :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                  <?=$result[12];?><br/>
                                 For DSr.secondary level : 
                                <?=$result[13];?>
                            </div><!-- sub_table2 -->
                       </div><!-- main_table -->
                       
                       <div style="clear:both;"></div>
                       
                        <div class="main_table">
                            <div class="sub_table1">
                                7. No of teachers proposed in column 4 by subject £ £ :
                            </div><!-- sub_table1 -->
                            <div class="sub_table2">                           
                                 Sc : <?=$result[14];?><br/>
                                 Maths : <?=$result[15];?><br/>
                                 Lang : <?=$result[16];?><br/>
                                 Soc. Study : <?=$result[17];?><br/>
                                 Computer Edn : <?=$result[18];?>
                            </div><!-- sub_table2 -->
                       </div><!-- main_table -->
                       
                       <div style="clear:both;"></div>
                       
                        <div class="main_table">
                            <div class="sub_table1">
                                8. Total Amt. required (not more than 3 teachers per Madrasa) :
                            </div><!-- sub_table1 -->
                            <div class="sub_table2">  
                                 <b>Graduate Teacher</b><br/>                         
                                 No of teachers : <?php echo $result[19];?>
                                 <br/>
                                 Unit Cost : <?php echo "60000";?><br/>
                                 Total : <?=$result[20];?><br/>
                                 <b>Post Graduate B.Ed</b><br/>                         
                                 No of teachers : <?php echo $result[21];?>
                                 <br/>
                                 Unit Cost :<?php  echo "120000";?><br/>
                                 Total : <?=$result[22];?><br/>
                                 Grand Total : <?=$result[23];?>
                            </div><!-- sub_table2 -->
                       </div><!-- main_table -->
                       
                       <div style="clear:both;"></div>
                        <div class="main_table">
                            <div class="sub_table1">
                                9. No of teachers to be provided annual trg. in modern subjects, by level :
                            </div><!-- sub_table1 -->
                            <div class="sub_table2">  
                                  <?php 
                            $qty4=$result[24];
                            $news = explode(",",$qty4);
                            implode(" ",$news);
                            $ee= $news[0];
                            $se= $news[1];
                            $see= $news[2];
                            $sse= $news[3];
                            $lee= $news[4];
                            $lse= $news[5];
                            $mee= $news[6];
                            $mse= $news[7];
                            ?>                 
                                 <b>Sc</b><br/>                         
                                 EE* : <?=$ee?>
                                 SE* :<?=$se?><br/>
                                 <b>Soc .Sc</b><br/>                         
                                 EE* : <?=$see?>
                                 SE* : <?=$sse?><br/>
                                 <b>Lang</b><br/>                         
                                 EE* : <?=$lee?>
                                 SE* : <?=$lse?><br/>
                                 <b>Maths</b><br/>                         
                                 EE* :<?=$mee?>
                                 SE* : <?=$mse?><br/>
                                 Total : <?=$result[25]?>
                            </div><!-- sub_table2 -->
                       </div><!-- main_table -->
                       
                       <div style="clear:both;"></div>  
                       
                         <div class="main_table">
                            <div class="sub_table1">
                                10. Agency/s through which trg. will be imparted :
                            </div><!-- sub_table1 -->
                            <div class="sub_table2">                           
                            <?=$result[26];?>
                            </div><!-- sub_table2 -->
                       </div><!-- main_table -->
                       
                       <div style="clear:both;"></div> 
                       
                        <div class="main_table">
                            <div class="sub_table1">
                                11. Total Amt required :
                            </div><!-- sub_table1 -->
                            <div class="sub_table2">                           
                            Unit Cost : <?=$result[27];?><br/>
                            Total : <?=$result[28];?>
                            </div><!-- sub_table2 -->
                       </div><!-- main_table -->
                       
                       <div style="clear:both;"></div>
               </div><!--print_style-->
                   <div class="noprint">
                                        <div>
                                            <a href="view_dd_reject_format2.php?id=<?php echo $result[1];?>" style="float: right;margin-top: 20px;">View Next Form</a>
                                        </div>       
                                        <!--
                                        
                                                           <div> <input type="submit" name="submit_cancel" value="Cancel" class="cancel_button"></div>
                                        -->
                                    </div>
                 <?php }
}
?>

                        </form> 
                    </div>
                </div>
            </div>
            <div id="footerouter">
                <div id="footer">© All Rights Reserved. IT@School, General Education Department, Govt of Kerala. </div>
            </div>
        </div>	
    </body>
</html>
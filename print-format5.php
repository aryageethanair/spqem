<?php
include 'library/dbconnect.php';
//include_once("submit.inc.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link rel="stylesheet" type="text/css" href="style.css" />
        <script src="JSCal2/js/jscal2.js"></script>
        <script src="JSCal2/js/lang/en.js"></script>
        <link rel="stylesheet" type="text/css" href="JSCal2/css/jscal2.css" />
        <link rel="stylesheet" type="text/css" href="JSCal2/css/border-radius.css" />
        <link rel="stylesheet" type="text/css" href="JSCal2/css/steel/steel.css" />

        <script src="JSCal2/js/gen_validatorv31.js" type="text/javascript"></script>
    </head>
    <body>
        <style type="text/css">

@media print
{
#header {display:none;}
.main_table0{display:none;}
.noprint{display:none;}
#noprint_pg{display:none;}
.breadcrumb-list{display:none;}
#footer{display:none;}
#menubar{display:none;}
#print_style{font-size:12px;padding:20px;margin-right: 150px !important;}
.main_heading{text-align: center;}
.application{text-align: center;}
.part{text-align: center;}
.print_new{border:none !important;}
.main_heading{margin-right: 170px !important;}
/*.sub_table18{margin-right: 250px  !important;}*/
}
</style>

        <div id="container">
            <div id="header">
                <div id="header_title">CENTRAL SPONSORED SCHEME FOR PROVIDING QUALITY EDUCATION IN MADRASA (SPQEM)</div>


            </div>
            <div id="menubar"><ul>
                    <li><a href="index.php">Home</a></li>
                    <li><a href="input.php">New Application</a></li>
                    <li><a href="edit.php">Edit Application</a></li>
                    <li><a href="print.php">Print Application</a></li>
                    <li><a href="status.php">View Application Status</a></li>
                    <li><a href="login.php">Login</a></li>

                </ul></div>
            <div id="body">

                <div id="inner" id="inner">
                    <div id="middle1" style="padding:20px;">

                        <div><font color="RED"><?php //echo $msg;  ?></font></div>

                    <form action="" method="post" name="test">    
                     
                <?php 
                 if(isset($_GET['id']) || isset($_POST['submit_form_five'])){ 
                  $serach=$_REQUEST['id'];
                  $file_no=$_REQUEST['fileno'];
                      $contact_exist = mysql_query("select * from wp_format5 where mater_application_id='$serach' and master_id_no='$file_no' and status=1");
                  // echo "select * from wp_master where application_id='$serach'";
                   	if(mysql_num_rows($contact_exist)!=0){
                            $result=mysql_fetch_row($contact_exist);
                            //print_r($result);
                            $seracch= $result[1];
                        
?>
                   
                   <input type="hidden" name="application_no" id="name" value="<?=$result[1] ?>" />
                   <input type="hidden" name="id_no" id="name" value="<?=$result[2] ?>" />
                   <div style="border: 1px solid black;padding: 10px;width: 910px;margin-top: 14px;" class="print_new">
                  <div class="main_heading">
                        <div class="application">
                            <?php                     
                                echo "<b>FORMAT-V - NA</b>";
                            ?>
                        </div><!--application-->
                        <div class="part">
                            <?php                     
                                echo "<b>(To be filled in the State Govt. after approval in GIAC to central Govt. for assistance)<br/>Details for assistance under the Madrasa Modernization Programme</b>";
                            ?>
                        </div><!--part--> 
                    </div><!--main_heading-->   

                 <div style="clear:both;"></div>
                 <div id="print_style">
   <div class="main_table">
                      <div class="sub_table1">
                            1. District/Tehsil :
                        </div><!-- sub_table1 -->
                      <div class="sub_table2">
                          <?=$result[3] ?>
                        </div><!-- sub_table2 -->
                    </div><!-- main_table -->  
                    
                    <div style="clear:both;"></div>
                    
                    <div class="main_table">
                      <div class="sub_table1">
                            2. Name & addrs of madrasa Level of madrasa eg: primary,upper primarty,secondary,senior secondary be stated :
                        </div><!-- sub_table1 -->
                      <div class="sub_table2">
                          <?=$result[4] ?>
                        </div><!-- sub_table2 -->
                    </div><!-- main_table -->  
                    
                    <div style="clear:both;"></div>
                    
                    <div class="main_table">
                      <div class="sub_table1">
                            3. Date of establishment and registration with Madrasa Board/Waqkf Board/NIOS :
                        </div><!-- sub_table1 -->
                      <div class="sub_table2">
                             Reg Date :<?php echo $result[5];?><br/>
                          Reg No :<?php echo $result[6]; ?>
                        </div><!-- sub_table2 -->
                    </div><!-- main_table -->  
                    
                    <div style="clear:both;"></div>
                    
                    <div class="main_table">
                      <div class="sub_table1">
                            4. Details of assistance recieved by central/state schemes in the past :
                        </div><!-- sub_table1 -->
                      <div class="sub_table2">
                          <?=$result[7] ?>
                        </div><!-- sub_table2 -->
                    </div><!-- main_table -->  
                    
                    <div style="clear:both;"></div>
                    
                      <div class="main_table">
                      <div class="sub_table1">
                            5. Total number of students in the madrasa :
                        </div><!-- sub_table1 -->
                      <div class="sub_table2">
                          <?=$result[8] ?>
                        </div><!-- sub_table2 -->
                    </div><!-- main_table -->  
                    
                    <div style="clear:both;"></div>
                    
                     <div class="main_table">
                      <div class="sub_table1">
                            6. If accredited with NIOS number of students appeared for certification of classes 3,5,8,10 & 12 :
                        </div><!-- sub_table1 -->
                      <div class="sub_table2">
                          <?=$result[9] ?>
                        </div><!-- sub_table2 -->
                    </div><!-- main_table -->  
                    
                    <div style="clear:both;"></div>
                    
                    <div class="main_table">
                      <div class="sub_table1">
                            7. Total number of teachers for whom assistance is proposed :
                        </div><!-- sub_table1 -->
                      <div class="sub_table2">
                          <?=$result[10] ?>
                        </div><!-- sub_table2 -->
                    </div><!-- main_table -->  
                    
                    <div style="clear:both;"></div>
                    
                    <div class="main_table">
                      <div class="sub_table1">
                            8. Details of equipment and teaching learning materials required for Science/Computer labs* and Science kit/Math kit* :
                        </div><!-- sub_table1 -->
                      <div class="sub_table2">
                         <?=$result[11] ?>
                        </div><!-- sub_table2 -->
                    </div><!-- main_table -->  
                    
                    <div style="clear:both;"></div>
                    
                    <div class="main_table">
                      <div class="sub_table1">
                            9. Details of text Books/books/Library required for students:
                        </div><!-- sub_table1 -->
                      <div class="sub_table2">
                          <?=$result[12] ?>
                        </div><!-- sub_table2 -->
                    </div><!-- main_table -->  
                    
                    <div style="clear:both;"></div>
                    
                     <div class="main_table">
                      <div class="sub_table1">
                            10. Accreditation with NIOS is required, if not accredited :
                        </div><!-- sub_table1 -->
                      <div class="sub_table2">
                          <?=$result[13] ?>
                        </div><!-- sub_table2 -->
                    </div><!-- main_table -->  
                    
                    <div style="clear:both;"></div>
                    
                     <div class="main_table">
                      <div class="sub_table1">
                            11. Remarks :
                        </div><!-- sub_table1 -->
                      <div class="sub_table2">
                         <?=$result[14] ?>
                        </div><!-- sub_table2 -->
                    </div><!-- main_table -->  
                       <div style="clear:both;"></div>
                     <div class="main_table"> 
                        <div class="sub_table18">
                        (Signature of the Member Secretary of State GIAC)
                        </div><!-- sub_table2 -->
                    </div><!-- main_table -->
                 
                       <div style="clear:both;"></div>
               </div><!--print_style-->
               </div><!--print_style-->
                   <div class="noprint">
                    <div  style="float:right;margin-right: 77px;">
                         <a href="print.php" onclick="javascript:window.print();">Print</a>
                        <!--<input type="submit" name="submit" value="Print" onclick="javascript:window.print();">-->
                     </div>       

                   </div>
<?php }else{
if($serach!=$seracch){
                            echo  "<div style='color:red;font-weight:bold;padding-top:10px;text-align:center;'>Application not found!</div>";}
}
                      }
                
                ?>                
                            </form> 
                    </div>
                </div>
            </div>
            <div id="footerouter">
                <div id="footer">© All Rights Reserved. IT@School, General Education Department, Govt of Kerala. </div>
            </div>
        </div>
    </body>
</html>
<?php
session_start();
if(!($_SESSION['type']=='DD' && $_SESSION['username'])){
    echo "<script>window.location='login.php'</script>";    
}
include 'library/dbconnect.php';
//include_once("submit.inc.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link rel="stylesheet" type="text/css" href="style.css" />
        <script src="JSCal2/js/jscal2.js"></script>
        <script src="JSCal2/js/lang/en.js"></script>
        <link rel="stylesheet" type="text/css" href="JSCal2/css/jscal2.css" />
        <link rel="stylesheet" type="text/css" href="JSCal2/css/border-radius.css" />
        <link rel="stylesheet" type="text/css" href="JSCal2/css/steel/steel.css" />

        <script src="JSCal2/js/gen_validatorv31.js" type="text/javascript"></script>
    </head>
    <body>
        <div id="container">
            <div id="header">
                <div id="header_title">CENTRAL SPONSORED SCHEME FOR PROVIDING QUALITY EDUCATION IN MADRASA (SPQEM)</div>


            </div>
            <div id="menubar"><ul>
                    <li><a href="DD.php">Home</a></li>
                    <li><a href="logout.php">Logout</a></li>

                </ul></div>
            <div id="body">

                <div id="inner" id="inner">
                    <div id="middle1" style="padding:20px;">

                        <div><font color="RED"><?php //echo $msg;    ?></font></div>

                        <form action="" method="post" name="test" style="border: 1px solid black;padding-left: 10px;padding-right: 10px;width: 935px;">    

                            <?php
                            if (isset($_GET['id'])) {
                            $id = $_REQUEST['id'];
                              $status=$_GET['status'];
                              $username=$_GET['username'];
                              //echo "select application_id from wp_master where id_no='$id'";
                                $district = mysql_query("select id_no from wp_master where application_id='$id'");
                             //   echo "select application_id from wp_master where application_id='$id'";
                                $application_id = mysql_fetch_array($district);
                                $id_no = $application_id[0];
                                $contact_exist = mysql_query("select * from wp_master where application_id='$id' and id_no=$id_no and (status=1 or status=2 or status=3)");
                               //echo "select * from wp_master where application_id='$id' and id_no=$id and status=1 or status=2 or status=3";
                                if (mysql_num_rows($contact_exist) != 0) {
                                    $result = mysql_fetch_row($contact_exist);
                                   // print_r($result);
                                  //  $seracch = $result[1];
                                    ?>

                                    <input type="hidden" name="application_no" id="name" value="<?= $result[1] ?>" />
                                    <input type="hidden" name="id_no" id="name" value="<?= $result[2] ?>" />
                                    <div class="main_heading">
                                        <div class="application">
                                            <?php
                                            echo "(To be submitted in duplicate)<br/><b>Scheme for Providing Quality Education to Madrasas <br/>APPLICATION FORM</b>";
                                            ?>
                                        </div><!--application-->
                                        <div class="part">
                                            <?php
                                            echo "<b>PART I</b><br/>(To be filled by applicant)";
                                            ?>
                                        </div><!--part--> 
                                    </div><!--main_heading-->
                                        <div id="print_style">
                                        <div class="applicationid">
                                            <div class="application_no">Application No:<?php echo $result[1]; ?></div>                          
                                        </div>
                                        <div class="main_table">
                                            <div class="sub_table1">
                                                1. <b>Name of organisation/Society</b> running the Madrasa * :
                                                <br/>(With complete address)
                                            </div><!-- sub_table1 -->
                                            <div class="sub_table2">
                                                <?= $result[4] ?>
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>

                                        <div class="main_table">
                                            <div class="sub_table1">
                                                2. <b>Name with address of Madrasa</b> seeking Financial Assistance :
                                            </div><!-- sub_table1 -->
                                            <div class="sub_table2">
                                                <?= $result[5] ?>
                                            </div><!-- sub_table2 -->                  

                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>

                                        <div class="main_table">
                                            <div class="sub_table1">
                                                (a). Email :
                                            </div><!-- sub_table1 -->
                                            <div class="sub_table2">
                                                <?= $result[7] ?>
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>

                                        <div class="main_table">
                                            <div class="sub_table1">
                                                3. <b>Objectives and activities</b> {give brief history of the organization/society running the Madras(s)} :
                                            </div><!-- sub_table1 -->
                                            <div class="sub_table2">
                                                <?= $result[8] ?>
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>

                                        <div class="main_table">
                                            <div class="sub_table1">
                                                4. <b>Whether Registered</b> under central or state WAKF Acts/State Madrasa Board or accredited center of NIOS. If yes,<b> Regn No.</b>( A copy of the registration / accrediation certificate may be attached) :
                                            </div><!-- sub_table1 -->
                                            <div class="sub_table2">
                                                <?php
                                                $value = $result[9];
                                                if ($value == 'Yes') {
                                                    ?>
                                                    <?= $result[9]; ?><br/>
                                                   <?php $madrasa= $result[10]; 
                                                    if($madrasa==1){
                                                        echo "WAKF Acts";
                                                    }
                                                     if($madrasa==2){
                                                        echo "Madrasa";
                                                    }
                                                    if($madrasa==3){
                                                        echo "NIOS";
                                                    }
                                                    if($madrasa==4){
                                                        echo "Others";
                                                    }
                                                    ?><br/>     
                                                    Reg No :<?= $result[11] ?><br/>
                                                    <div id="noprint_pg"> Reg Date : <?= $result[12] ?></div>
                                                    <?php
                                                }
                                                if ($value == 'No') {
                                                    ?>
                                                    <?= $result[9]; ?>
                                                <?php }
                                                ?>

                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>

                                        <div class="main_table">
                                            <div class="sub_table1">
                                                5. <b>Specific educational activities in modern subjects of the Madrasa seeking financial assistance under the scheme.</b>                                          
                                            </div><!-- sub_table1 -->
                                            <div class="sub_table2">
                                                <?= $result[13] ?>
                                            </div><!-- sub_table2 -->                    
                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>

                                        <div class="main_table">
                                            <div class="sub_table1">
                                                (a). Whether the Madrasa seeking financial assistance has any experience in teaching of subjects like science (Phy., chem.., Bio.), Maths , social studies (history, geography, civics etc.); Languages (State language/Hindi/English) etc.? If so, brief description may be given:
                                            </div><!-- sub_table1 -->
                                            <div class="sub_table2">
                                                <?php
                                                $values = $result[14];
                                                if ($values == 'Yes') {
                                                    ?>
                                                    <?= $result[14] ?><br/> 
                                                    <?= $result[15] ?>
                                                    <?php
                                                }
                                                if ($values == 'No') {
                                                    ?>
                                                    <?= $result[14] ?>
                                                <?php }
                                                ?>
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>

                                        <div class="main_table">
                                            <div class="sub_table1">
                                                (b). Whether State curriculum, NIOS or any other curriculum followed; please specify; :
                                            </div><!-- sub_table1 -->
                                            <div class="sub_table2">
                                                <?php $cirriculam= $result[16];
                                                if($cirriculam==1){
                                                  echo "State curriculum";
                                                }
                                                
                                                 if($cirriculam==2){
                                                  echo "NIOS curriculum";
                                                }
                                                
                                                 if($cirriculam==3){
                                                  echo "Others";echo "<br/>";
                                                  echo $result[17];
                                                }
                                                
                                                ?>

                                            </div><!-- sub_table2 -->

                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>

                                        <div class="main_table">
                                            <div class="sub_table1">
                                                (c). Number of children studying these subjects by class and by gender.If there are any children with special needs (disabled children), number and class may be mentioned :
                                            </div><!-- sub_table1 -->
                                            <div class="sub_table2">    
                                                <?php
                                                $total = $result[18];
                                                $newArray = explode(",", $total);
                                                implode(" ", $newArray);
                                                $boy = $newArray[0];
                                                $girl = $newArray[1];
                                                ?>

                                                Boys: <?= $boy ?> &
                                                Girls: <?= $girl ?>
                                                <br/>  Total :<?= $result[19]; ?>
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>


                                        <?php
                                        $count_techer1 = $result[20];
                                        $qualification_teacher = $result[21];
                                        $qualification_teacher1 = explode(",", $qualification_teacher);
                                        implode(" ", $qualification_teacher1);
                                        $qualification_teachers1 = $qualification_teacher1[0];
                                        $qualification_teachers2 = $qualification_teacher1[1];
                                        $qualification_teachers3 = $qualification_teacher1[2];
                                        if ($count_techer1 == 1) {
                                            ?>
                                            <div class="main_table">
                                                <div class="sub_table1">
                                                    (d). No. of teachers already working and teaching modern subjects. Please give details including year of recruitment; whether they are trained teachers (with pre-service qualifications as per NCTE norms); information be given disaggregated by level of teaching (primary/upper primary/secondary/ senior secondary subjects); Subject wise break up be also mentioned :
                                                </div><!-- sub_table1 -->
                                                <div class="sub_table2">
                                                    <?= $result[20] . ' &nbsp;Teacher' ?>
                                                    <br/>

                                                    <div id="elmt_cont" style="width:200px;min-height:50px;">
                                                        <div id="elm1">
                                                            <?php if($qualification_teachers1==1){
                                                                echo "PG";
                                                            }
                                                            if($qualification_teachers1==2){
                                                                echo "UG";
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>                          
                                                </div><!-- sub_table2 -->
                                            </div><!-- main_table -->
                                            <?php
                                        }

                                        if ($count_techer1 == 2) {
                                            ?>
                                            <div class="main_table">
                                                <div class="sub_table1">
                                                    (d). No. of teachers already working and teaching modern subjects. Please give details including year of recruitment; whether they are trained teachers (with pre-service qualifications as per NCTE norms); information be given disaggregated by level of teaching (primary/upper primary/secondary/ senior secondary subjects); Subject wise break up be also mentioned :
                                                </div><!-- sub_table1 -->
                                                <div class="sub_table2">
                                                    <?= $result[20] . '&nbsp;Teachers' ?><br/>
                                                    <input type="hidden" name="elmt_count" id="elmt_count" value="1">
                                                        <div id="elmt_cont" style="width:200px;min-height:50px;">
                                                            <div id="elm1">
                                                                <?php if($qualification_teachers1==1){
                                                                echo "PG";
                                                                echo "<br/>";
                                                            }
                                                            if($qualification_teachers1==2){
                                                                echo "UG";
                                                                 echo "<br/>";
                                                            }
                                                           if($qualification_teachers2==1){
                                                                echo "PG";
                                                                 echo "<br/>";
                                                            }
                                                            if($qualification_teachers2==2){
                                                                echo "UG";
                                                                 echo "<br/>";
                                                            } ?>
                                                            </div>
                                                        </div>                          
                                                </div><!-- sub_table2 -->
                                            </div><!-- main_table -->

                                            <?php
                                        }
                                        if ($count_techer1 == 3) {
                                            ?>
                                            <div class="main_table">
                                                <div class="sub_table1">
                                                    (d). No. of teachers already working and teaching modern subjects. Please give details including year of recruitment; whether they are trained teachers (with pre-service qualifications as per NCTE norms); information be given disaggregated by level of teaching (primary/upper primary/secondary/ senior secondary subjects); Subject wise break up be also mentioned :
                                                </div><!-- sub_table1 -->
                                                <div class="sub_table2">
                                                    <?= $result[20] .'&nbsp;Teachers' ?><br/>
                                                    <div id="elmt_cont" style="width:200px;min-height:50px;">
                                                        <div id="elm1">
                                                            
                                                             <?php if($qualification_teachers1==1){
                                                                echo "Post Graduation";
                                                                echo "<br/>";
                                                            }
                                                            if($qualification_teachers1==2){
                                                                echo "Graduation";
                                                                 echo "<br/>";
                                                            }
                                                           if($qualification_teachers2==1){
                                                                echo "Post Graduation";
                                                                 echo "<br/>";
                                                            }
                                                            if($qualification_teachers2==2){
                                                                echo "Graduation";
                                                                 echo "<br/>";
                                                            } 
                                                            if($qualification_teachers3==1){
                                                                echo "Post Graduation";
                                                                 echo "<br/>";
                                                            }
                                                            if($qualification_teachers3==2){
                                                                echo "Graduation";
                                                                 echo "<br/>";
                                                            } 
                                                            ?>
                                                            
                                                        </div>
                                                    </div>                          
                                                </div><!-- sub_table2 -->
                                            </div><!-- main_table -->

                                        <?php } ?>
                                        <!--
                                                             <script> $("#drop1 option[value='<?php //echo $result[20];  ?>']").attr('selected', 'selected') </script>
                                        -->
                                        <div style="clear:both;"></div>
                                        <div class="main_table" id="noprint_pg">
                                            <div class="sub_table1">
                                                (f). School Level  :
                                            </div><!-- sub_table1 -->
                                            <div class="sub_table2">
                                                <? $school_level=$result[22];
                                                if($school_level==1){
                                                    echo "Primary Level";
                                                }
                                                if($school_level==2){
                                                    echo "Upper Primary Level";
                                                }
                                                if($school_level==3){
                                                    echo "Secondary Level";
                                                }
                                                if($school_level==4){
                                                    echo "Sr.Secondary Level";
                                                }
                                                ?>
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table --> 

                                        <div style="clear:both;"></div>

                                        <div class="main_table">                        
                                            6. <b>Infrastructure details of the Madrasa</b>  
                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>

                                        <div class="main_table">
                                            <div class="sub_table1">
                                                (a). Whether the Madrasa is located in its own or rented building? Give details :
                                            </div><!-- sub_table1 -->
                                            <div class="sub_table2">   
                                                <?php
                                                $rent = $result[23];
                                                if ($rent == 'own') {
                                                    ?>
                                                    <?= $result[23]; ?><br/> 
                                                    <?= $result[24]; ?>
                                                    <?php
                                                }
                                                if ($rent == 'rent') {
                                                    ?>
                                                    <?= $result[23]; ?><br/>
                                                    <?= $result[24]; ?>
                                                <?php }
                                                ?>

                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table --> 

                                        <div style="clear:both;"></div>

                                        <div class="main_table">
                                            <div class="sub_table1">
                                                (b). No of rooms available for teaching & administrative purposes :
                                            </div><!-- sub_table1 -->
                                            <div class="sub_table2">                        
                                                <?= $result[25]; ?>
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->  

                                        <div style="clear:both;"></div>

                                        <div class="main_table">
                                            <div class="sub_table1">
                                                (c). If the present accommodation sufficient for the teaching of traditional as well as modern subjects ? Give details :
                                            </div><!-- sub_table1 -->
                                            <div class="sub_table2">

                                                <?php
                                                $accommodation = $result[26];
                                                if ($accommodation == 'No') {
                                                    ?>
                                                    <?= $result[26]; ?>

                                                    <?php
                                                }
                                                if ($accommodation == 'Yes') {
                                                    ?>
                                                    <?= $result[26]; ?><br/>                                
                                                    <?= $result[27] ?>
                                                <?php }
                                                ?>                            

                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>

                                        <div class="main_table">
                                            <div class="sub_table1">
                                                (d). Whether the Madarssa has a seperate room(s) for Science Laboratories & Computer education labs etc. [Applicable only for Secondary/ Sr.secondary level Madarassas(s)] Give details :
                                            </div><!-- sub_table1 -->
                                            <div class="sub_table2">
                                                <?php
                                                $seperate_room = $result[28];
                                                if ($seperate_room == 'No') {
                                                    ?>
                                                    <?= $result[28]; ?><br/> 
                                                    <?= $result[29]; ?>
                                                    <?php
                                                }
                                                if ($seperate_room == 'Yes') {
                                                    ?>
                                                    <?= $result[28]; ?>
                                                <?php }
                                                ?>         

                                            </div><!--completed value--><!-- sub_table2 -->
                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>


                                        <div class="main_table">                        
                                            7. <b>Accreditation with NIOS</b>  
                                        </div><!-- main_table -->

                                        <div class="main_table">                        
                                            <div class="sub_table1">
                              (a). If already accrediated by NIOS give details of 
                         </div><!-- sub_table1 -->
                         
                                        
                                        <div class="sub_table2">
                                                <?php
                                               $radio6 = $result[30];
                                                if ($radio6 == 'No') {
                                                    ?>
                                                    <?= $radio6;
                                                }
                                                if ($radio6 == 'Yes') {
                                                    ?>
                                                    <?= $radio6; ?>
                                            </div><!-- sub_table2 -->
                                            </div><!-- main_table -->
                                            <div id="ifYes">       
                                                                                            <div class="main_table">
                                                                                                <div class="sub_table1">
                                                                                                    (i). Number of students registered with NIOS :                            
                                                                                                </div><!-- sub_table1 -->
                                                                                                <div class="sub_table2">
                                                                                                    <?=$result[31]?>
                                                                                                </div><!-- sub_table2 -->
                                                                                            </div><!-- main_table -->

                                                                                            <div style="clear:both;"></div>

                                                                                            <div class="main_table">
                                                                                                <div class="sub_table1">
                                                                                                    (ii). Number of students who have obtained certification from NIOS for class 3,5,8,10 and 12 sepeartely for each of the year of accrediation :

                                                                                                </div><!-- sub_table1 -->
                                                                                                <div class="sub_table2">
                                                                                                    Class 3 :&nbsp;&nbsp;<br/>
                                                                                                    Number of students:<?=$result[32]?><br/>  
                                                                                                           Year Of Passing : <?=$result[33]?>
                                                                                                </div><!-- sub_table2 -->
                                                                                                <div style="clear:both;"></div>                         

                                                                                                <div class="sub_table2">
                                                                                                    Class 5 :&nbsp;&nbsp;<br/>
                                                                                                    Number of students:<?=$result[34]?><br/>  
                                                                                                           Year Of Passing : <?=$result[35]?>
                                                                                                </div><!-- sub_table2 -->
                                                                                                <div style="clear:both;"></div>

                                                                                                <div class="sub_table2">
                                                                                                    Class 8 :&nbsp;&nbsp;<br/>
                                                                                                    Number of students:<?=$result[36]?><br/>  
                                                                                                       Year Of Passing : <?=$result[37]?>
                                                                                                </div><!-- sub_table2 -->
                                                                                                <div style="clear:both;"></div>                        

                                                                                                <div class="sub_table2">
                                                                                                    Class 10 :<br/>
                                                                                                    Number of students:<?=$result[38]?><br/>                                                                                                   
                                                                                                       Year Of Passing : <?=$result[39]?>
                                                                                                </div><!-- sub_table2 -->
                                                                                                <div style="clear:both;"></div>                        

                                                                                                <div class="sub_table2">
                                                                                                    Class 12 :<br/>
                                                                                                    Number of students:<?=$result[40]?><br/>
                                                                                                   Year Of Passing : <?=$result[41]?>
                                                                                                </div><!-- sub_table2 -->
                                                                                            </div><!-- main_table -->

                                                                                            <div style="clear:both;"></div>

                                                                                            <div class="main_table">
                                                                                                <div class="sub_table1">
                                                                                                    (b). If not yet accrediated, whether the Madrassa seeking financial assistance is interested in NIOS accrediation ? If so, whether applied to NIOS (refernce number of application be given) and by when NIOS accrediation is expected for (a) academic stream (class 3,5,8,10 & 12) and /or (b) vocational stream (Secondary & Sr. seconadry)) :

                                                                                                </div><!-- sub_table1 -->
                                                                                                <div class="sub_table2">
                                                                                                    <?=$result[42]?>
                                                                                                </div><!-- sub_table2 -->
                                                                                            </div><!-- main_table -->
                                                                                        </div>
                                            
                                                <?php }
                                                ?>  
                                            
                                        <div style="clear:both;"></div>

                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>


                                        <div class="main_table"> 
                                            <div class="sub_table1">                       
                                                8. <b>Details of proposal for financial assistance :</b>  
                                            </div>
                                            <div class="sub_table2">
                                                <?= $result[43] ?>
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>

                                        <div class="main_table">
                                            <div class="sub_table1">
                                                (i). No of addtional teachers and amount required for teaching modern subjects as well as provision for their training :
                                            </div><!-- sub_table1 -->
                                            <div class="sub_table2">
                                                <?= $result[44] ?>
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>

                                        <div class="main_table"> 
                                            <div class="sub_table1">                       
                                                <b>Requirements be given in Format I enclosed :</b>  
                                            </div>
                                            <div class="sub_table2">
                                                <?= $result[45] ?>
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>

                                        <div class="main_table"> 
                                            <div class="sub_table1">                       
                                                (These appointments may be on contract basis. This scheme doesnot provide for a cadre or regular appointment. These are purely on the short term basis.) :  
                                            </div>
                                            <div class="sub_table10">
                                                <?//= $result[46] ?>
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>

                                       <div class="main_table">
                                                                                            <div class="sub_table1">
                                                                                                (ii). Number and amount required for Libraries/Book banks/Text books/Science labs/Computer labs/Science & Maths Kits tc. for teaching modern subjects :
                                                                                            </div><!-- sub_table1 -->
                                                                                            <div class="sub_table2">                          
                                                                                                Computer rate :<?= $result[46] ?>
                                                                                            </div>
                                                                                            <div style="padding-bottom: 25px;padding-top:20px"></div>
                                                                                            <div class="sub_table2">
                                                                                                For Training :&nbsp;&nbsp;&nbsp; <?= $result[47] ?>                       
                                                                                            </div>
                                                                                            <div style="padding-bottom: 25px;padding-top:20px"></div>
                                                                                            <div class="sub_table2">
                                                                                                Annual Grand : <?= $result[48] ?>
                                                                                            </div>
                                                                                            <div style="padding-bottom: 25px;padding-top:20px"></div>        
                                                                                            <div class="sub_table2">
                                                                                                Library :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                                <?= $result[49] ?>                        
                                                                                            </div>                      
                                                                                            <div style="padding-bottom: 25px;padding-top:20px"></div>
                                                                                            <div class="sub_table2">
                                                                                                Total : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                                <?= $result[50] ?>
                                                                                            </div><!-- sub_table2 -->
                                                                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>

                                        <div class="main_table"> 
                                            <div class="sub_table1">                       
                                                <b>Requirements be given in Format II enclosed :</b> <br/>
                                                (Laboratories/Science labs/Computer labs are for secondary & senior secondary level only) 
                                            </div>
                                            <div class="sub_table2">
                                                <?= $result[51] ?>
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>

                                        <div class="main_table"> 
                                            <div class="sub_table1">                       
                                                (iii) For library books has a selction criteria been developed and a purchase commitee been set up by the madrassa ? :
                                            </div>
                                            <div class="sub_table2">
                                                <?php
                                                
                                                $library = $result[52] ;
                                                if ($library  == 'Yes') {
                                                    ?>
                                                    <?= $result[52]; ?><br/> 
                                                    <?= $result[53]; ?>
                                                    <?php
                                                }
                                                if ($library  == 'No') {
                                                    ?>
                                                    <?= $result[52]; ?>
                                                <?php }
                                                ?>     
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>

                                        <div class="main_table"> 
                                            <div class="sub_table1">                       
                                                (iv) Amount required by Madrassas opting for NIOS accrediation for academic stream in modern subjects :
                                            </div>
                                            <div class="sub_table2">
                                                <?= $result[54] ?>
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>

                                        <div class="main_table">                                              
                                            <b>Requirements be given in Format III enclosed :</b> 
                                        </div><!-- main_table -->

                                        <div class="main_table"> 
                                            <div class="sub_table1">                       
                                                (v) Amount required by Madrassas opting for NIOS accrediation for vocational stream :
                                            </div>
                                            <div class="sub_table2">
                                                <?= $result[55] ?>
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>

                                        <div class="main_table"> 
                                            <div class="sub_table1">                       
                                                10. Total Amount required :
                                            </div>
                                            <div class="sub_table2">
                                                
                                                For salary : <?=$result[56] ?> <br/>
                                                Others : <?= $result[57] ?>  <br/>                
                                                Total : <?= $result[58]; ?><br/>
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>

                                        <div class="main_table"> 
                                            <div class="sub_table1">                       
                                                (11) Whether the Madrasa is getting any financial assistance for teaching of modern subjects from an y other source. If so, the amount and the purpose for which it is intended, be mentioned. (No duplication should be done) :
                                            </div>
                                            <div class="sub_table2">
                                                <?= $result[59]; ?>
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>

                                        <div class="main_table"> 
                                            <div class="sub_table1">                       
                                                (12) Net amount requested from Government (10-11) :
                                            </div>
                                            <div class="sub_table2">
                                                Net Amount :  <?= $result[60];
                                                $word=$result[60];?>
                                                <br/>
                                                In Words : <? echo convert_number_to_words($word).'&nbsp;Only.'; ?>
                                            </div><!-- sub_table2 -->
                                        </div><!-- main_table -->

                                        <div style="clear:both;"></div>
 
                                    </div> <!--print_style-->
                                    <div class="noprint">
                                        <div>
                                            <a href="viewformat1.php?id=<?php echo $result[1]; ?>&status=<?php echo $status;?>&username=<?php echo $username?>" style="float: right;margin-top: 20px;">View Next Form</a>
                                        </div>  
                                    </div>
                                    <?php
                                }
                            }
                            ?>

                        </form> 
                    </div>
                </div>
            </div>
            <div id="footerouter">
                <div id="footer">© All Rights Reserved. IT@School, General Education Department, Govt of Kerala. </div>
            </div>
        </div>	
    </body>
</html>
<?php 
   /*
* Function for converting number to words
*/
function convert_number_to_words($number) {
   
    $hyphen      = '-';
    $conjunction = ' and ';
    $separator   = ', ';
    $negative    = 'negative ';
    $decimal     = ' point ';
    $dictionary  = array(
        0                   => 'Zero',
        1                   => 'One',
        2                   => 'Two',
        3                   => 'Three',
        4                   => 'Four',
        5                   => 'Five',
        6                   => 'Six',
        7                   => 'Seven',
        8                   => 'Eight',
        9                   => 'Nine',
        10                  => 'Ten',
        11                  => 'Eleven',
        12                  => 'Twelve',
        13                  => 'Thirteen',
        14                  => 'Fourteen',
        15                  => 'Fifteen',
        16                  => 'Sixteen',
        17                  => 'Seventeen',
        18                  => 'Eighteen',
        19                  => 'Nineteen',
        20                  => 'Twenty',
        30                  => 'Thirty',
        40                  => 'Fourty',
        50                  => 'Fifty',
        60                  => 'Sixty',
        70                  => 'Seventy',
        80                  => 'Eighty',
        90                  => 'Ninety',
        100                 => 'Hundred',
        1000                => 'Thousand',
        1000000             => 'Million',
        1000000000          => 'Billion',
        1000000000000       => 'Trillion',
        1000000000000000    => 'Quadrillion',
        1000000000000000000 => 'Quintillion'
    );
   
    if (!is_numeric($number)) {
        return false;
    }
   
    if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
        // overflow
        trigger_error(
            'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
            E_USER_WARNING
        );
        return false;
    }

    if ($number < 0) {
        return $negative . convert_number_to_words(abs($number));
    }
   
    $string = $fraction = null;
   
    if (strpos($number, '.') !== false) {
        list($number, $fraction) = explode('.', $number);
    }
   
    switch (true) {
        case $number < 21:
            $string = $dictionary[$number];
            break;
        case $number < 100:
            $tens   = ((int) ($number / 10)) * 10;
            $units  = $number % 10;
            $string = $dictionary[$tens];
            if ($units) {
                $string .= $hyphen . $dictionary[$units];
            }
            break;
        case $number < 1000:
            $hundreds  = $number / 100;
            $remainder = $number % 100;
            $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
            if ($remainder) {
                $string .= $conjunction . convert_number_to_words($remainder);
            }
            break;
        default:
            $baseUnit = pow(1000, floor(log($number, 1000)));
            $numBaseUnits = (int) ($number / $baseUnit);
            $remainder = $number % $baseUnit;
            $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
            if ($remainder) {
                $string .= $remainder < 100 ? $conjunction : $separator;
                $string .= convert_number_to_words($remainder);
            }
            break;
    }
   
    if (null !== $fraction && is_numeric($fraction)) {
        $string .= $decimal;
        $words = array();
        foreach (str_split((string) $fraction) as $number) {
            $words[] = $dictionary[$number];
        }
        $string .= implode(' ', $words);
    }
   
    return $string;
}

?>

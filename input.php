<?php
//include_once 'submit.inc.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link rel="stylesheet" type="text/css" href="style.css" />
        <script src="JSCal2/js/jscal2.js"></script>
        <script src="JSCal2/js/lang/en.js"></script>
        <link rel="stylesheet" type="text/css" href="JSCal2/css/jscal2.css" />

        <link rel="stylesheet" type="text/css" href="JSCal2/css/border-radius.css" />
        <link rel="stylesheet" type="text/css" href="JSCal2/css/steel/steel.css" />

        <link rel="stylesheet" type="text/javascript" href="JSCal2/js/jquery-1.7.2-min.js" />
        <script type="text/javascript" src="JSCal2/js/jquery-1.4.js"></script>

        <script src="JSCal2/js/gen_validatorv31.js" type="text/javascript"></script>
        <script src="js/jquery.min.js"></script>
        <script src="js/jquery.validate.js"></script>

    </head>

    <body>
            <div id="container">
                <div id="header">
                    <div style="padding-top: 56px;">CENTRAL SPONSORED SCHEME FOR PROVIDING QUALITY EDUCATION IN MADRASA (SPQEM)</div>
                </div>
                <div id="menubar"><ul>
                        <li><a href="index.php">Home</a></li>

                        <li><a href="input.php">New Application</a></li>

                        <li><a href="edit.php">Edit Application</a></li>

                        <li><a href="print.php">Print Application</a></li>

                        <li><a href="status.php">View Application Status</a></li>

                        <li><a href="login.php">Login</a></li>

                    </ul>
                </div>
                
                <div id="menubar2">
                      <div class="heading_head">APPLICATION FORM</div>
                 </div>                
                <div id="body">
                    <div id="content" style="margin-top: 30px;">
                        <div style="font-size:14px;font-style:oblique;color:#C90065;text-align:center;padding-bottom: 10px;">Application has been closed</div>
                    </div>                
                </div>
                
                <div style="clear:both;"></div>
                <div id="footerouter1">
                    <div id="footer">© All Rights Reserved. IT@School, General Education Department, Govt of Kerala. </div>
                </div>
         </div>
       </body>
     </html>
-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 15, 2014 at 05:51 AM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `spqem`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_dd_comments`
--

CREATE TABLE IF NOT EXISTS `wp_dd_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `master_application_no` int(11) NOT NULL,
  `master_id_no` int(11) NOT NULL,
  `comments` text NOT NULL,
  `district` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

-- --------------------------------------------------------

--
-- Table structure for table `wp_dpi_comments`
--

CREATE TABLE IF NOT EXISTS `wp_dpi_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `master_application_id` int(11) NOT NULL,
  `master_id_no` int(11) NOT NULL,
  `comments` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Table structure for table `wp_format1`
--

CREATE TABLE IF NOT EXISTS `wp_format1` (
  `id` int(11) NOT NULL,
  `master_application_id` int(11) NOT NULL,
  `master_id_no` int(11) NOT NULL,
  `teachers_total` int(11) NOT NULL,
  `student_total` int(11) NOT NULL,
  `primary_class` varchar(200) NOT NULL,
  `secondary_class` varchar(200) NOT NULL,
  `existing` varchar(200) NOT NULL,
  `recruited` varchar(200) NOT NULL,
  `total_count` varchar(200) NOT NULL,
  `primary_level` varchar(200) NOT NULL,
  `upper_primary` varchar(200) NOT NULL,
  `secondary_level` varchar(200) NOT NULL,
  `dsrsecondary_level` varchar(200) NOT NULL,
  `sc` varchar(200) NOT NULL,
  `maths` varchar(200) NOT NULL,
  `lang` varchar(200) NOT NULL,
  `soc_study` varchar(200) NOT NULL,
  `computer_edn` varchar(200) NOT NULL,
  `hidden_ug` varchar(200) NOT NULL,
  `total_ug` varchar(200) NOT NULL,
  `hidden_pg` varchar(200) NOT NULL,
  `total_pg` varchar(200) NOT NULL,
  `grand_total` varchar(200) NOT NULL,
  `qty4` varchar(200) NOT NULL,
  `totals` varchar(200) NOT NULL,
  `agency` varchar(200) NOT NULL,
  `cost` varchar(200) NOT NULL,
  `total_costs` varchar(200) NOT NULL,
  `status` int(11) NOT NULL,
  `status_dpi` int(11) NOT NULL,
  PRIMARY KEY (`master_application_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wp_format2`
--

CREATE TABLE IF NOT EXISTS `wp_format2` (
  `id` int(11) NOT NULL,
  `mater_application_id` int(11) NOT NULL,
  `master_id_no` int(11) NOT NULL,
  `science_kit` varchar(200) NOT NULL,
  `science_kit_no_class` varchar(200) NOT NULL,
  `science_kit_children` varchar(200) NOT NULL,
  `science_kit_amt` varchar(200) NOT NULL,
  `science_kit_anual` varchar(200) NOT NULL,
  `maths_kit` varchar(200) NOT NULL,
  `maths_kit_no_class` varchar(200) NOT NULL,
  `maths_kit_children` varchar(200) NOT NULL,
  `maths_kit_amt` varchar(200) NOT NULL,
  `maths_lab_anual` varchar(200) NOT NULL,
  `science_lab` varchar(200) NOT NULL,
  `science_lab_no_class` varchar(200) NOT NULL,
  `science_lab_children` varchar(200) NOT NULL,
  `science_lab_amt` varchar(200) NOT NULL,
  `science_lab_anual` varchar(200) NOT NULL,
  `computer_lab` varchar(200) NOT NULL,
  `computer_lab_no_class` varchar(200) NOT NULL,
  `computer_lab_children` varchar(200) NOT NULL,
  `computer_lab_amt` varchar(200) NOT NULL,
  `computer_lab_anual` varchar(200) NOT NULL,
  `library_banks_children` varchar(200) NOT NULL,
  `library_banks_amt` varchar(200) NOT NULL,
  `library_banks_anual` varchar(200) NOT NULL,
  `grand_amt` varchar(200) NOT NULL,
  `grand_anual` varchar(200) NOT NULL,
  `grand_total` varchar(200) NOT NULL,
  `status` int(11) NOT NULL,
  `status_dpi` int(11) NOT NULL,
  PRIMARY KEY (`mater_application_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wp_format3`
--

CREATE TABLE IF NOT EXISTS `wp_format3` (
  `id` int(200) NOT NULL,
  `mater_application_id` int(11) NOT NULL,
  `master_id_no` int(11) NOT NULL,
  `address_full` text NOT NULL,
  `date_accrediation` text NOT NULL,
  `no_registered` text NOT NULL,
  `no_stud3` varchar(200) NOT NULL,
  `no_stud5` varchar(200) NOT NULL,
  `no_stud8` varchar(200) NOT NULL,
  `no_boys` varchar(200) NOT NULL,
  `no_girls` varchar(200) NOT NULL,
  `cwsn` varchar(200) NOT NULL,
  `total` varchar(200) NOT NULL,
  `unit_cost` varchar(200) NOT NULL,
  `total_cost` varchar(200) NOT NULL,
  `no_child` varchar(200) NOT NULL,
  `unit_costs` varchar(200) NOT NULL,
  `total_costs` varchar(200) NOT NULL,
  `grand_costs` varchar(200) NOT NULL,
  `status` int(11) NOT NULL,
  `status_dpi` int(11) NOT NULL,
  PRIMARY KEY (`mater_application_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wp_format4`
--

CREATE TABLE IF NOT EXISTS `wp_format4` (
  `id` int(11) NOT NULL,
  `mater_application_id` int(11) NOT NULL,
  `master_id_no` int(11) NOT NULL,
  `address_fulls` text NOT NULL,
  `date_accrediation_vocational` text NOT NULL,
  `no_register` varchar(200) NOT NULL,
  `no_stud13` varchar(200) NOT NULL,
  `no_stud18` varchar(200) NOT NULL,
  `no_boyss` varchar(200) NOT NULL,
  `total_new` varchar(200) NOT NULL,
  `unit_cost_new` varchar(200) NOT NULL,
  `total_cost_new` varchar(200) NOT NULL,
  `no_childs` varchar(200) NOT NULL,
  `unit_costs_new` varchar(200) NOT NULL,
  `status` int(11) NOT NULL,
  `status_dpi` int(11) NOT NULL,
  PRIMARY KEY (`mater_application_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wp_format5`
--

CREATE TABLE IF NOT EXISTS `wp_format5` (
  `id` int(11) NOT NULL,
  `mater_application_id` int(11) NOT NULL,
  `master_id_no` int(11) NOT NULL,
  `tehsil` varchar(200) NOT NULL,
  `name_address` text NOT NULL,
  `date` varchar(200) NOT NULL,
  `no` varchar(200) NOT NULL,
  `detial_recieved` text NOT NULL,
  `student_madrasa` varchar(200) NOT NULL,
  `student_cerf` varchar(200) NOT NULL,
  `assiatance_teacher` varchar(200) NOT NULL,
  `equipment` varchar(200) NOT NULL,
  `books` varchar(200) NOT NULL,
  `not_accredited` varchar(200) NOT NULL,
  `remark` text NOT NULL,
  `status` int(11) NOT NULL,
  `status_dpi` int(11) NOT NULL,
  PRIMARY KEY (`mater_application_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wp_master`
--

CREATE TABLE IF NOT EXISTS `wp_master` (
  `id` int(11) NOT NULL,
  `application_id` int(11) NOT NULL,
  `id_no` int(11) NOT NULL,
  `namehere` varchar(200) NOT NULL,
  `society_address` text NOT NULL,
  `name_address` text NOT NULL,
  `country` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `obj_actv` text NOT NULL,
  `radio1` varchar(200) NOT NULL,
  `madrasa` varchar(200) NOT NULL,
  `reg_no` varchar(200) NOT NULL,
  `reg_date` varchar(200) NOT NULL,
  `educational` text NOT NULL,
  `radio2` varchar(200) NOT NULL,
  `financial` text NOT NULL,
  `nios` text NOT NULL,
  `curriculum` varchar(200) NOT NULL,
  `no_boys` text NOT NULL,
  `total` int(11) NOT NULL,
  `drop1` varchar(200) NOT NULL,
  `qualification_teacher` varchar(200) NOT NULL,
  `school_level` varchar(200) NOT NULL,
  `radio4` varchar(200) NOT NULL,
  `located` text NOT NULL,
  `room_avaliable` varchar(200) NOT NULL,
  `radio3` varchar(200) NOT NULL,
  `accommodation` text NOT NULL,
  `radio5` varchar(200) NOT NULL,
  `seperate_room` text NOT NULL,
  `radio6` varchar(200) NOT NULL,
  `resgistered` varchar(200) NOT NULL,
  `no_stud_cert` varchar(200) NOT NULL,
  `year_passing` varchar(200) NOT NULL,
  `no_stud_cert1` varchar(200) NOT NULL,
  `year_passing1` varchar(200) NOT NULL,
  `no_stud_cert2` varchar(200) NOT NULL,
  `year_passing2` varchar(200) NOT NULL,
  `no_stud_cert3` varchar(200) NOT NULL,
  `year_passing3` varchar(200) NOT NULL,
  `no_stud_cert4` varchar(200) NOT NULL,
  `year_passing4` varchar(200) NOT NULL,
  `financila_assisment` text NOT NULL,
  `proposal` varchar(200) NOT NULL,
  `additional_teacher` int(11) NOT NULL,
  `requirements` varchar(200) NOT NULL,
  `computer` int(11) NOT NULL,
  `training` int(11) NOT NULL,
  `annual` int(11) NOT NULL,
  `library` int(11) NOT NULL,
  `total1` varchar(200) NOT NULL,
  `format` varchar(200) NOT NULL,
  `radio7` varchar(200) NOT NULL,
  `purchase` text NOT NULL,
  `amount_required` varchar(200) NOT NULL,
  `vocational_stream` varchar(200) NOT NULL,
  `salary` varchar(200) NOT NULL,
  `others` varchar(200) NOT NULL,
  `total2` varchar(200) NOT NULL,
  `modern_subject` text NOT NULL,
  `net_amount` varchar(200) NOT NULL,
  `in_words` text NOT NULL,
  `status` int(11) NOT NULL,
  `status_dpi` int(11) NOT NULL,
  PRIMARY KEY (`application_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wp_status_details`
--

CREATE TABLE IF NOT EXISTS `wp_status_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status_no` int(11) NOT NULL,
  `status_indicate` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `wp_status_details`
--

INSERT INTO `wp_status_details` (`id`, `status_no`, `status_indicate`) VALUES
(1, 1, 'Indicates after all forms submitted'),
(2, 2, 'Indicates when DD Confirm the application '),
(3, 3, 'Indicates when DD reject the application'),
(4, 4, 'Indicates when DPI confirm the application'),
(5, 5, 'Indicates when DPI reject the application');

-- --------------------------------------------------------

--
-- Table structure for table `wp_user_master`
--

CREATE TABLE IF NOT EXISTS `wp_user_master` (
  `userid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) NOT NULL,
  `password` varchar(1000) NOT NULL,
  `status` varchar(200) NOT NULL,
  `district` varchar(200) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` int(11) NOT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=228 ;

--
-- Dumping data for table `wp_user_master`
--

INSERT INTO `wp_user_master` (`userid`, `username`, `password`, `status`, `district`, `email`, `phone`) VALUES
(3, 'DPI', '202cb962ac59075b964b07152d234b70', 'A', '0', 'dpi@gmail.com', 12344),
(4, 'ADPI_(general)', '1a1dc91c907325c69271ddf0c944bc72', 'DD', '0', '', 0),
(5, 'ADPI_(Academic)', '1a1dc91c907325c69271ddf0c944bc72', 'DD', '0', '', 0),
(6, 'DDE_TVPM', '202cb962ac59075b964b07152d234b70', 'DD', 'Thiruvanathapuram', '', 0),
(7, 'DDE_Kollam', '1a1dc91c907325c69271ddf0c944bc72', 'DD', 'Kollam', '', 0),
(8, 'DDE_Pathanamthitta', '1a1dc91c907325c69271ddf0c944bc72', 'DD', 'Pathanamthitta', '', 0),
(9, 'DDE_Allapuzha', '1a1dc91c907325c69271ddf0c944bc72', 'DD', 'Alappuzha', '', 0),
(10, 'DDE_Ernakulam', '1a1dc91c907325c69271ddf0c944bc72', 'DD', 'Ernakulam', '', 0),
(11, 'DDE_idukki', '1a1dc91c907325c69271ddf0c944bc72', 'DD', 'Idukki', '', 0),
(12, 'DDE_Thrichur', '1a1dc91c907325c69271ddf0c944bc72', 'DD', 'Thrissur', '', 0),
(13, 'DDE_Palakad', '1a1dc91c907325c69271ddf0c944bc72', 'DD', 'Palakkad', '', 0),
(14, 'DDE_Malapuram', '1a1dc91c907325c69271ddf0c944bc72', 'DD', 'Malappuram', '', 0),
(15, 'DDE_Kozhikode', '1a1dc91c907325c69271ddf0c944bc72', 'DD', 'Kozhikode', '', 0),
(16, 'DDE_Kannur', '1a1dc91c907325c69271ddf0c944bc72', 'DD', 'Kannur', '', 0),
(17, 'DDE_wayanad', '1a1dc91c907325c69271ddf0c944bc72', 'DD', 'Wayanad', '', 0),
(18, 'DDE_Kasarkode', '1a1dc91c907325c69271ddf0c944bc72', 'DD', 'Kasargod', '', 0),
(19, 'DDE_Kottayam', '1a1dc91c907325c69271ddf0c944bc72', 'DD', 'Kottayam', '', 0),
(20, 'DEO_Attingal', '1a1dc91c907325c69271ddf0c944bc72', '3', '0', '', 0),
(21, 'DEO_Neyyatinkara', '1a1dc91c907325c69271ddf0c944bc72', '3', '0', '', 0),
(22, 'DEO_Kottarakara', '1a1dc91c907325c69271ddf0c944bc72', '3', '0', '', 0),
(23, 'DEO_punalur', '1a1dc91c907325c69271ddf0c944bc72', '3', '0', '', 0),
(24, 'DEO_kollam', '1a1dc91c907325c69271ddf0c944bc72', '3', '0', '', 0),
(25, 'DEO_Thiruvalla', '1a1dc91c907325c69271ddf0c944bc72', '3', '0', '', 0),
(26, 'DEO_Pathanamthitta', '1a1dc91c907325c69271ddf0c944bc72', '3', '0', '', 0),
(27, 'DEO_Cherthala', '1a1dc91c907325c69271ddf0c944bc72', '3', '0', '', 0),
(28, 'DEO_Alappuzha', '1a1dc91c907325c69271ddf0c944bc72', '3', '0', '', 0),
(29, 'DEO_Mavelikkara', '1a1dc91c907325c69271ddf0c944bc72', '3', '0', '', 0),
(30, 'DEO_Kottayam', '1a1dc91c907325c69271ddf0c944bc72', '3', '0', '', 0),
(31, 'DEO_Kanjirappilly', '1a1dc91c907325c69271ddf0c944bc72', '3', '0', '', 0),
(32, 'DEO_Pala', '1a1dc91c907325c69271ddf0c944bc72', '3', '0', '', 0),
(33, 'DEO_Kattappana', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(34, 'DEO_Thodupuzha', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(35, 'DEO_Muvattupuzha', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(36, 'DEO_Kothamangalam', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(37, 'DEO_Ernakulam', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(38, 'DEO_Aluva', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(39, 'DEO_Chavakkad', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(40, 'DEO_Irinjalakuda', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(41, 'DEO_Thrissur', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(42, 'DEO_Palakkad', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(43, 'DEO_Ottapalam', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(44, 'DEO_Tirur', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(45, 'DEO_Malappuram', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(46, 'DEO_Kozhikode', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(47, 'DEO_Vatakara', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(48, 'DEO_Thalassery', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(49, 'DEO_Kannur', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(50, 'DEO_Kanhangad', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(51, 'DEO_Kasaragod', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(52, 'DEO_Kaduthuruthy', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(53, 'DEO_Wandoor', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(54, 'DEO_Wayanad', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(55, 'DEO_Kuttanadu', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(56, 'DEO_Thalassery', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(57, 'hcadmin', '1a1dc91c907325c69271ddf0c944bc72', '0', '2', '', 0),
(58, 'AEO_Manjeswar', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(59, 'AEO_Kumbla', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(60, 'AEO_Kasaragod', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(61, 'AEO_Bekal', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(62, 'AEO_Hosdurg', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(63, 'AEO_Chittarikal', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(64, 'AEO_Cheruvathur', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(65, 'AEO_Kannur South', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(66, 'AEO_Kannur North', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(67, 'AEO_Irikur', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(68, 'AEO_Madayi', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(69, 'AEO_Pappinissery', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(70, 'AEO_Thalparamba Nort', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(71, 'AEO_Thaliparamba Sou', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(72, 'AEO_Payyannur', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(73, 'AEO_Thalassery South', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(74, 'AEO_Thalassery North', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(75, 'AEO_Chokli', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(76, 'AEO_Panoor', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(77, 'AEO_Kuthuparamba', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(78, 'AEO_Mattannur', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(79, 'AEO_Iritty', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(80, 'AEO_Vythiri', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(81, 'AEO_Sulthan Bathery', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(82, 'AEO_Mananthavady', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(83, 'AEO_Chombala', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(84, 'AEO_Koyilandy', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(85, 'AEO_Kunnummal', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(86, 'AEO_Melady', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(87, 'AEO_Nadapuram', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(88, 'AEO_Thodannur', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(89, 'AEO_Vatakara', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(90, 'AEO_Kozhikode City', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(91, 'AEO_Kozhikode Rural', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(92, 'AEO_Chevayur', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(93, 'AEO_Feroke', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(94, 'AEO_Kizhisseri', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(95, 'AEO_Kondotty', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(96, 'AEO_Malappuram', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(97, 'AEO_Manjeri', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(98, 'AEO_Mankada', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(99, 'AEO_Perinthalmanna', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(100, 'AEO_Edappal', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(101, 'AEO_Kuttippuram', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(102, 'AEO_Parappanangadi', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(103, 'AEO_Ponnani', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(104, 'AEO_Tanur', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(105, 'AEO_Tirur', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(106, 'AEO_Vengara', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(107, 'AEO_Ottapalam', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(108, 'AEO_Cherpulassery', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(109, 'AEO_Shornur', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(110, 'AEO_Thrithala', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(111, 'AEO_Pattambi', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(112, 'AEO_Alathur', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(113, 'AEO_Chittur', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(114, 'AEO_Coyalamannam', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(115, 'AEO_Kollamghode', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(116, 'AEO_Palakkad', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(117, 'AEO_Parali', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(118, 'AEO_Mannarkkad', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(119, 'AEO_Cherpu', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(120, 'AEO_Thrissur East', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(121, 'AEO_Thrissur West', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(122, 'AEO_Chalakudy', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(123, 'AEO_Irinjalakuda', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(124, 'AEO_Kodungallur', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(125, 'AEO_Mala', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(126, 'AEO_Chavakkad', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(127, 'AEO_Kunnamkulam', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(128, 'AEO_Mullassery', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(129, 'AEO_Valappad', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(130, 'AEO_Wadakancheri', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(131, 'AEO_Aluva', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(132, 'AEO_Angamaly', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(133, 'AEO_Kolancherry', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(134, 'AEO_North Paravur', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(135, 'AEO_Ernakulam', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(136, 'AEO_Mattancherry', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(137, 'AEO_Thripunithura', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(138, 'AEO_Vypeen', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(139, 'AEO_Perumbavoor', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(140, 'AEO_Kothamangalam', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(141, 'AEO_Kalloorkadu', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(142, 'AEO_Koothattukulam', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(143, 'AEO_Muvattupuzha', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(144, 'AEO_Piravom', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(145, 'AEO_Arakkulam', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(146, 'AEO_Thodupuzha', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(147, 'AEO_Adimaly', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(148, 'AEO_Kattappana', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(149, 'AEO_Munnar', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(150, 'AEO_Peermedu', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(151, 'AEO_Nedumkandam', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(152, 'AEO_Ramapuram', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(153, 'AEO_Kozhuvanal', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(154, 'AEO_Ettumanoor', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(155, 'AEO_Pala', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(156, 'AEO_Erattupetta', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(157, 'AEO_Kanjrappilly', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(158, 'AEO_Karukachal', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(159, 'AEO_Kottayam West', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(160, 'AEO_Changanacherry', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(161, 'AEO_Kottayam East', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(162, 'AEO_Pampady', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(163, 'AEO_Cherthala', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(164, 'AEO_Thuravoor', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(165, 'AEO_Alappuzha', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(166, 'AEO_Ambalappuzha', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(167, 'AEO_Haripad', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(168, 'AEO_Mavelikara', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(169, 'AEO_Chengannur', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(170, 'AEO_Kayamkulam', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(171, 'AEO_Tiruvalla', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(172, 'AEO_Pullad', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(173, 'AEO_Aranmula', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(174, 'AEO_Mallappally', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(175, 'AEO_Vennikulam', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(176, 'AEO_Adoor', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(177, 'AEO_Pandalam', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(178, 'AEO_Kozhencherry', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(179, 'AEO_Ranni', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(180, 'AEO_Pathanamthitta', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(181, 'AEO_Konni', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(182, 'AEO_Kottarakkara', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(183, 'AEO_Veliyam', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(184, 'AEO_Kulakkada', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(185, 'AEO_Sasthamcotta', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(186, 'AEO_Chadayamangalam', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(187, 'AEO_Anchal', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(188, 'AEO_Punalur', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(189, 'AEO_Karunagappally', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(190, 'AEO_Chavara', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(191, 'AEO_Kollam', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(192, 'AEO_Chathannoor', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(193, 'AEO_Kundara', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(194, 'AEO_Varkala', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(195, 'AEO_Attingal', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(196, 'AEO_Kilimanoor', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(197, 'AEO_Nedumangad', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(198, 'AEO_Palode', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(199, 'AEO_Thiruvananthapur', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(200, 'AEO_Thiruvananthapur', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(201, 'AEO_Kaniapuram', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(202, 'AEO_Balaramapuram', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(203, 'AEO_Kattakada', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(204, 'AEO_Neyyattinkara', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(205, 'AEO_Parassala', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(206, 'AEO_Vaikom', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(207, 'AEO_Kuravilangadu', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(208, 'AEO_Moncompu', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(209, 'AEO_Thalavady', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(210, 'AEO_Veliyanadu', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(211, 'AEO_Kunnamangalam', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(212, 'AEO_Mukkam', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(213, 'AEO_Thamarassery', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(214, 'AEO_Balussery', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(215, 'AEO_Perambra', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(216, 'AEO_Areacode', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(217, 'AEO_Melattur', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(218, 'AEO_Nilambur', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(219, 'AEO_Wandoor', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(220, 'AEO_Koduvally', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(221, 'Govt_Chief Secretary', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(222, 'Govt_Educational Secretary', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(223, 'IT@School', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(225, 'Pareeksha_Bhavan', '1a1dc91c907325c69271ddf0c944bc72', '0', '0', '', 0),
(227, 'state_admin', '1a1dc91c907325c69271ddf0c944bc72', 'S', '0', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_user_status`
--

CREATE TABLE IF NOT EXISTS `wp_user_status` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=228 ;

-- --------------------------------------------------------

--
-- Table structure for table `wp_year`
--

CREATE TABLE IF NOT EXISTS `wp_year` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `year` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

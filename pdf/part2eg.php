<?php

if(isset($_POST['subipart2'])){
	

include '../library/dbconnect.php';

$gen=$_POST["appnospart2"];
 $query4="SELECT * FROM input_details WHERE Fileno='$gen'";

$result4=mysql_query($query4)or die(mysql_error());
$row=mysql_fetch_array($result4);
// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');

// create new PDF document
 $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('TCPDF Example 021');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 021', PDF_HEADER_STRING);

// set header and footer fonts
//$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('times', '', 12);

// add a page
$pdf->AddPage();

// create some HTML content
$html = '	<div>
				 <div style="text-align: center">
				 	PART-II<br />
					(RECOMMENDATION OF THE STATE GRANT -IN -AID COMMITTEE)
				 </div>
				 <div style="margin-top: 20px;">
				 &nbsp;&nbsp;Name of the Organization Whose case is being recommended: <u>'.$row['orgname'].'</u>
				 </div>				
				<div>
					<table>
					<tr>
						<td width="20">1.</td>
						<td width="500">Whether State  govt has drawn up & disseminated a criteria for prioritization of applications under this scheme</td>
						<td  width="30">:</td>
						<td>Yes/No</td>
					</tr>
					<br />	
					<tr>
						<td>2.</td>	
						<td>Whether the proposal being recommended  for financial assistance,is in accordance with this criteria</td>
						<td  width="30">:</td>
						<td>Yes/No</td>
					</tr>	
					<br />
					<tr>	
						<td>3.</td>
						<td>Whether proposal has been received in the specified application form as prescribed?</td>					
						<td  width="30">:</td>
						<td>Yes/No</td>
					</tr>	
					<br />
					<tr>
						<td>4.</td>
						<td>Whether proposal has been scurtinized  and is in accordance with the eligibility and financial parameters of the scheme</td>
						<td  width="30">:</td>
						<td>Yes/No</td>
					</tr>	
					<br />
					<tr>	
						<td>5.</td>
						<td>Whether the organization has a legal rights to land on which infrastructure is being proposed under this scheme?</td>
						<td  width="30">:</td>
						<td>Yes/No</td>
					</tr>	
					<br />
					<tr>	
						<td>6.</td>
						<td>Whether estimates for the proposal infrastructure are not more than the state PWD schedule of rates?</td>
						<td  width="30">:</td>
						<td>Yes/No</td>
					</tr>	
					<br />
					<tr>	
						<td>7.</td>
						<td>Whether it has been ascertained that the organization being recommended for funding is not duplicating funds received from other state/Central Govt.scheme/programmes for the same purpose?</td> 
						<td  width="30">:</td>
						<td>Yes/No</td>
					</tr>	
					<br />
					<tr>	
						<td>8.</td>
						<td>Whether the organization whose case is being recommended,has the furnished the audited accounts,utilization certificates,annual report & any other performance report as specified,which was due till date of forwarding of case?</td>
						<td  width="30">:</td>
						<td>Yes/No</td>
					</tr>
					<br />	
			
					<tr>
					<td>9.</td>
					<td>The Order of Priority in Which the case of the organization is being recomemended?</td>
					<td  width="30">:</td>
						<td>Yes/No</td>
					</tr>
					
					
					</table><p></p>
				The application has benn examined and it is certified that the organization is elligible<br />for assistance and has the capability of taking up a programme applied for:.	
				
				<p align="right">(Signature of the Member Secretary of State GIAC)</p>
				
				
				
				</div>
			</div>	';
$pdf->writeHTML($html, true, 0, true, 0);

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('example_021.pdf', 'I');
}
?>
//============================================================+
// END OF FILE
//============================================================+

<?php
//============================================================+
// File name   : example_001.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 001 for TCPDF class
//               Default Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @since 2008-03-04
 */
include '../library/dbconnect.php';
$gen=$_POST["appnospart2"];
$val=4005069927;
$query4="SELECT * FROM input_details WHERE Fileno='$val'";

$result4=mysql_query($query4)or die(mysql_error());
$row=mysql_fetch_array($result4);
require_once('tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
//$pdf->SetAuthor('Nicola Asuni');
//$pdf->SetTitle('TCPDF Example 001');
//$pdf->SetSubject('TCPDF Tutorial');
//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
$pdf->setFooterData(array(0,64,0), array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('dejavusans', '', 10, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();
$i="Welcome to th world of Pdf generator";
// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

// Set some content to print
$html ='<html>
	
<table width="100%" border="1" cellspacing="0">
  <tr>
    <td colspan="20" align="center"><strong>SCHEME FOR INFRASTRUCTURE DEVELOPMENT IN MONORITY INSTITUTIONS(IDMI)</strong></td>
  </tr>
  <tr>
    <td colspan="20" align="center"><strong>PROPOSAL FOR THE YEAR 2012-13(KERALA)</strong></td>
  </tr>
  <tr>
    <td colspan="20" align="center"><strong>PART-I</strong></td>
  </tr>
  <tr>
    <td style="-webkit-transform: rotate(270deg);-moz-transform: rotate(270deg);-o-transform: rotate(270deg);writing-mode: lr-tb; border: 0px;">Appliation No.</td>
    <td style="-webkit-transform: rotate(270deg);-moz-transform: rotate(270deg);-o-transform: rotate(270deg);writing-mode: lr-tb; border: 0px;">District</td>
    <td>Name and Address of the school/institution</td>
    <td>Name&amp;address of Agency</td>
    <td>Community of the management</td>
    <td>Specify Category(Aided,Unaided)</td>
    <td>Category of Agency(corporate,chaitable Trust Etc.)</td>
    <td>Whether Higher secondary,secondary or primary</td>
    <td>Specify No.&amp; date of Registration</td>
    <td>Specify the Registration No &amp;Date of Minority Status Certificate obtained from the National Minority Commision</td>
    <td>Whetehr performance report enclosed with the application</td>
    <td>Whether the school has three years of existence</td>
    <td>whether performance report enclosed with application</td>
    <td>No.of students in the school(for 2012-13)</td>
    <td>Estimated amount (Item No.18 of appln)</td>
    <td>75% of estimated amount against item No.18 or 50 Lakhs whichever is less,</td>
    <td>Amount Committed by (Estimated amount minus Central grant)</td>
    <td>Source of Commitment of the agencies to provide the amount</td>
    <td>Ground for grant</td>
    <td>Details of proposed work to be done by using financial assistance.Write each item</td>
  </tr>
  <tr>
    <td>1</td>
    <td>2</td>
    <td>3</td>
    <td>4</td>
    <td>5</td>
    <td>6</td>
    <td>7</td>
    <td>8</td>
    <td>9</td>
    <td>10</td>
    <td>11</td>
    <td>12</td>
    <td>13</td>
    <td>14</td>
    <td>15</td>
    <td>16</td>
    <td>17</td>
    <td>18</td>
    <td>19</td>
    <td>20</td>
  </tr>
  <tr>
  
    <td>'.$val.'</td>
    <td>'.$row['districts'].'</td>
    <td>'.$row['schoolname'].'</td>
    <td>'.$row['orgname'].'</td>
    <td>'.$row['orgname'].'</td>
    <td>'.$row['client_involve'].'</td>
    <td>'.$row['client_involve'].'</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
</html>';

// Print text using writeHTMLCell()
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('example_001.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
//  
    
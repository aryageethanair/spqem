<?php include_once("./login_submit.php");?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
        <link rel="stylesheet" type="text/css" href="style.css" />
        <script src="JSCal2/js/jscal2.js"></script>
        <script src="JSCal2/js/lang/en.js"></script>
        <link rel="stylesheet" type="text/css" href="JSCal2/css/jscal2.css" />
        <link rel="stylesheet" type="text/css" href="JSCal2/css/border-radius.css" />
        <link rel="stylesheet" type="text/css" href="JSCal2/css/steel/steel.css" />

        <script src="JSCal2/js/gen_validatorv31.js" type="text/javascript"></script>
    </head>

    <body>
        <form name="frmlogin" method="post" action="">
        <div id="container">

            <div id="header">
                <div id="header_title">CENTRAL SPONSORED SCHEME FOR PROVIDING QUALITY EDUCATION IN MADRASA (SPQEM)</div>


            </div>
            <div id="menubar"><ul>
                    <li><a href="index.php">Home</a></li>
                    <li><a href="input.php">New Application</a></li>
                    <li><a href="edit.php">Edit Application</a></li>
                    <li><a href="print.php">Print Application</a></li>
                    <li><a href="status.php">View Application Status</a></li>
                    <li><a href="login.php">Login</a></li>
                </ul></div>
            <div id="body">


                <div id="middle">

                    <div id="login">
                        <table width="400" align="center">
                            <tr><td colspan="3" align="center"><font color="#f09186">LOGIN</font></td></tr>
                            
                                <tr><td>Username</td><td width="100" align="center">:</td><td><input type="text" name="uname" value="" id="uname" style="width: 177px;"/></td></tr>
                                <tr><td>Password</td><td align="center">:</td><td><input type="password" name="psd" value="" style="width:177px"/></td></tr>
                                <tr><td colspan="3" align="center"><input type="submit" name="login" value="Login" class="login"/></td></tr>
                                <tr>
                                    <td colspan="3" align="center"><font color="RED"><?php echo $msg;?></font></td>
                                </tr>
                           
                        </table>
                    </div>
                </div>

            </div>
            <div id="footerouter">
                <div id="footer">© All Rights Reserved. IT@School, General Education Department, Govt of Kerala. </div>
            </div>
        </div>
             </form>
    </body>
</html>
